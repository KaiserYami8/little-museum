﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeonGeneratorController : MonoBehaviour
{

    public Animator Peonimator;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Sad_Walk()
    {
        Peonimator.SetTrigger("Sad walk");
    }

    public void Idle()
    {
        Peonimator.SetTrigger("IDLE");
    }

    public void Walk()
    {
        Peonimator.SetTrigger("Walk");
    }
}

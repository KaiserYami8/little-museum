﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PigeonScript : MonoBehaviour
{

    public Animator P_Anim;
    public float AnimSelector;

    public bool IsFlying;

    public GameObject FlyingPoint;
    public float rotatingspeed;

    // Start is called before the first frame update
    void Start()
    {
        if (IsFlying == true)
        {
            P_Anim.SetBool("isFlying", true);
        }
        else { Invoke("AnimPicker", 10); }

        
    }

    // Update is called once per frame
    void Update()
    {
        

        if (FlyingPoint != null)
        {
            FlyingPoint.transform.Rotate(0, rotatingspeed * Time.deltaTime, 0);
        }
    }

    void AnimPicker()
    {
        AnimSelector = Random.Range(0, 2);

        if (AnimSelector == 0) { Launchanim1(); }
        if (AnimSelector == 1) { Launchanim2(); }
        if (AnimSelector == 2) { Launchanim3(); }

        Invoke("AnimPicker", 5);
    }


    void Launchanim1() 
    {
        P_Anim.SetTrigger("Idle_1");
    }
    void Launchanim2()
    {
        P_Anim.SetTrigger("idle_2");
    }
    void Launchanim3()
    {
        P_Anim.SetTrigger("idle_3");
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class voiture : MonoBehaviour
{
    // Start is called before the first frame update

    public float timer = 2;
    public Animator CarAnimator;

    public List<Mesh> MeshList;
    public int RandomMeshDisplay;
    void Start()
    {
        CarAnimator = GetComponent<Animator>();
        timer += Random.Range(1, 20);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= 1 * Time.deltaTime;

        if (timer <= 0)
        {
            CarAnimator.SetTrigger("Go");
            timer += Random.Range(10, 20);

            int RandomMesh = Random.Range(0, MeshList.Count);
            RandomMeshDisplay = RandomMesh;

            gameObject.GetComponent<MeshFilter>().mesh = MeshList[RandomMesh];
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animation_UI_Left : MonoBehaviour
{
    public static Animation_UI_Left _instance;
    QuestManager refQuestManager;
    UIController refUIController;
    public Animator Primary_buttons_animator;

    public Animator OptionAnimator, InventaireAnimator, StatsAnimator, MarketAnimator, ConstructionAnimator;

    public GameObject OptionCloseButton, InventaireCloseButton, StatsCloseButton, MarketCloseButton, ConstructionCloseButton;


    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        refQuestManager = QuestManager._instance;
        refUIController = UIController._instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }



   public void OpenCloseOption()
    {
        //OptionAnimator.SetTrigger("Option_OpenClose");

        if (OptionAnimator.GetBool("Option_OpenClose") == false)
        {
            OptionAnimator.SetBool("Option_OpenClose", true);

            OptionCloseButton.SetActive(true);


            ConstructionAnimator.SetBool("Construction_OpenClose", false);
            InventaireAnimator.SetBool("Inventaire_OpenClose", false);
            StatsAnimator.SetBool("Stats_OpenClose", false);
            MarketAnimator.SetBool("Market_OpenClose", false);

            refUIController.IsOptions = true;
        }
        else
        {
            OptionAnimator.SetBool("Option_OpenClose", false);

            OptionCloseButton.SetActive(false);

            OpenCloseAllMenu();
            refUIController.IsOptions = false;
        }

    }

    public void OpenCloseInventaire()
    {
       // InventaireAnimator.SetTrigger("Inventaire_OpenClose");

        if (InventaireAnimator.GetBool("Inventaire_OpenClose") == false)
        {
            InventaireAnimator.SetBool("Inventaire_OpenClose", true);

            InventaireCloseButton.SetActive(true);

            ConstructionAnimator.SetBool("Construction_OpenClose", false);
            StatsAnimator.SetBool("Stats_OpenClose", false);
            MarketAnimator.SetBool("Market_OpenClose", false);
            OptionAnimator.SetBool("Option_OpenClose", false);

            refUIController.IsCollection = true;
            //Pour Quete
            if (refQuestManager.quests[refQuestManager.activeQuest].tableauxBool)
            {
                if (!refQuestManager.quests[refQuestManager.activeQuest].ouvertureCollection) refQuestManager.CheckOpenInventary();
            }
        }
        else
        {
            InventaireAnimator.SetBool("Inventaire_OpenClose", false);

            InventaireCloseButton.SetActive(false);
            refUIController.IsCollection = false;

            OpenCloseAllMenu();
        }

    }

    public void OpenCloseStats()
    {
      //  StatsAnimator.SetTrigger("Stats_OpenClose");

        if (StatsAnimator.GetBool("Stats_OpenClose") == false)
        {
            StatsAnimator.SetBool("Stats_OpenClose", true);

            StatsCloseButton.SetActive(true);

            ConstructionAnimator.SetBool("Construction_OpenClose", false);
            InventaireAnimator.SetBool("Inventaire_OpenClose", false);
            MarketAnimator.SetBool("Market_OpenClose", false);
            OptionAnimator.SetBool("Option_OpenClose", false);

            refUIController.IsStat = true;
        }
        else
        {
            StatsAnimator.SetBool("Stats_OpenClose", false);

            StatsCloseButton.SetActive(false);
            refUIController.IsStat = false;

            OpenCloseAllMenu();
        }


    }

    public void OpenCloseMarket()
    {
       // MarketAnimator.SetTrigger("Market_OpenClose");

        if (MarketAnimator.GetBool("Market_OpenClose") == false)
        {
            MarketAnimator.SetBool("Market_OpenClose", true);

            MarketCloseButton.SetActive(true);

            ConstructionAnimator.SetBool("Construction_OpenClose", false);
            InventaireAnimator.SetBool("Inventaire_OpenClose", false);
            StatsAnimator.SetBool("Stats_OpenClose", false);          
            OptionAnimator.SetBool("Option_OpenClose", false);

            refUIController.IsMarche = true;
        }
        else
        {
            MarketAnimator.SetBool("Market_OpenClose", false);

            MarketCloseButton.SetActive(false);
            refUIController.IsMarche = false;

            OpenCloseAllMenu();
        }

    }

    public void OpenCloseConstruction()
    {
       // ConstructionAnimator.SetTrigger("Construction_OpenClose");


        if (ConstructionAnimator.GetBool("Construction_OpenClose") == false)
        {
            ConstructionAnimator.SetBool("Construction_OpenClose", true); // open construct menu

            ConstructionCloseButton.SetActive(true);

            InventaireAnimator.SetBool("Inventaire_OpenClose", false); // close all other tabs
            StatsAnimator.SetBool("Stats_OpenClose", false);
            MarketAnimator.SetBool("Market_OpenClose", false);
            OptionAnimator.SetBool("Option_OpenClose", false);
            refUIController.IsConstruct = true;
        }
        else
        {
            ConstructionAnimator.SetBool("Construction_OpenClose", false);

            ConstructionCloseButton.SetActive(false);
            refUIController.IsConstruct = false;

            OpenCloseAllMenu();
        }
    }





    
    public void OpenCloseAllMenu()
    {
       // Primary_buttons_animator.SetTrigger("PrimaryButtonsSlide");

        if (Primary_buttons_animator.GetBool("PrimaryButtonSlide_bool") == false)
        {
            Primary_buttons_animator.SetBool("PrimaryButtonSlide_bool", true);

        }
        else 
        { 
            Primary_buttons_animator.SetBool("PrimaryButtonSlide_bool", false);

           
        }
    }




    public void CloseAllTabs()
    {
        ConstructionAnimator.SetBool("Construction_OpenClose", false);
        InventaireAnimator.SetBool("Inventaire_OpenClose", false);
        StatsAnimator.SetBool("Stats_OpenClose", false);
        MarketAnimator.SetBool("Market_OpenClose", false);
        OptionAnimator.SetBool("Option_OpenClose", false);
    }







}

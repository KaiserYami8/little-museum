﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pecheur_script : MonoBehaviour
{

    public float Timer = 10;
    public Animator PecheurAnimator;

    // Start is called before the first frame update
    void Start()
    {
        PecheurAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        Timer -= 1 * Time.deltaTime;

        

        if (Timer <=0)
        {
            ChooseAnim();
            Timer = 10;
        }
    }

    public void ChooseAnim()
    {
        int i = (Random.Range(1, 3));

        if (i == 1)
        {
            PecheurAnimator.SetTrigger("launch");

        }
        else if (i == 2)
        {
            PecheurAnimator.SetTrigger("touche");
        }
    }
}

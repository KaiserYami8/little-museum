﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlayerMovement : MonoBehaviour
{
    Vector3 _movementVector;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // get AD inputs (strafe left/right) X-axis
        float horizontal = Input.GetAxis("Horizontal");
        // get WS inputs (forward,back) Z-axis
        float vertical = Input.GetAxis("Vertical");

        Vector3 m_Movement = new Vector3(horizontal * 10f, 0f, vertical * 10f);
        m_Movement = transform.TransformDirection(m_Movement);
        _movementVector = m_Movement;

    }

    private void FixedUpdate()
    {
        // move the game object forward/back/left/ight with the keyboard input
        gameObject.transform.Translate(_movementVector * Time.deltaTime, Space.World);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum DetectionType { SimpleForward, FovScanning }

public class FovDetectionCheck : MonoBehaviour
{
    bool detected;

    [SerializeField] Transform eyes;
    [SerializeField] LayerMask layerToDetect;
    [SerializeField] DetectionType detectionType;

    Vector3 target;

    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        switch (detectionType)
        {
            case DetectionType.SimpleForward:

                Debug.DrawRay(eyes.position, eyes.forward * 10f, Color.green);

                RaycastHit hitInfo;
                // scan directly forwards, from the "eyes" position for an entity in the layer "Player"
                if (Physics.Raycast(eyes.position, eyes.forward, out hitInfo, 10f, layerToDetect)) //
                {
                    if (hitInfo.distance > 0.1f) // when we get close enough, stop detecting (we dont need to worry about collisions right now)
                        detected = true;
                    else
                        detected = false;
                }
                else
                {
                    detected = false;
                }
                break;

            case DetectionType.FovScanning:

                var hits = ScanForPlayerFov(120, 5f, 10f, true);

                if (hits != null && hits.Count > 0)
                {
                    target = hits.First().transform.position;

                    detected = true;
                }
                else
                {
                    detected = false;
                }
                break;
        }
    }

    private void FixedUpdate()
    {
        switch (detectionType)
        {
            case DetectionType.SimpleForward:
                // if the scan detected our object, move forwards
                if (detected)
                    transform.Translate(Vector3.forward * Time.deltaTime);
                break;

            case DetectionType.FovScanning:
                if (detected)
                {
                    // look towards the new position, keeping the view vector "level" 
                    // (i.e. dont change the y axis of the look vector, so we dont "look" up or down)
                    var look = new Vector3(target.x, transform.position.y, target.z);
                    transform.LookAt(look, Vector3.up);

                    // move forward after making the turn
                    transform.position += transform.forward * Time.deltaTime;
                }
                
                break;
        }
    }

    public List<RaycastHit> ScanForPlayerFov(int fov, float scanStep, float maxScanRange, bool drawCastLines)
    {
        int startScanAngle = fov / 2;
        int scanStepInterval = (int)(fov / scanStep);

        Quaternion startingAngle = Quaternion.AngleAxis(-1f * startScanAngle, Vector3.up);
        Quaternion stepAngle = Quaternion.AngleAxis(scanStep, Vector3.up);

        List<RaycastHit> hits = new List<RaycastHit>();

        if (maxScanRange <= float.Epsilon)
            return null;

        var angle = eyes.rotation * startingAngle;
        var direction = angle * Vector3.forward;
        var pos = eyes.position;

        for (var i = 0; i < scanStepInterval; i++)
        {
            Color rayColor = Color.yellow;
            float drawDistance = maxScanRange;

            RaycastHit hitInfo;
            if (Physics.Raycast(pos, direction, out hitInfo, maxScanRange, layerToDetect))
            {
                drawDistance = hitInfo.distance;
                hits.Add(hitInfo);
                rayColor = Color.red;
            }

            if (drawCastLines) Debug.DrawRay(pos, direction * drawDistance, rayColor);

            direction = stepAngle * direction;
        }

        return hits.Count == 0 ? null : hits;
    }    
}

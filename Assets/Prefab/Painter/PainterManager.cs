﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PainterManager : MonoBehaviour
{
    public static PainterManager _instance;

    private Salle refSalle;
    private SalleV2 refSalleV2;
    SoundManager refSM;
    CoefManagerV2 refCoef;
    private GenerationMur refGenMur;

    public GameObject Message_prefab;

    public string Tableau1_name;

    public string Tableau2_name;

    public float Tableau_affinite;

    public GameObject painter_scrollMenu;

    public GameObject Cellphone; // récupération du gameobject pour l'animation d'activation;

    public GameObject Notif_Icon; // icone de notification de nouveau message sur painter;

    public float NewMessageCount; // compteur de nouveaux messages a développer
    public Text MessageCount_Display;

    int nbSalleDispo;

    bool isOpen;

    private float[] CoefMultiplicateur;
    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {

        refSalle = Salle._instance;
        refSalleV2 = SalleV2._instance;
        refGenMur = GenerationMur._instance;
        refSM = SoundManager._instance;
        refCoef = CoefManagerV2._instance;

        CoefMultiplicateur = refCoef.CoefMultiplicateur;



        NotifHideShow();
    }


    public void PostPainter()
    {
        var j = 0;
        var i = 0;

        var x = 0;
        var y = 0;
        foreach (var item in refGenMur.canConstructSalle)
        {
            if(item == true)
            {
                j++;
            }
        }
        i = Random.Range(1,j);
        if (refGenMur.canConstructSalle[i] == true)
        {
            if (refSalleV2.ListTabSalles[i].Count > 1)
            {
                x = Random.Range(0, refSalleV2.ListTabSalles_GO[i].Count);
                do
                {
                    y = Random.Range(0, refSalleV2.ListTabSalles_GO[i].Count);
                } while (x == y);



                CheckPopularityTableau(i, x, y);

            }
        }


    }

    public void Painter_OpenClose() // ouverture et fermeture du telephone;
    {
        if (Cellphone != null)
        {
            Animator animator = Cellphone.GetComponent<Animator>();
            if (animator != null)
            {
                bool isOpen = animator.GetBool("Open");

                if(isOpen == true)
                {
                    animator.SetBool("Open", !isOpen);
                    //print("Fermeture de Painter");
                    refSM.FermerPainter();
                    this.isOpen = !isOpen;
                }
                else
                {
                    animator.SetBool("Open", !isOpen);
                    //print("Ouverture de painter");
                    NewMessageCount = 0;
                    refSM.OuvrirPainter();
                    this.isOpen = !isOpen;
                }
                NotifHideShow();
            }
        }
    }

    public void PainterNotif()
    {
        if (!isOpen)
        {
            NewMessageCount += 1;
            NotifHideShow();
            refSM.NotificationPainter();
        }
        
    }



    public void NotifHideShow()
    {
        if (NewMessageCount == 0)
        {
            if (Notif_Icon.activeSelf == true)
            {
                Notif_Icon.SetActive(false);
            }
        }
        else
        {
            Notif_Icon.SetActive(true);
            MessageCount_Display.text = NewMessageCount.ToString();
        }
    }

    public void CheckPopularityTableau(int IDSalle, int tab1, int tab2)
    {
        var listTabSalle = refSalleV2.ListTabSalles[IDSalle];
        var refTab1 = listTabSalle[tab1];
        var refTab2 = listTabSalle[tab2];
        
        float nbPointCommun = 0;
        
        
        if(refTab1.courantArtTableau == refTab2.courantArtTableau)
        {
            //print("pas mal du tous jack");
            nbPointCommun += CoefMultiplicateur[0];
        }
        if (refTab1.periode == refTab2.periode)
        {
            //print("periode check");
            nbPointCommun += CoefMultiplicateur[1];
        }
        if (refTab1.style == refTab2.style)
        {
            //print("style");
            nbPointCommun += CoefMultiplicateur[2];
        }
        if (refTab1.elements == refTab2.elements)
        {
            //print("elements check");
            nbPointCommun += CoefMultiplicateur[3];
        }


        float maxPuntos = 0;

        foreach (var item in CoefMultiplicateur)
        {
            maxPuntos += item;
        }

        var tiers = CalculateTiers(maxPuntos);

        var NewMessage = Instantiate(Message_prefab, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0), painter_scrollMenu.transform); // instantie le prefab en enfant du scrollmenu
        NewMessage.GetComponent<Button>().onClick.AddListener(SoundCliqueCommentaire);
        if (nbPointCommun == 0)
        {
            //AfficherPhraseNul
            NewMessage.GetComponent<PainterMessageScript>().BadAffinity(refTab1.titre, refTab2.titre);
        }
        else if(nbPointCommun <tiers)
        {
            //afficherPhraseBof
            NewMessage.GetComponent<PainterMessageScript>().BofAffinity(refTab1.titre, refTab2.titre);
        }
        else if (nbPointCommun <= tiers*2)
        {
            //afficherPhraseOkay
            NewMessage.GetComponent<PainterMessageScript>().OkayAffinity(refTab1.titre, refTab2.titre);
        }
        else if (nbPointCommun < maxPuntos)
        {
            //afficherPhraseBien
            NewMessage.GetComponent<PainterMessageScript>().GoodAffinity(refTab1.titre, refTab2.titre);
        }
        else
        {
            //afficherPhraseParfait
            NewMessage.GetComponent<PainterMessageScript>().PerfectAffinity(refTab1.titre, refTab2.titre);
        }

        NewMessage.transform.SetAsFirstSibling();

        PainterNotif();
    }

    void SoundCliqueCommentaire()
    {
        refSM.CliquerSurUnCommentaire();
    }
    private float CalculateTiers(float pointsMax)
    {
        var tier = pointsMax / 3;
        var updateTier = (Mathf.FloorToInt(tier) + 1) - tier < 0.5f ? Mathf.FloorToInt(tier) + 1 : Mathf.FloorToInt(tier);
        return updateTier;
    }

}

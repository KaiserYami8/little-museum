﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PainterMessageScript : MonoBehaviour
{

    /* SCRIPT DU MESAGE QUI RECOIS AUTOMATIQUEMENT LES INFORMATIONS DU MANAGER ET QUI ACTUALISE LE TOUT*/
   


    public string T1_name;
    public string T2_name;
    public float T_affinity;

    public Text TextHolder;

    public List<string> BadAffinityList;

    public List<string> GoodAffinityList;

    public List<string> MediumAffinittyList;

    public string green = " <color=#008000ff> ";

    public string orange = " <color=#ffa500ff> ";

    public string red = " <color=#ff0000ff> ";

    public string blue = " <color=#008080ff>" ;

    public string PickedColor;


    public void Start()
    {
     
    }



    //public void MessageUpdate() // lancée directement par le manager
    //{
    //    if (T_affinity <= 1.3f)
    //    {
    //        PickedColor = red;
    //        //print("bad afinitty");

    //        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + T1_name.ToString() + "</color>" + " et " + " <color=#008080ff>" + T2_name.ToString() + "</color>" + " est " + " <color=#ff0000ff> " + BadAffinityList[Random.Range(0,BadAffinityList.Count)].ToString() + " </color> ";

    //    }
    //    else if (T_affinity > 1.3f && T_affinity <= 1.7f)
    //    {
    //        PickedColor = orange;
    //        //print("medium afinitty");

    //        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + T1_name.ToString() + "</color>" + " et " + " <color=#008080ff>" + T2_name.ToString() + "</color>" + " est " + " <color=#ffa500ff> " + MediumAffinittyList[Random.Range(0, MediumAffinittyList.Count)].ToString() + " </color> ";

    //    }
    //    else if (T_affinity > 1.7f)
    //    {
    //        PickedColor = green;
    //        //print("good afinitty");
    //        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + T1_name.ToString() + "</color>" + " et " + " <color=#008080ff>" + T2_name.ToString() + "</color>" + " est " + " <color=#008000ff> " + GoodAffinityList[Random.Range(0, GoodAffinityList.Count)].ToString() + " </color> ";

    //    }

    // //   TextHolder.text = " la combinaison de " + T1_name.ToString() + " et " + T2_name.ToString() + " est " + PickedColor + T_affinity.ToString() + " </color> ";
    //}


    public void BadAffinity(string tabName1, string tabName2)
    {
        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + tabName1 + "</color>" + " et " + " <color=#008080ff>" + tabName2 + "</color>" + " est " + " <color=#ff0000ff> " + BadAffinityList[Random.Range(0, BadAffinityList.Count)].ToString() + " </color> ";
    }

    public void BofAffinity(string tabName1, string tabName2)
    {
        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + tabName1 + "</color>" + " et " + " <color=#008080ff>" + tabName2 + "</color>" + " est " + " <color=#ff0000ff> " + BadAffinityList[Random.Range(0, BadAffinityList.Count)].ToString() + " </color> ";
    }

    public void OkayAffinity(string tabName1, string tabName2)
    {
        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + tabName1 + "</color>" + " et " + " <color=#008080ff>" + tabName2 + "</color>" + " est " + " <color=#ffa500ff> " + MediumAffinittyList[Random.Range(0, MediumAffinittyList.Count)].ToString() + " </color> ";
    }

    public void GoodAffinity(string tabName1, string tabName2)
    {
        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + tabName1 + "</color>" + " et " + " <color=#008080ff>" + tabName2 + "</color>" + " est " + " <color=#008000ff> " + GoodAffinityList[Random.Range(0, GoodAffinityList.Count)].ToString() + " </color>";
    }

    public void PerfectAffinity(string tabName1, string tabName2)
    {
        TextHolder.text = " la combinaison de " + " <color=#008080ff>" + tabName1 + "</color>" + " et " + " <color=#008080ff>" + tabName2 + "</color>" + " est " + " <color=#008000ff> " + GoodAffinityList[Random.Range(0, GoodAffinityList.Count)].ToString() + " </color>";
    }
}

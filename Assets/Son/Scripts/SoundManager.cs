﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string clicATHNeutre_1_1_Sound;
    [FMODUnity.EventRef]
    public string clicATHFenetreOn_1_2_Sound;
    [FMODUnity.EventRef]
    public string clicATHFenetreOff_1_3_Sound;
    [FMODUnity.EventRef]
    public string clicATHFonctionOn_1_4_Sound;
    [FMODUnity.EventRef]
    public string clicATHFonctionOff_1_5_Sound;
    [FMODUnity.EventRef]
    public string scrolling_bas_1_6_Sound;
    [FMODUnity.EventRef]
    public string scrolling_haut_1_7_Sound;
    [FMODUnity.EventRef]
    public string tableau_en_main_2_1_Sound;
    [FMODUnity.EventRef]
    public string tableau_pose_2_2_Sound;
    [FMODUnity.EventRef]
    public string tableau_pose2_2_2_Sound;
    [FMODUnity.EventRef]
    public string tableau_ne_peut_etre_pose_2_3_Sound;
    [FMODUnity.EventRef]
    public string ranger_un_tableau_2_4_Sound;
    [FMODUnity.EventRef]
    public string selectionner_peinture_3_1_Sound;
    [FMODUnity.EventRef]
    public string deselectionner_peinture_3_2_Sound;
    [FMODUnity.EventRef]
    public string peindre_un_mur_3_3_Sound;
    [FMODUnity.EventRef]
    public string peindre_un_sol_3_4_Sound;
    [FMODUnity.EventRef]
    public string extention_de_salle_4_1_Sound;
    [FMODUnity.EventRef]
    public string mur_selectionne_4_2_Sound;
    [FMODUnity.EventRef]
    public string mode_extention_active_4_3_Sound;
    [FMODUnity.EventRef]
    public string mode_extention_desactive_4_4_Sound;
    [FMODUnity.EventRef]
    public string bouton_indisponible_5_1_Sound;
    [FMODUnity.EventRef]
    public string changement_de_journee_5_2_Sound;
    [FMODUnity.EventRef]
    public string tempo_pause_6_1_Sound;
    [FMODUnity.EventRef]
    public string tempo_start_6_2_Sound;
    [FMODUnity.EventRef]
    public string tempo_vitesse_X2_6_3_Sound;
    [FMODUnity.EventRef]
    public string tempo_vitesse_X3_6_4_Sound;
    [FMODUnity.EventRef]
    public string notification_painter_7_1_Sound;
    [FMODUnity.EventRef]
    public string ouvrir_painter_7_2_Sound;
    [FMODUnity.EventRef]
    public string cliquer_sur_un_commentaire_7_3_Sound;
    [FMODUnity.EventRef]
    public string fermer_painter_7_4_Sound;
    [FMODUnity.EventRef]
    public string quete_reussie_8_1_Sound;
    [FMODUnity.EventRef]
    public string recompense_obtenue_8_3_Sound;
    [FMODUnity.EventRef]
    public string tableau_individuel_gagne_8_4_Sound;
    [FMODUnity.EventRef]
    public string ouvrir_liste_quete_8_5_Sound;
    [FMODUnity.EventRef]
    public string fermer_liste_quete_8_6_Sound;
    [FMODUnity.EventRef]
    public string bulle_suivante_9_1_Sound;
    [FMODUnity.EventRef]
    public string bulle_precedente_9_2_Sound;


    //Singelton de l'UI
    public static SoundManager _instance;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }





    //Se déclenche lorsque :
    //Dans le menu principal,
    //??? A voir après avoir réparé le proto
    //DONE  On appuit sur Jouer (rajouter un petit delay avant de lancer la scène ?)
    //DONE  On appuit sur Quitter (rajouter un petit delay avant de quitter ?)

    //Dans le jeu,
    //DONE  On clic sur le bouton pour relancer une bulle de dialogue qui s'était quittée
    //DONE  On clique sur sauvegarder
    //DONE  On clique sur charger sauvegarde
    //DONE  On clique sur retour au bureau
    //DONE  Quand on va débloquer une salle, on appuit sur Valider
    //DONE  Quand on va débloquer une salle, on appuit sur Refuser
    public void ClicATHNeutre()
    {
        FMODUnity.RuntimeManager.PlayOneShot(clicATHNeutre_1_1_Sound);
    }



    //Se déclenche lorsque :
    //DONE  On ouvre la fenêtre de Option
    //DONE  On ouvre la fenetre de Construction
    //DONE  On ouvre la fenetre de Statistique
    //DONE  On ouvre la fenetre de Collection
    //DONE  On ouvre la fenetre de Marche
    //DONE  On ouvre la liste des quetes
    //DONE  On clique sur GetReward

    //Dans le menu,
    //DONE  On appuit sur Credit
    //DONE  On appuit sur Options
    //Dans les options, on ouvre la fenetre de résolutions
    //DONE  Dans les options, on choisit une résolution une fois la fenetre ouverte
    //Dans les options, on ouvre la fenetre de qualité graphique
    //DONE  Dans les options, on choisit une qualité graphique une fois la fenetre ouverte
    public void ClicATHFenetreOn()
    {
        FMODUnity.RuntimeManager.PlayOneShot(clicATHFenetreOn_1_2_Sound);
    }



    //Se déclenche lorsque :
    //DONE  On ferme la fenêtre de Option
    //DONE  On ferme la fenetre de Construction
    //DONE  On ferme la fenetre de Statistique
    //DONE  On ferme la fenetre de Collection
    //DONE  On ferme la fenetre de Marche
    //On ferme la liste des quetes
    //On ferme l'acquisition de quetes

    //Dans le Menu,
    //DONE  On ferme Credits
    //DONE  On ferme Options
    public void ClicATHFenetreOff()
    {
        FMODUnity.RuntimeManager.PlayOneShot(clicATHFenetreOff_1_3_Sound);
    }



    //Dans le Menu,
    //DONE  Dans les options, on active le plein ecran
    public void ClicATHFonctionOn()
    {
        FMODUnity.RuntimeManager.PlayOneShot(clicATHFonctionOn_1_4_Sound);
    }



    //Dans le Menu,
    //DONE  Dans les options, on desactive le plein ecran
    public void ClicATHFonctionOff()
    {
        FMODUnity.RuntimeManager.PlayOneShot(clicATHFonctionOff_1_5_Sound);
    }



    //. Si t'as pas envie de le coder préviens-moi on verra une autre solution
    //Lorsque le joueur est en train de scroller vers le bas les barres de son dans les options
    //Lorsque le joueur est en train de scroller vers le bas la barre de scrolling dans la collection

    //J'ai mit le son UP quand le joueur change la valeur
    public void ScrollingBas()
    {
        FMODUnity.RuntimeManager.PlayOneShot(scrolling_bas_1_6_Sound);
    }



    //Lorsque le joueur est en train de scroller vers le haut les barres de son dans les options
    //Lorsque le joueur est en train de scroller vers le haut la barre de scrolling dans la collection

    //J'ai mit le son UP quand le joueur change la valeur
    public void ScrollingHaut()
    {
        FMODUnity.RuntimeManager.PlayOneShot(scrolling_haut_1_7_Sound);
    }



    //DONE  Lorsque le joueur sélectionne un tableau dans la Collection, afin de le poser
    //DONE  Lorsque le joueur déplace un tableau précédemment posé
    public void TableauEnMain()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tableau_en_main_2_1_Sound);
    }



    //DONE  Lorsque le joueur pose un tableau contre un mur
    public void TableauPose()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tableau_pose_2_2_Sound);
        //FMODUnity.RuntimeManager.PlayOneShot(tableau_pose2_2_2_Sound);
    }



    //DONE  Lorsque le joueur tente de poser un tableau là où il ne peut pas le faire
    public void TableauNePeutEtrePose()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tableau_ne_peut_etre_pose_2_3_Sound);
    }



    //DONE  Lorsque le joueur range un tableau
    public void RangerUnTableau()
    {
        FMODUnity.RuntimeManager.PlayOneShot(ranger_un_tableau_2_4_Sound);
    }



    //DONE  Le joueur a choisit une peinture de mur à placer
    //DONE  Le joueur a choisit une peinture de sol à placer
    public void SelectionnerPeinture()
    {
        FMODUnity.RuntimeManager.PlayOneShot(selectionner_peinture_3_1_Sound);
    }



    //DONE  Le joueur se débarasse de la fonction "peindre un mur"
    //DONE  Le joueur se débarasse de la fonction "peindre un sol"
    public void DeselectionnerPeinture()
    {
        FMODUnity.RuntimeManager.PlayOneShot(deselectionner_peinture_3_2_Sound);
    }



    //DONE  Le joueur vient de peindre un mur
    public void PeindreUnMur()
    {
        FMODUnity.RuntimeManager.PlayOneShot(peindre_un_mur_3_3_Sound);
    }



    //DONE  Le joueur vient de peindre un sol
    public void PeindreUnSol()
    {
        FMODUnity.RuntimeManager.PlayOneShot(peindre_un_sol_3_4_Sound);
    }



    //DONE  Le joueur a débloqué une salle
    public void ExtentionDeSalle()
    {
        FMODUnity.RuntimeManager.PlayOneShot(extention_de_salle_4_1_Sound);
    }



    //DONE  Le joueur sélectionne un mur à détruire
    public void MurSelectionne()
    {
        FMODUnity.RuntimeManager.PlayOneShot(mur_selectionne_4_2_Sound);
    }



    //DONE Le joueur est prêt à sélectionner un mur à détruire
    public void ModeExtentionActive()
    {
        FMODUnity.RuntimeManager.PlayOneShot(mode_extention_active_4_3_Sound);
    }



    //DONE Le joueur n'est plus prêt à sélectionner un mur à détruire
    public void ModeExtentionDesactive()
    {
        FMODUnity.RuntimeManager.PlayOneShot(mode_extention_desactive_4_4_Sound);
    }



    //Le joueur essaye de cliquer sur une fonction pendant que le narrateur est activé
    public void BoutonIndisponible()
    {
        FMODUnity.RuntimeManager.PlayOneShot(bouton_indisponible_5_1_Sound);
    }



    //DONE Le jeu est passé au jour suivant
    public void ChangementDeJournee()
    {
        FMODUnity.RuntimeManager.PlayOneShot(changement_de_journee_5_2_Sound);
    }



    //DONE Le joueur clique sur le bouton "pause" en haut de l'écran
    public void TempoPause()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tempo_pause_6_1_Sound);
    }



    //DONE Le joueur clique sur le bouton "temps vitesse X1" en haut de l'écran
    public void TempoStart()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tempo_start_6_2_Sound);
    }



    //DONE Le joueur clique sur le bouton "temps vitesse X2" en haut de l'écran
    public void TempoVitesseX2()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tempo_vitesse_X2_6_3_Sound);
    }



    //DONE Le joueur clique sur le bouton "temps vitesse X3" en haut de l'écran
    public void TempoVitesseX3()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tempo_vitesse_X3_6_4_Sound);
    }




    //DONE Une nouvelle notification est apparue dans Painter
    public void NotificationPainter()
    {
        FMODUnity.RuntimeManager.PlayOneShot(notification_painter_7_1_Sound);
    }



    //DONE Le joueur ouvre la fenetre Painter
    public void OuvrirPainter()
    {
        FMODUnity.RuntimeManager.PlayOneShot(ouvrir_painter_7_2_Sound);
    }



    //DONE Le joueur clique sur un commentaire. C'est pas pertinent le temps qu'on a pas fait la mise en avant des tableaux en cliquant sur les commentaires en tout cas.
    public void CliquerSurUnCommentaire()
    {
        FMODUnity.RuntimeManager.PlayOneShot(cliquer_sur_un_commentaire_7_3_Sound);
    }



    //DONE Le joueur ferme la fenetre Painter
    public void FermerPainter()
    {
        FMODUnity.RuntimeManager.PlayOneShot(fermer_painter_7_4_Sound);
    }



    //DONE La quête en cours a été achevée
    public void QueteReussie()
    {
        FMODUnity.RuntimeManager.PlayOneShot(quete_reussie_8_1_Sound);
    }



    //DONE Le joueur valide son choix de tableau en cliquant sur Valider
    public void RecompenseObtenue()
    {
        FMODUnity.RuntimeManager.PlayOneShot(recompense_obtenue_8_3_Sound);
    }



    //DONE Ce son se déclenche une fois par tableau obtenue (un son par 0.4 secondes par exemple quand les tableaux atteingnent l'inventaire. Parles-en moi si tu vois pas de quoi je veux parler
    public void TableauIndividuelGagne()
    {
        FMODUnity.RuntimeManager.PlayOneShot(tableau_individuel_gagne_8_4_Sound);
    }



    //DONE Ce son se déclenche une fois par tableau obtenue (un son par 0.4 secondes par exemple quand les tableaux atteingnent l'inventaire. Parles-en moi si tu vois pas de quoi je veux parler
    public void OuvrirListeQuete()
    {
        FMODUnity.RuntimeManager.PlayOneShot(ouvrir_liste_quete_8_5_Sound);
    }



    //DONE Ce son se déclenche une fois par tableau obtenue (un son par 0.4 secondes par exemple quand les tableaux atteingnent l'inventaire. Parles-en moi si tu vois pas de quoi je veux parler
    public void FermerListeQuete()
    {
        FMODUnity.RuntimeManager.PlayOneShot(fermer_liste_quete_8_6_Sound);
    }



    //Le joueur passe la bulle de narrateur à la bulle suivante
    public void BulleSuivante()
    {
        FMODUnity.RuntimeManager.PlayOneShot(bulle_suivante_9_1_Sound);
    }



    //Le joueur passe la bulle de narrateur à la bulle précédente
    public void BullePrecedente()
    {
        FMODUnity.RuntimeManager.PlayOneShot(bulle_precedente_9_2_Sound);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{

    public AudioClip[] clips;
    private AudioSource audioSource;


    void Start()
    {
        audioSource = FindObjectOfType<AudioSource>(); //ça marche que si y'a qu'une AudioSource sur la scène
        audioSource.loop = false;
    }


    private AudioClip GetRandomClip()
    {
        return clips[Random.Range(0, clips.Length)];
    }
    

    void Update()
    {
        if (audioSource.isPlaying == false)
        {
            audioSource.clip = GetRandomClip();
            audioSource.Play();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ArenaMode_Class : MonoBehaviour
{
    public Proto_Tableau_Scriptable tableau;
    public GameObject tableauArena;
    GameLibrary GL;
    Proto_Inventory PI;
    SoundManager SM;

    public Text nomTableau;
    public Text mvtTableau;
    public Image imageTableau;

    public int numberToPick;

    void Start()
    {
        GL = GameLibrary._instance;
        PI = Proto_Inventory._instance;
        SM = SoundManager._instance;
        

        RefreshTableau();
    }

    public void AddTableauInventory()
    {
        GL.tableau_never_use.Remove(tableau);
        PI.Inventaire.Add(tableau);
        gameObject.SetActive(false);
        //SM.AcquisitionTableau();
        QuestManager._instance.numberToPick--;


        int x = QuestManager._instance.quests[QuestManager._instance.activeQuest].numberToPick;

        if (x <= 0)
        {
            tableauArena.SetActive(false);

            UIController._instance.CanModifiateTerrain = false;

            QuestManager._instance.QuestCompleted();

            RefreshTableau();
        }
        

    }

    public void RefreshTableau()
    {
        //var i = Random.Range(0, GL.tableau_never_use.Count);
        //tableau = GL.tableau_never_use[i];

        nomTableau.text = tableau.titre;
        mvtTableau.text = tableau.courantArtTableau.ToString();
        imageTableau.sprite = tableau.imageOeuvre;
        //imageTableau.GetComponent<RectTransform>().sizeDelta = new Vector2(tableau.HauteurTableau*100, tableau.LargeurTableau*100);
    }

    
}

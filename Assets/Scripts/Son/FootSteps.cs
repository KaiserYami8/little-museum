﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootSteps : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string inputSound;
    bool playerIsMoving;
    public float walkingSpeed;
    public float CDBlabla;
    public float CDBlablaMin;
    public float CDBlablaMax;
    public float TimerBlabla;

    private void Start()
    {
    }

    private void Update()
    {
        if (TimerBlabla < CDBlabla)
        {
            TimerBlabla += Time.deltaTime;
        }

        if (TimerBlabla >= CDBlabla)
        {
            FMODUnity.RuntimeManager.PlayOneShot(inputSound);
            TimerBlabla = 0;
        }
    }


    void CallBlabla()
    {

    }



}
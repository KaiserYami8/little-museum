﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blabla : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string inputSound;
    bool playerIsMoving;
    public float walkingSpeed;
    public float CDBlabla;
    public float CDBlablaMin;
    public float CDBlablaMax;
    public float TimerBlabla;

    private void Start()
    {
        CDBlabla = Random.Range(CDBlablaMin,CDBlablaMax);
    }

    private void Update()
    {
        if (TimerBlabla < CDBlabla)
        {
            TimerBlabla += Time.deltaTime;
        }

        if (TimerBlabla >= CDBlabla)
        {
            FMODUnity.RuntimeManager.PlayOneShot(inputSound);
            TimerBlabla = 0;
        }
    }


    void CallBlabla()
    {

    }



}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GetTabTween : MonoBehaviour
{
    public Transform pos;
    Vector3 posV;

    public RectTransform circle, rect;

    public ParticleSystem getParticle;

    void Start()
    {
        if (pos == null)
            posV = GameObject.Find("CollectionFeedbackGoal").transform.position;
        else
            posV = pos.position;

        //getParticle.transform.position = pos.position;

        Sequence addSequence = DOTween.Sequence();

        addSequence.Append(transform.DOMoveX(posV.x, 1).SetEase(Ease.InCirc));
        addSequence.Join(transform.DOMoveY(posV.y, 1).SetEase(Ease.OutBack));
        addSequence.Join(rect.transform.DOScale(Vector3.zero, 1).SetEase(Ease.InExpo));

        //addSequence.AppendCallback(() => getParticle.Play());
        addSequence.Append(circle.transform.DOScale(Vector3.one * 5, .5f).SetEase(Ease.OutExpo));
        addSequence.Join(circle.GetComponent<Image>().DOColor(new Color(1, 1, 1, 0), .5f).SetEase(Ease.InExpo));

        addSequence.AppendCallback(() => Destroy(gameObject));
    }
}

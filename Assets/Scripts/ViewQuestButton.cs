﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewQuestButton : MonoBehaviour
{

    public GameObject panelQuest;
    bool seePanel;
    NarateurManager refNarrateur;

    private void Start()
    {
        refNarrateur = NarateurManager._instance;
    }

    public void SeePanel()
    {
        if (!seePanel)
        {
            panelQuest.SetActive(true);
            seePanel = true;

            if (!refNarrateur.isEventGone[1] && refNarrateur.NarrateurEnabled)
            {
                refNarrateur.AppelNarrateurEvent(1);
                refNarrateur.isEventGone[1] = true;
            }
               
        }
        else
        {
            panelQuest.SetActive(false);
            seePanel = false;
        }
    }
}

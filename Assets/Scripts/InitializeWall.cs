﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeWall : MonoBehaviour
{

    GenerationMur GenMurRef;
    UIController UIControllerRef;
    public enum OrientationWall
    {
        Vertical = 0,
        Horizontal = 1
    }

    public OrientationWall orientation;



    void Start()
    {
        GenMurRef = GenerationMur._instance;
        UIControllerRef = UIController._instance;
        var refWall = UIControllerRef.allFloor;

        for (int i = 0; i < transform.childCount; i++)
        {
            var enfant = transform.GetChild(i);

            var enfant1 = enfant.transform.GetChild(0);
            var enfant2 = enfant.transform.GetChild(1);

            var enfantScript = enfant.GetComponent<WallScript>();
            enfantScript.IsBorder = true;

            if (orientation == OrientationWall.Horizontal)
            {
                enfantScript.orientation = WallScript.OrientationWall.Horizontal;
                var valueX = Mathf.FloorToInt(enfant.gameObject.transform.position.x);
                var valueZ = Mathf.FloorToInt(enfant.gameObject.transform.position.z);
                enfant.GetComponent<WallScript>().PosInArray = new Vector3(valueX, 0, valueZ);
                GenMurRef.FirstFloorHorizontalWall[valueX, valueZ] = enfant.gameObject;

                //if(refWall)

            }
            else 
            {
                var valueX = Mathf.FloorToInt(enfant.gameObject.transform.position.x);
                var valueZ = Mathf.FloorToInt(enfant.gameObject.transform.position.z);
                enfant.GetComponent<WallScript>().PosInArray = new Vector3(valueX, 0, valueZ);
                GenMurRef.FirstFloorVerticalWall[valueX, valueZ] = enfant.gameObject;

                enfantScript.orientation = WallScript.OrientationWall.Vertical;
            }
                


        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

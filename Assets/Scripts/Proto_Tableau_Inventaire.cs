﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Rendering;

public class Proto_Tableau_Inventaire : MonoBehaviour
{
    public Proto_Tableau_Scriptable refTableau;
    public bool _canmodifiateterrain;
    public SoundManager refSM;
    public GameObject panelInfoTableau;
    public bool IsTabNew;

    GenerationMur genmur;
    UIController uiControlRef;
    Proto_Inventory refInventaire;
    QuestManager refQuestManager;

    Color normalColor;
    Color pressedColor;
    Image imageChild;
    private bool _isCLicked;
    public bool IsClicked
    {
        get => _isCLicked;
        set
        {
            if (_isCLicked == value) return;

            _isCLicked = value;

            ChangeColorButton(_isCLicked);
        }
    }


    public void ChangeColorButton(bool statut)
    {
        if (statut)
        {
            imageChild.color = pressedColor;
        }
        else
        {
            imageChild.color = normalColor;

        }
    }



    private void Awake()
    {
        IsTabNew = true;

        normalColor = this.GetComponent<Button>().colors.normalColor;
        pressedColor = this.GetComponent<Button>().colors.pressedColor;
        imageChild = this.transform.GetChild(0).GetComponent<Image>();
    }

    private void Start()
    {
        refSM = SoundManager._instance;
        panelInfoTableau = GameObject.FindGameObjectWithTag("panelInfoTableau");
        genmur = GenerationMur._instance;
        uiControlRef = UIController._instance;
        refInventaire = Proto_Inventory._instance;
        refQuestManager = QuestManager._instance;





    }

    private void Update()
    {
        //print(uiControlRef.CanPosTableau);
        //print(_canCreateTableau);
    }

    public void initializeTabBool(bool isNew)
    {
        IsTabNew = isNew;

        if (!IsTabNew)
        {
            IsTabNew = false;
            gameObject.transform.GetChild(4).gameObject.SetActive(IsTabNew);
            //uiControlRef.CheckTab();
            UIController._instance.CheckTab();
        }
    }

    public void ClickButtonTableau()
    {
        GetButBoolActualise();

        if(uiControlRef.CanPosTableau && IsClicked)
        {
           //IL SE PASSE QUE DALLE
        }
        else if(!uiControlRef.CanPosTableau && IsClicked)
        {
            uiControlRef.PosTableau();
        }
        else if (uiControlRef.CanPosTableau && !IsClicked)
        {
            uiControlRef.PosTableau();
        }



        genmur.ScriptableTableau = refTableau;
        genmur.FakeTableau.transform.localScale = new Vector3(refTableau.ProfondeurTableau, refTableau.HauteurTableau, refTableau.LargeurTableau);


        //Element de quete
        if (refQuestManager.quests[refQuestManager.activeQuest].tableauxBool)
        {
            if (!refQuestManager.quests[refQuestManager.activeQuest].selectionTab) refQuestManager.CheckSelectTab();
        }

        if (IsClicked == false)
        {
            refSM.TableauEnMain();
        }
        else
        {
            refSM.TableauEnMain();
        }
    }

    public void SetNormalStatuts()
    {
        IsClicked = false;
    }

    public void GetButBoolActualise()
    {
        var boutonIventaire = refInventaire.buttonInventaire;
        for (int i = 0; i <boutonIventaire.Count ; i++)
        {
            if (boutonIventaire[i].name == transform.name)
            {
                IsClicked = IsClicked ? false : true;
            }
            else
            {
                boutonIventaire[i].GetComponent<Proto_Tableau_Inventaire>().IsClicked = false;
            }
        }
    }


    public void OnHoverButton()
    {
        //UI_Tableau._instance.tab = refTableau;
        if (IsTabNew)
        {
            IsTabNew = false;
            gameObject.transform.GetChild(4).gameObject.SetActive(IsTabNew);
            uiControlRef.CheckTab();
        }
        UI_Tableau._instance.EnableWindow(/*GetComponent<RectTransform>().position,*/ refTableau /*, false*/);
        //panelInfoTableau.GetComponent<Animator>().SetBool("isOpen",true);
    }
    public void OnExitHOverButton()
    {
        //UI_Tableau._instance.tab = null;
        UI_Tableau._instance.DisableWindow();
        //panelInfoTableau.GetComponent<Animator>().SetBool("isOpen", false);
    }

    private void checkIsNew(bool tabNew, bool other)
    {

        if (other)
        {
            IsTabNew = false;
            gameObject.transform.GetChild(4).gameObject.SetActive(IsTabNew);
            uiControlRef.CheckTab();
        }
    }




}

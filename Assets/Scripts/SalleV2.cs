﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SalleV2 : MonoBehaviour
{
    public static SalleV2 _instance;

    [Header("nbrSalle+1")]
    public int NbrSalle;

    public List<Proto_Tableau_Scriptable>[] ListTabSalles;  
    public List<GameObject>[] ListTabSalles_GO;
    public int[] NbrTableauParSalle;


    public float ReputationAllSalle;

    [Header("Valeur des Salles")]
    public float[] ReputationSalles;
    public float[] ValeurSalles;


    private GenerationMur GenMurRef;
    private CoefManagerV2 CoefRef;
    //private UIDebug UiDebugRef;
    private GameLibrary refGL;
    QuestManager refQuestManager;

    private int ElementCheck1;
    private int ElementCheck2;

    private GameObject tabCheck1;
    private GameObject tabCheck2;

    private float Coef=0;
    float iteration=0;

    [Header("Value Default")]
    public float defaultCoef;


    [Header("ElemSalleToSuppr")]
    public GameObject[] BazarSalles;
    public List<GameObject>[] stockLineRenderer;



    [Header("CoefPourChaqueTableau")]
    //1 = SALLE CIBLÉ
    //2 = Tableau 1
    //3 = TAbleau 2
    public float[][,] coefPerTableauSalles;


    [Header("Array d'array de nb de type de tags par salles")]
    public int[][] nbTableauParSalleParMouvement;
    public int[][] nbTableauParSalleParPeriode;
    public int[][] nbTableauParSalleParStyle;
    public int[][] nbTableauParSalleParElements;


    [Header("VARIABLE DE MORT")]
    public int nbMvt;
    public int nbPeriode;
    public int nbStyle;
    public int nbElem;
    public float[] coefMvtGlobal;
    public float[] coefPeriodeGlobal;
    public float[] coefStyleGlobal;
    public float[] coefElemGlobal;
    public float[] coefPuissance;
    public float[] coefGlobal;

    private void Awake()
    {

        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    void Start()
    {
        GenMurRef = GenerationMur._instance;
        CoefRef = CoefManagerV2._instance;
        //UiDebugRef = UIDebug._instance;
        refGL = GameLibrary._instance;
        refQuestManager = QuestManager._instance;

        nbMvt = System.Enum.GetValues(typeof(GameLibrary.CourantArtistique)).Length;
        nbPeriode = System.Enum.GetValues(typeof(GameLibrary.periode)).Length;
        nbStyle = System.Enum.GetValues(typeof(GameLibrary.style)).Length;
        nbElem = System.Enum.GetValues(typeof(GameLibrary.Elements)).Length;

        //print(nbElem);
        //print(nbMvt);
        ListTabSalles = new List<Proto_Tableau_Scriptable>[NbrSalle];
        ListTabSalles_GO = new List<GameObject>[NbrSalle];
        ReputationSalles = new float[NbrSalle];
        //NbrTableauParSalle = new int[NbrSalle];
        stockLineRenderer = new List<GameObject>[NbrSalle];
        coefPerTableauSalles = new float[NbrSalle][,];

        nbTableauParSalleParMouvement = new int[NbrSalle][];
        nbTableauParSalleParPeriode = new int[NbrSalle][];
        nbTableauParSalleParStyle = new int[NbrSalle][];
        nbTableauParSalleParElements = new int[NbrSalle][];

        ValeurSalles = new float[NbrSalle];

        coefGlobal = new float[NbrSalle];
        coefMvtGlobal = new float[NbrSalle];
        coefPeriodeGlobal = new float[NbrSalle];
        coefStyleGlobal = new float[NbrSalle];
        coefElemGlobal = new float[NbrSalle];

        for (int i = 0; i < ListTabSalles.Length; i++)
        {
            ListTabSalles[i] = new List<Proto_Tableau_Scriptable>();
        }

        for (int i = 0; i < ListTabSalles_GO.Length; i++)
        {
            ListTabSalles_GO[i] = new List<GameObject>();
        }

        for (int i = 0; i < stockLineRenderer.Length; i++)
        {
            stockLineRenderer[i] = new List<GameObject>();
        }

        for (int i = 0; i < nbTableauParSalleParMouvement.Length; i++)
        {
            nbTableauParSalleParMouvement[i] = new int[nbMvt];
        }
        for (int i = 0; i < nbTableauParSalleParPeriode.Length; i++)
        {
            nbTableauParSalleParPeriode[i] = new int[nbPeriode];
        }
        for (int i = 0; i < nbTableauParSalleParStyle.Length; i++)
        {
            nbTableauParSalleParStyle[i] = new int[nbStyle];
        }
        for (int i = 0; i < nbTableauParSalleParElements.Length; i++)
        {
            nbTableauParSalleParElements[i] = new int[nbElem];
        }
    }

    private void Update()
    {
        if (refQuestManager.quests[refQuestManager.activeQuest].sameCourantTabsBool)
        {
            if (!refQuestManager.quests[refQuestManager.activeQuest].sameCourantTabs) CheckQuestNbTabByMouvement();
        }
    }



    public void CalculateSalleCoef(int idSalle)
    {
        //print("La Salle " + idSalle + " est demandé");
        coefPerTableauSalles[idSalle] = new float[ListTabSalles[idSalle].Count, ListTabSalles[idSalle].Count];
        ValeurSalles[idSalle] = 0;
        ReputationSalles[idSalle] = 0;

        CalculateValueSalle(idSalle);
        CalculateCoefMouvement(idSalle);
        CalculateCoefPeriode(idSalle);
        CalculateCoefStyle(idSalle);
        CalculateCoefElements(idSalle);

        CalculateCoefGlobal(idSalle);
        CalculateReputSalle(idSalle);


        CheckCoefPerTableau(idSalle);
        //DeleteLine(idSalle);
        //DrawLine(ListTabSalles_GO[idSalle], coefPerTableauSalles[idSalle], idSalle);

        CalculateValueAllSalles();

        //UiDebugRef.ChangeTextReputSalle(ReputationSalles[idSalle], idSalle);
        //UiDebugRef.ChangeTextReputMusee(ReputationAllSalle);

        
    }

    void CalculateValueSalle(int idSalle)
    {
        if (ListTabSalles[idSalle].Count > 0)
        {
            foreach (var item in ListTabSalles[idSalle])
            {
                ValeurSalles[idSalle] += item.OriginalValue;
            }
            ValeurSalles[idSalle] = ValeurSalles[idSalle] / ListTabSalles[idSalle].Count;
        }
        else
        {
            ValeurSalles[idSalle] = 0;
        }
        
    }

    private float CalculeCoefNbTableau(int idSalle , int idTAGS)
    {
        float cmb = 0;

        switch (idTAGS)
        {
            case 0:
                for (int i = 0; i < nbMvt; i++)
                {
                    cmb += CoefRef.CoefficientNbTableau[nbTableauParSalleParMouvement[idSalle][i], idTAGS];
                }
                break;

            case 1:
                for (int i = 0; i < nbPeriode; i++)
                {
                    cmb += CoefRef.CoefficientNbTableau[nbTableauParSalleParPeriode[idSalle][i], idTAGS];
                }
                break;

            case 2:
                for (int i = 0; i < nbStyle; i++)
                {
                    cmb += CoefRef.CoefficientNbTableau[nbTableauParSalleParStyle[idSalle][i], idTAGS];
                }
                break;

            case 3:
                for (int i = 0; i < nbElem; i++)
                {
                    cmb += CoefRef.CoefficientNbTableau[nbTableauParSalleParElements[idSalle][i], idTAGS];
                }
                break;

        }
        
        return cmb;
    }

    void CalculateCoefMouvement(int idSalle)
    {
        coefMvtGlobal[idSalle] = CalculeCoefNbTableau(idSalle, 0) * coefPuissance[0];
    }

    void CalculateCoefPeriode(int idSalle)
    {
        //print("calculcoefperiode");
        coefPeriodeGlobal[idSalle] = CalculeCoefNbTableau(idSalle, 1) * coefPuissance[1];
    }

    void CalculateCoefStyle(int idSalle)
    {
        //print("calculcoefstyle");
        coefStyleGlobal[idSalle] = CalculeCoefNbTableau(idSalle, 2) * coefPuissance[2];
    }

    void CalculateCoefElements(int idSalle)
    {
        //print("calculcoefele");
        coefElemGlobal[idSalle] = CalculeCoefNbTableau(idSalle, 3) * coefPuissance[3];
    }

    void CalculateCoefGlobal(int idSalle)
    {
        //print("calculcoefglobal");
        coefGlobal[idSalle] = coefMvtGlobal[idSalle] + coefPeriodeGlobal[idSalle] + coefStyleGlobal[idSalle] + coefElemGlobal[idSalle];
    }

    void CalculateReputSalle(int idSalle)
    {
        //print("calculreput");
        ReputationSalles[idSalle] = ValeurSalles[idSalle] * coefGlobal[idSalle];
    }

    private void DrawLine(List<GameObject> listToUse, float[,] coef, int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            tabCheck1 = listToUse[i];
            for (int j = 0; j < listToUse.Count; j++)
            {
                if (i != j)
                {
                    tabCheck2 = listToUse[j];
                    var coefperso = coef[i, j];
                    var line = Proto_Line._instance.CreateLine(tabCheck1.transform.position, tabCheck2.transform.position,tabCheck1 ,coefperso);
                    stockLineRenderer[idSalle].Add(line);
                }
            }
        }
    }

    private void DeleteLine(int idSalle)
    {
        if(stockLineRenderer[idSalle].Count > 0)
        {
            for (int i = 0; i < stockLineRenderer[idSalle].Count; i++)
            {
                foreach (var item in stockLineRenderer[idSalle])
                {
                    Destroy(item);
                }
            }
        }  
    }

    public void UnlockRoom(int x)
    {
        GenMurRef.canConstructSalle[x] = true;
        print(BazarSalles.Length);
        BazarSalles[x].SetActive(false);
    }

    public void CheckCoefPerTableau(int idSalle)
    {

        for (int i = 0; i < coefPerTableauSalles[idSalle].GetLength(0); i++)
        {
            for (int j = 0; j < coefPerTableauSalles[idSalle].GetLength(0); j++)
            {
                if (i != j) coefPerTableauSalles[idSalle][i, j] = coefPerTableauSalles[idSalle][i, j] / 4;
            }
        }


    }

    private void CalculateValueAllSalles()
    {
        float total=0;
        for (int i = 0; i < ReputationSalles.Length; i++)
        {
            if (i != 0)
            {
                total += ReputationSalles[i];
            }
            
        }
        ReputationAllSalle = total;
    }

    public void ChangeLayerTab(bool value)
    {
        var iteration = 0;
        print($"Value Layer : {value}");
        foreach (var item in ListTabSalles_GO)
        {
            iteration++;
            foreach (var item2 in item)
            {
                //print($"Elem2 : {item2}");
                if(item2 != null)
                {
                    item2.GetComponent<DisplayTableau>().ToogleLayer(value);
                }
                else
                {
                    ListTabSalles_GO[iteration].Remove(item2);
                }
                
            }
        }
    }

    private bool CheckQuestNbTabByMouvement()
    {
        var nbTab = refQuestManager.quests[refQuestManager.activeQuest].NbTabRequired;
        GameLibrary.CourantArtistique refCA; 
        switch (refQuestManager.quests[refQuestManager.activeQuest].CourantRequired.ToString())
        {
            case "Manierisme":
                refCA = GameLibrary.CourantArtistique.Manierisme;
                break;
            case "Impressionnisme":
                refCA = GameLibrary.CourantArtistique.Impressionnisme;
                break;
            case "Cubisme":
                refCA = GameLibrary.CourantArtistique.Cubisme;
                break;
            case "Surrealisme":
                refCA = GameLibrary.CourantArtistique.Surrealisme;
                break;
            case "Artnouveau":
                refCA = GameLibrary.CourantArtistique.Artnouveau;
                break;
            default:
                refCA = GameLibrary.CourantArtistique.Manierisme;
                break;
        }

        for (int i = 0; i < ListTabSalles.Length; i++)
        {
            var nbGoodValues = 0;
            for (int j = 0; j < ListTabSalles[i].Count; j++)
            {
                if (ListTabSalles[i][j].courantArtTableau == refCA)
                {
                    nbGoodValues++;
                    if (nbGoodValues >= nbTab)
                    {
                        refQuestManager.CheckNbTabByCourant();
                        return true;
                    }
                }
            }
        }

        return false;



    }

}




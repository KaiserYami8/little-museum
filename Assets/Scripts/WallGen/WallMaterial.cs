﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallMaterial : MonoBehaviour
{
    [Header("Materials")]
    public Material[] OriginMat;

    public Material[] MatWall;
    public Renderer MaterialSlot;


    //permet de savoir dans quelle salle ce situe le pan de mur
    public int IdSalle;
    void Start()
    {
        MaterialSlot = GetComponent<Renderer>();
        //OriginMat = MaterialSlot.materials;
        MatWall = MaterialSlot.materials;
        ApplyMaterials(OriginMat);
    }


    void Update()
    {
        
    }



    public void OverWall(Material mat)
    {
        

        if (OriginMat[0] == mat) return;
        MatWall[0] = mat;
        ApplyMaterials(MatWall);
    }

    public void ExitWall()
    {

        //var newMat = OriginMat;
        //MatWall = newMat;
        
        if (MaterialSlot.materials[0].name == OriginMat[0].name) return;


        ApplyMaterials(OriginMat);

    }

    public void changeMat(Material mat)
    {
        

        if (OriginMat[0] == mat) return;
        OriginMat[0] = mat;
        ApplyMaterials(OriginMat);
    }



    private void ApplyMaterials(Material[] newMat)
    {
        MaterialSlot.materials = newMat;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.AI;

public class GameController : MonoBehaviour
{
    ////Enum pour savoir quel mode de construction utiliser pour placer les murs (destruction aussi?)
    //public enum OverrideMode
    //{
    //    none = 0,
    //    line = 1,
    //    quad = 2
    //}
    ////La valeur de l'enum
    //private OverrideMode overridMode;

    //[Header("Taille Grid")]
    ////Height & Width de la zone de construction
    //public int GridHeight;
    //public int GridWidth;

    //[Header("Reference d'objets")]
    //public GameObject Wall;
    //public GameObject Floor;
    //public GameObject FakeWall;

    ////ref au menu UI de construction(pour le cacher/rendre visible)
    //public GameObject MenuConstructWall { get; set; }
    //public GameObject MenuDestroyWall { get; set; }
    //private Camera Cam { get; set; }

    ////Permet de connaître l'élément de la grille que le joueur à over (ainsi que l'ancien pour enlever l'effet de over)
    //public GameObject ActualOverGO { get; set; }
    //public GameObject PreviousOverGO { get; set; }

    ////permet de visualiser un mur unique sur la grille (preview)
    //public GameObject ActualFakeWall { get; set; }
    //public GameObject PreviousFakeWall { get; set; }

    ////permet de connaitre le le premier élément et dernier de grille pour créer une forme (ligne et quad ici)
    //public GameObject FirstElemFakeGrid { get; set; }
    //public GameObject LastElemFakeGrid { get; set; }

    ////Grille ayant tout les éléments "floor" en réference
    //public List<GameObject> GridFloor { get; set; }

    ////permet de visualiser un ensemble de mur sur la grille (preview)
    //public List<GameObject> GridFake { get; set; }
    //public List<GameObject> PreviewGridFake { get; set; }

    ////
    //private bool SkipConstruct { get; set; }
    //private bool IsConstruct { get; set; }

    ////Bool permettant de savoir si le joueur peut modifier des éléments sur la grille
    //private bool _canModifiateTerrain;
    //public bool CanModifiateTerrain
    //{
    //    get => _canModifiateTerrain;
    //    set
    //    {
    //        if (_canModifiateTerrain == value) return;

    //        _canModifiateTerrain = value;
    //        OverrideReset();

    //    }
    //}

    ////Bool permettant de savoir si le joueur peut créer des murs
    //private bool _canCreateWall;
    //public bool CanCreateWall
    //{
    //    get => _canCreateWall;
    //    set
    //    {
    //        if (_canCreateWall == value) return;

    //        _canCreateWall = value;

    //        MenuConstructWall.SetActive(_canCreateWall);
            
    //        var ColorButt = GameObject.Find("ButtonCreateWall").GetComponent<Image>();
    //        ColorButt.color = _canCreateWall ? Color.red : Color.white;
    //    }
    //}

    ////Bool permettant de savoir si le joueur peut détruire des murs
    //private bool _canDetroyWall;
    //public bool CanDestroyWall
    //{
    //    get => _canDetroyWall;
    //    set
    //    {
    //        if (_canDetroyWall == value) return;

    //        _canDetroyWall = value;

    //        //MenuDestroyWall.SetActive(_canDetroyWall);

    //        var ColorButt = GameObject.Find("ButtonDestroyWall").GetComponent<Image>();
    //        ColorButt.color = _canDetroyWall ? Color.red : Color.white;
    //    }
    //}

    ////Initialisation des variables
    //private void Awake()
    //{
    //    Cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    //    overridMode = OverrideMode.none;
    //    MenuConstructWall = GameObject.Find("MenuBuild");
    //    MenuDestroyWall = GameObject.Find("MenuDestroy");
    //    PreviewGridFake= new List<GameObject>();
    //    GridFloor = new List<GameObject>();
    //    MenuConstructWall.SetActive(false);
    //    MenuDestroyWall.SetActive(false);
    //}
    //void Start()
    //{
    //    GridElement gridGame = new GridElement(GridWidth, GridHeight);
    //    CreateFloor(gridGame.GetGrid());
    //    CreateNavMesh(GridWidth, GridHeight);
        
    //}

    //void Update()
    //{
    //    //print(ActualOverGO.tag);
    //    //Si CanModifiate, on peut voir ou est le curseur sur la grille grace à un systeme de "highlight" 
    //    //et lorsque le joueur est en mode createWall et qu'il relache le button gauche de sa souris il crée un ou plusieurs mur en fonction de sa sélection, il à une preview avant
    //    //si il est en mode destroywall il détruit le mur selectionner (mettre le meme système que la pose de mur pour la destruction?)
    //    if (UIController._instance.CanModifiateTerrain)
    //    {
    //        GetMouseOverItem();


    //        if (Input.GetKeyUp(KeyCode.Mouse0))
    //        {
    //            if (UIController._instance.CanCreateWall && ActualOverGO != null )
    //            {
    //                foreach (var floor in GridFake)
    //                {
    //                    if (floor.GetComponent<BlockController>().HaveObject)
    //                    {
    //                        //var wall = floor.GetComponent<BlockController>().WallRef;
    //                        //wall.GetComponent<WallScript>().ExitWall();
    //                        //print("exit");
    //                    }
    //                    if (!floor.GetComponent<BlockController>().HaveObject)
    //                    {
    //                        floor.GetComponent<BlockController>().CreateWall();
    //                        print("create");
    //                        UpdateNavMesh();

    //                    }
    //                    else
    //                    {
    //                        //var wall = floor.GetComponent<BlockController>().WallRef;
    //                        //wall.GetComponent<WallScript>().ExitWall();
    //                        //print("exit");
    //                    }

    //                }
    //                GridFake.Clear();
    //                foreach (var fake in PreviewGridFake)
    //                {
    //                    Destroy(fake);
    //                }

    //                PreviewGridFake.Clear();

    //            }
    //            SkipConstruct = false;
    //            IsConstruct = false;
    //        }

    //        if (Input.GetKey(KeyCode.Mouse0) && CanDestroyWall && ActualOverGO != null)
    //        {
    //            if (ActualOverGO.tag == "Wall" && !ActualOverGO.GetComponent<WallScript>().IsBorder)
    //            {
    //                ActualOverGO.GetComponent<WallScript>().DestroyWall();
    //            }
    //        }

    //        if (Input.GetKeyDown(KeyCode.Mouse1) && IsConstruct)
    //        {
    //            OverrideReset();
    //            SkipConstruct = true;
    //        }
    //    }

    //    //initialistion de la grille de preview et du premier élément de sélection
    //    if (UIController._instance.CanCreateWall && ActualOverGO != null)
    //    {
    //        if (Input.GetKeyDown(KeyCode.Mouse0))
    //        {
    //            if (ActualOverGO.tag == "Wall")
    //            {
    //                FirstElemFakeGrid = ActualOverGO.GetComponent<WallScript>().ParentFloor;
    //            }
    //            else if (ActualOverGO.tag == "Floor")
    //            {
    //                FirstElemFakeGrid = ActualOverGO;
    //            }
    //            GridFake = new List<GameObject>();
    //            GridFake.Add(FirstElemFakeGrid);
    //        }
    //    }


    //}

    ///// <summary>
    ///// Permet de détecter l'élement sous laquelle le souris est ainsi que de déterminer sur quel élement il est(sol et mur pour l'instant)
    ///// </summary>
    //public void GetMouseOverItem()
    //{

    //    Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
    //    //print(Cam.ScreenPointToRay(Input.mousePosition));
    //    RaycastHit hit;

    //    if (Physics.Raycast(ray, out hit))
    //    {
    //        if (Input.GetKeyDown(KeyCode.Mouse0))
    //        {
    //            print($"hit point : {hit.point}");
    //            var xPoint = Mathf.FloorToInt(hit.point.x);
    //            var yPoint = Mathf.Floor(hit.point.y);
    //            var zPoint = Mathf.FloorToInt(hit.point.z);
    //            var newCoord = new Vector3(xPoint, yPoint, zPoint);
    //            print(newCoord);
    //        }
            
    //        if (hit.collider.gameObject != ActualOverGO)
    //        {
    //            if (ActualOverGO == null)
    //            {
    //                ActualOverGO = hit.collider.gameObject;
    //            }
    //            else
    //            {
    //                PreviousOverGO = ActualOverGO;
    //                ActualOverGO = hit.collider.gameObject;

    //                if (PreviousOverGO.tag == "Floor")
    //                {
    //                    PreviousOverGO.GetComponent<BlockController>().ExitFloor();

    //                }
    //                else if (PreviousOverGO.tag == "Wall")
    //                {
    //                    PreviousOverGO.GetComponent<WallScript>().ExitWall();
    //                }
    //            }

    //            if (ActualOverGO.tag == "Floor")
    //            {
    //                ActualOverGO.GetComponent<BlockController>().OverFloor();
    //            }
    //            else if (ActualOverGO.tag == "Wall")
    //            {
    //                ActualOverGO.GetComponent<WallScript>().OverWall();
    //            }

    //            if (UIController._instance.CanCreateWall && ActualOverGO != null && !SkipConstruct)
    //            {
    //                PreviewWall();
    //            }

    //        }
    //    }
    //    else
    //    {
    //        OverrideReset();
    //    }
    //}

    ///// <summary>
    ///// Permet d'enlever l'élement override
    ///// </summary>
    //public void OverrideReset()
    //{
    //    if (ActualOverGO != null)
    //    {
           
    //        switch (ActualOverGO.tag)
    //        {
    //            case "Floor":
    //                ActualOverGO.GetComponent<BlockController>().ExitFloor();
    //                break;
    //            case "Wall":
    //                ActualOverGO.GetComponent<WallScript>().ExitWall();
    //                break;
    //            default: print("erreur de tag");
    //                break;

    //        }
    //    }
    //    ActualOverGO = null;
    //    PreviousOverGO = null;

    //    if(GridFake != null)
    //    {
    //        foreach (var floor in GridFake)
    //        {
    //            if (floor.GetComponent<BlockController>().HaveObject)
    //            {
    //                var wall = floor.GetComponent<BlockController>().WallRef;
    //                wall.GetComponent<WallScript>().ExitWall();
    //            }
    //        }
    //        GridFake.Clear();
    //    }
        
        
    //    foreach (var fake in PreviewGridFake)
    //    {
    //        Destroy(fake);
    //    }
    //    PreviewGridFake.Clear();
    //    if(ActualFakeWall != null) Destroy(ActualFakeWall);

    //}

    ///// <summary>
    ///// permet de créer une forme (ligne et quad pour l'instant) et de voir la preview de placement de mur
    ///// </summary>
    //public void PreviewWall()
    //{
    //    if (Input.GetKey(KeyCode.Mouse0))
    //    {
    //        if (ActualOverGO.tag == "Wall")
    //        {
    //            LastElemFakeGrid = ActualOverGO.GetComponent<WallScript>().ParentFloor;
    //        }
    //        else if (ActualOverGO.tag == "Floor")
    //        {
    //            LastElemFakeGrid = ActualOverGO;
    //        }
    //        IsConstruct = true;

    //        if (GridFake != null)
    //        {
    //            foreach (var floor in GridFake)
    //            {
    //                if (floor.GetComponent<BlockController>().HaveObject)
    //                {
    //                    var wall = floor.GetComponent<BlockController>().WallRef;
    //                    wall.GetComponent<WallScript>().ExitWall();
    //                    print("exit");
    //                }
    //            }
    //        }

    //        foreach (var fake in PreviewGridFake)
    //        {
    //            Destroy(fake);
    //        }

    //        PreviewGridFake.Clear();

    //        //GridFake = new List<GameObject>();
    //        GridFake.Clear();
    //        GridFake.Add(FirstElemFakeGrid);
    //        GridFake.Add(LastElemFakeGrid);

    //        switch (overridMode)
    //        {
    //            case OverrideMode.none:
    //                GridFake.Clear();
    //                break;
    //            case OverrideMode.line:
    //                generateWallLine();
    //                break;
    //            case OverrideMode.quad:
    //                generateWallQuad();
    //                break;
    //        }

    //        foreach (var floor in GridFake)
    //        {
    //            if (floor.GetComponent<BlockController>().HaveObject)
    //            {
    //                var wall = floor.GetComponent<BlockController>().WallRef;
    //                wall.GetComponent<WallScript>().OverWall();
    //            }
    //            else
    //            {
    //                var newfakewall = Instantiate(FakeWall, new Vector3(floor.transform.position.x, floor.transform.position.y + 1, floor.transform.position.z), Quaternion.identity);
    //                PreviewGridFake.Add(newfakewall);
    //            }

    //        }

    //    }
    //    else
    //    {
    //        if (ActualOverGO.tag == "Floor")
    //        {
    //            if (ActualFakeWall == null)
    //            {
    //                ActualFakeWall = Instantiate(FakeWall, new Vector3(ActualOverGO.transform.position.x, ActualOverGO.transform.position.y + 1, ActualOverGO.transform.position.z), Quaternion.identity);
    //            }
    //            else
    //            {
    //                PreviousFakeWall = ActualFakeWall;
    //                ActualFakeWall = Instantiate(FakeWall, new Vector3(ActualOverGO.transform.position.x, ActualOverGO.transform.position.y + 1, ActualOverGO.transform.position.z), Quaternion.identity);

    //                Destroy(PreviousFakeWall);
    //            }
    //        }
    //        else
    //        {
    //            Destroy(ActualFakeWall);
    //        }

    //        print("test");
    //    }

    //}

   

    ///// <summary>
    ///// fonction qui crée un quad
    ///// </summary>
    //private void generateWallQuad()
    //{
    //    var firstX = FirstElemFakeGrid.transform.position.x;
    //    var firstZ = FirstElemFakeGrid.transform.position.z;

    //    var lastX = LastElemFakeGrid.transform.position.x;
    //    var lastZ = LastElemFakeGrid.transform.position.z;

    //    var diffX = lastX - firstX;
    //    var diffZ = lastZ - firstZ;

    //    if (LastElemFakeGrid == FirstElemFakeGrid)
    //    {
    //        GridFake.RemoveAt(1);
    //    }
    //    else
    //    {

    //        if (diffX > diffZ)
    //        {
    //            if (diffX > 0)
    //            {
    //                diffX--;
    //            }
    //            else if (diffX < 0)
    //            {
    //                diffX++;
    //            }
    //            else if (diffX == 0)
    //            {
    //                diffZ++;
    //            }
    //        }
    //        else if (diffX < diffZ)
    //        {
    //            if (diffZ > 0)
    //            {
    //                diffZ--;
    //            }
    //            else if (diffZ < 0)
    //            {
    //                diffZ++;
    //            }
    //            else if (diffZ == 0)
    //            {
    //                diffX++;
    //            }
    //        }
    //        else if (diffX == diffZ)
    //        {
    //            if (diffX > 0)
    //            {
    //                diffX--;
    //            }
    //            else
    //            {
    //                diffX++;
    //            }
    //        }

    //        var Xpos = diffX > 0 ? true : false;
    //        var Zpos = diffZ > 0 ? true : false;
    //        var diffXPos = diffX > 0 ? diffX : diffX * -1;
    //        var diffZPos = diffZ > 0 ? diffZ : diffZ * -1;

    //        for (int i = 0; i <= diffXPos; i++)
    //        {
    //            for (int j = 0; j <= diffZPos; j++)
    //            {
    //                foreach (var floor in GridFloor)
    //                {
    //                    var xTofind = Xpos ? FirstElemFakeGrid.transform.position.x + i : FirstElemFakeGrid.transform.position.x - i;
    //                    var zTofind = Zpos ? FirstElemFakeGrid.transform.position.z + j : FirstElemFakeGrid.transform.position.z - j;

    //                    var posFloorTofind = new Vector3(xTofind, 0, zTofind);

    //                    if (xTofind == FirstElemFakeGrid.transform.position.x || zTofind == FirstElemFakeGrid.transform.position.z)
    //                    {
    //                        if (floor.transform.position == posFloorTofind /*&& !floor.GetComponent<BlockController>().HaveObject*/)
    //                        {
    //                            GridFake.Add(floor);
    //                        }
    //                    }
    //                }
    //            }
    //        }


    //        for (int i = 0; i <= diffXPos; i++)
    //        {
    //            for (int j = 0; j <= diffZPos; j++)
    //            {
    //                foreach (var floor in GridFloor)
    //                {
    //                    var xTofind = Xpos ? LastElemFakeGrid.transform.position.x - i : LastElemFakeGrid.transform.position.x + i;
    //                    var zTofind = Zpos ? LastElemFakeGrid.transform.position.z - j : LastElemFakeGrid.transform.position.z + j;


    //                    var posFloorTofind = new Vector3(xTofind, 0, zTofind);
    //                    if (xTofind == LastElemFakeGrid.transform.position.x || zTofind == LastElemFakeGrid.transform.position.z)
    //                    {
    //                        if (floor.transform.position == posFloorTofind /*&& !floor.GetComponent<BlockController>().HaveObject*/)
    //                        {
    //                            GridFake.Add(floor);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    }

    //}

    ///// <summary>
    ///// fonction qui crée une ligne
    ///// </summary>
    //private void generateWallLine()
    //{
    //    var firstX = FirstElemFakeGrid.transform.position.x;
    //    var firstZ = FirstElemFakeGrid.transform.position.z;

    //    var lastX = LastElemFakeGrid.transform.position.x;
    //    var lastZ = LastElemFakeGrid.transform.position.z;

    //    var diffX = lastX - firstX;
    //    var diffZ = lastZ - firstZ;

    //    var Xpos = diffX > 0 ? true : false;
    //    var Zpos = diffZ > 0 ? true : false;

    //    var diffXPos = diffX > 0 ? diffX : diffX * -1;
    //    var diffZPos = diffZ > 0 ? diffZ : diffZ * -1;


    //    if (diffXPos >= diffZPos)
    //    {
    //        for (int i = 0; i <= diffXPos; i++)
    //        {
    //            foreach (var floor in GridFloor)
    //            {
    //                var xTofind = Xpos ? FirstElemFakeGrid.transform.position.x + i : FirstElemFakeGrid.transform.position.x - i;
    //                var zTofind = FirstElemFakeGrid.transform.position.z;

    //                var posFloorTofind = new Vector3(xTofind, 0, zTofind);

    //                if (floor.transform.position == posFloorTofind /*&& !floor.GetComponent<BlockController>().HaveObject*/)
    //                {
    //                    GridFake.Add(floor);
    //                }
    //            }
    //        }
    //    }
    //    else
    //    {
    //        for (int i = 0; i <= diffZPos; i++)
    //        {
    //            foreach (var floor in GridFloor)
    //            {
    //                var xTofind = FirstElemFakeGrid.transform.position.x;
    //                var zTofind = Zpos ? FirstElemFakeGrid.transform.position.z + i : FirstElemFakeGrid.transform.position.z - i;

    //                var posFloorTofind = new Vector3(xTofind, 0, zTofind);

    //                if (floor.transform.position == posFloorTofind /*&& !floor.GetComponent<BlockController>().HaveObject*/)
    //                {
    //                    GridFake.Add(floor);
    //                }
    //            }
    //        }
    //    }

    //    GridFake.RemoveAt(1);

    //}


    ///// <summary>
    ///// Fonction de création de la grille de sol et permet de determiner les murs 'bodure'
    ///// </summary>
    ///// <param name="floorGrid"></param>
    //private void CreateFloor(int[,] floorGrid)
    //{

    //    for (int i = 0; i < floorGrid.GetLength(0); i++)
    //    {
    //        for (int j = 0; j < floorGrid.GetLength(1); j++)
    //        {
    //            var posFloor = new Vector3(i, 0, j);
    //            var newFloor = Instantiate(Floor, posFloor, Quaternion.identity);
    //            if ((int)floorGrid.GetValue(i, j) == 0)
    //            {
    //                newFloor.GetComponent<BlockController>().SetBorder();
    //            }
    //            GridFloor.Add(newFloor);
    //        }
    //    }
    //}

    //private void CreateNavMesh(float x,float z)
    //{
    //    GameObject FloorPlane = GameObject.CreatePrimitive(PrimitiveType.Plane);
    //    FloorPlane.transform.position = new Vector3((x/2) - 0.5f, 0.5f , (z/2)- 0.5f);
    //    FloorPlane.transform.localScale = new Vector3((x/10) * 0.98f, 0, (z/10) * 0.98f);
    //    FloorPlane.AddComponent<Rigidbody>();
    //    FloorPlane.AddComponent<NavMeshSurface>();
    //    FloorPlane.tag = "NavMesh";
    //    FloorPlane.GetComponent<Rigidbody>().useGravity = false;
    //    FloorPlane.GetComponent<Rigidbody>().isKinematic = true;
    //    FloorPlane.GetComponent<Renderer>().enabled = false;

    //    UpdateNavMesh();
    //}

    //private void UpdateNavMesh()
    //{
    //    GameObject.FindGameObjectWithTag("NavMesh").GetComponent<NavMeshSurface>().BuildNavMesh();
    //}

    ////Fonctions d'UI


    

}

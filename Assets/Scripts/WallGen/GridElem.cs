﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridElement 
{
    private int width;
    private int height;
    private int[,] gridArray;

    public GridElement(int width, int height)
    {
        this.width = width;
        this.height = height;

        gridArray = new int[width, height];
        
       for (int i = 0; i < gridArray.GetLength(0); i++)
       {
           for (int j = 0; j < gridArray.GetLength(1); j++)
           {
                var gridWidth = gridArray.GetLength(0) -1;
                var gridLength = gridArray.GetLength(1) -1;
                if(i == 0 || j == 0 || i == gridWidth || j == gridLength)
                {
                   gridArray[i, j] = 0;
                }
                else
                {
                    gridArray[i, j] = 1;
                }

           }
       }
        
        
    }

    public GridElement(int width, int height, int otherInit)
    {
        this.width = width;
        this.height = height;

        gridArray = new int[width, height];

        for (int i = 0; i < gridArray.GetLength(0); i++)
        {
            for (int j = 0; j < gridArray.GetLength(1); j++)
            {
                var gridWidth = gridArray.GetLength(0) - 1;
                var gridLength = gridArray.GetLength(1) - 1;

                if (i >= 0 && i < gridWidth / 2)
                {
                    if (j >= 0 && j < gridLength / 2) gridArray[i, j] = 0;
                }
                else if (i >= gridWidth / 2 && i <= gridWidth - 1)
                {
                    if (j >= 0 && j < gridLength / 2) gridArray[i, j] = 1;
                }
                else
                {
                    gridArray[i, j] = 2;
                }
            }
        }
    }

    public (int width, int height) ReturnHeightWidthGrid()
    {
        return (this.width, this.height);
    }

    public int[,] GetGrid()
    {
        return gridArray;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScript : MonoBehaviour
{

    public bool IsBorder;
    public bool CantPlaceTableauFront;
    public bool CantPlaceTableauBehind;

    private bool oneShoot;

    public float WallHeight = 3;
    public float WallRemain=3;

    [Header("Tableaux")]
    public bool HaveTableauFront;
    public bool HaveTableauBehind;
    //La ref du/des tableau poser sur le mur
    public List<GameObject> RefTableauFront;
    public List<GameObject> RefTableauBehind;
    //Elements ou placer le tableau
    public GameObject PosTableauFront;
    public GameObject PosTableauBehind;


    [Header("RefWall")]
    public Vector3 PosInArray; //[X,Z] Y=étage de l'élément.
    public OrientationWall orientation;

    private GenerationMur GenMurRef;
    public enum OrientationWall
    {
        Vertical =0,
        Horizontal = 1
    }

    void Start()
    {
        //ParentController = ParentFloor.GetComponent<BlockController>();
        
        PosTableauFront = transform.GetChild(2).gameObject;
        PosTableauBehind = transform.GetChild(3).gameObject;
        GenMurRef = GenerationMur._instance;
        
    }


    void Update()
    {
        //if (!oneShoot)
        //{
        //    oneShoot = true;
        //    if(orientation == OrientationWall.Horizontal)
        //    {
        //        //Check Front puis Behind
        //        if((int)PosInArray.x+1 <= GenMurRef.FirstFloorVerticalWall.GetLength(0)/* && (int)PosInArray.x - 1 >=0 *//*&& (int)PosInArray.z + 1 <= GenMurRef.FirstFloorVerticalWall.GetLength(1) */&& (int)PosInArray.z - 1 >= 0)
        //        {
        //            if (GenMurRef.FirstFloorVerticalWall[(int)PosInArray.x, (int)PosInArray.z - 1] != null || GenMurRef.FirstFloorVerticalWall[(int)PosInArray.x + 1, (int)PosInArray.z - 1] != null) CantPlaceTableauFront = true;
        //            if (GenMurRef.FirstFloorVerticalWall[(int)PosInArray.x, (int)PosInArray.z] != null || GenMurRef.FirstFloorVerticalWall[(int)PosInArray.x + 1, (int)PosInArray.z] != null) CantPlaceTableauBehind = true;
        //        }
        //        else
        //        {
        //            //if ((int)PosInArray.z - 1 >= 0) CantPlaceTableauFront = true;


        //            if ((int)PosInArray.x + 1 <= GenMurRef.FirstFloorVerticalWall.GetLength(0)) 
        //            {
        //                if (GenMurRef.FirstFloorVerticalWall[(int)PosInArray.x, (int)PosInArray.z] != null || GenMurRef.FirstFloorVerticalWall[(int)PosInArray.x + 1, (int)PosInArray.z] != null) CantPlaceTableauBehind = true;
        //            }
        //        }

                
        //    }
        //    else
        //    {
        //        if (/*(int)PosInArray.x + 1 <= GenMurRef.FirstFloorHorizontalWall.GetLength(0) &&*/ (int)PosInArray.x - 1 >= 0 && (int)PosInArray.z + 1 <= GenMurRef.FirstFloorHorizontalWall.GetLength(1) /*&& (int)PosInArray.z - 1 >= 0*/)
        //        {
        //            //Check Front puis Behind
        //            if (GenMurRef.FirstFloorHorizontalWall[(int)PosInArray.x, (int)PosInArray.z + 1] != null || GenMurRef.FirstFloorHorizontalWall[(int)PosInArray.x, (int)PosInArray.z] != null) CantPlaceTableauFront = true;
        //            if (GenMurRef.FirstFloorHorizontalWall[(int)PosInArray.x - 1, (int)PosInArray.z + 1] != null || GenMurRef.FirstFloorHorizontalWall[(int)PosInArray.x - 1, (int)PosInArray.z] != null) CantPlaceTableauBehind = true;
        //        }
        //        else
        //        {
        //            //if ((int)PosInArray.x - 1 < 0) CantPlaceTableauFront = true;

        //            if ((int)PosInArray.z + 1 <= GenMurRef.FirstFloorHorizontalWall.GetLength(1))
        //            {
        //                if (GenMurRef.FirstFloorHorizontalWall[(int)PosInArray.x, (int)PosInArray.z + 1] != null || GenMurRef.FirstFloorHorizontalWall[(int)PosInArray.x, (int)PosInArray.z] != null) CantPlaceTableauFront = true;
        //            }
        //        }

        //    }
            
        //}
    }

    public void DestroyWall()
    {
        //remove 
        Destroy(gameObject);
    }

    public void SetWall(Quaternion rotationWall)
    {
        //Set PosInArray & Orientation
    }



    public void PosTableau()
    {

    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreRenderTableauCol : MonoBehaviour
{
    public bool IsCollision;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //print(IsCollision);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "tableau" || other.tag == "Wall")
        {
            //print("hit tableau end ");
            IsCollision = false;
        }
       // print("hit exit ");

        
    }

    private void OnTriggerStay(Collider other)
    {
        //print(other.tag);
        if (other.tag == "tableau" || other.tag == "Wall"/* et d'autre potentiellement*/)
        {
            //print("hit tableau stay ");
            IsCollision = true;

        }
        

       // print("hit stay ");
    }
}

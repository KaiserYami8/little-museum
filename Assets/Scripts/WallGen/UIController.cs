﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    //Singelton de l'UI
    public static UIController _instance;

    [Header("Array de Prefab")]
    public GameObject[] ArrayWallPrefab;
    public GameObject[] ArrayPreviewWallPrefab;
    public Material[] ArrayMaterialWall;
    public Material[] ArrayMaterialFloor;

    [Header("Reference des différents Menu")]
    public GameObject ButConstructionGO;
    public GameObject ButInventaireGO;
    public GameObject InventoryPanel;
    public GameObject ParentTimeScale;

    private float fixedDeltaTime;

    Proto_Inventory refInventaire;
    public GenerationMur GenMurRef;
    public Proto_Temporalite ProtoTempoRef;
    SoundManager refSM;
    SalleV2 refSalleV2;
    PeonManager refPeonM;
    Animation_UI_Left refAnimLeft;
    UnlockSalle refUnlockSalle;
    public enum OverrideMode
    {
        none = 0,
        line = 1,
        quad = 2
    }
    //La valeur de l'enum
    public OverrideMode overridMode;

    public List<GameObject> allFloor;

    //ref au menu UI de construction(pour le cacher/rendre visible)
    public GameObject MenuConstructWall { get; set; }
    public GameObject MenuDestroyWall { get; set; }
    [Header("Reference ListButt UI")]
    public GameObject ListButtonWall;
    public GameObject ListButtonMatWall;
    public GameObject MenuButtonMatWall;
    public GameObject ListButtonMatFloor;
    public GameObject MenuButtonMatFloor;
    public GameObject refMenuCompareTableau;
    [HideInInspector]
    public GameObject refMenuTableau;
    


    //Bool permettant de savoir si le joueur peut modifier des éléments sur la grille
    private bool _canModifiateTerrain;
    public bool CanModifiateTerrain
    {
        get => _canModifiateTerrain;
        set
        {
            if (_canModifiateTerrain == value) return;

            _canModifiateTerrain = value;

            foreach (var item in allFloor)
            {
                item.GetComponent<FloorControler>().checkCanConstruct(_canModifiateTerrain);
            }

            GenMurRef.StopPreviewMouseExitConstruct();
            GenMurRef.ResetTableau();
        }
    }

    //Bool permettant de savoir si le joueur peut créer des murs
    private bool _canCreateWall;
    public bool CanCreateWall
    {
        get => _canCreateWall;
        set
        {
            if (_canCreateWall == value) return;

            _canCreateWall = value;

            MenuConstructWall.SetActive(_canCreateWall);

            var ColorButt = GameObject.Find("ButtonCreateWall").GetComponent<Image>();
            ColorButt.color = _canCreateWall ? Color.red : Color.white;

            ListButtonWall.SetActive(_canCreateWall);
            GenMurRef.StopPreviewMouseExitConstruct();

        }
    }

    //Bool permettant de savoir si le joueur peut détruire des murs
    private bool _canDetroyWall;
    public bool CanDestroyWall
    {
        get => _canDetroyWall;
        set
        {
            if (_canDetroyWall == value) return;

            _canDetroyWall = value;

            //MenuDestroyWall.SetActive(_canDetroyWall);

            var ColorButt = GameObject.Find("ButtonDestroyWall").GetComponent<Image>();
            ColorButt.color = _canDetroyWall ? Color.red : Color.white;
        }
    }

    private bool _canDetroyTab;
    public bool CanDestroyTab
    {
        get => _canDetroyTab;
        set
        {
            if (_canDetroyTab == value) return;

            _canDetroyTab = value;

            //MenuDestroyWall.SetActive(_canDetroyWall);

            var ColorButt = GameObject.Find("ButtonDestroyTableau").GetComponent<Image>();
            ColorButt.color = _canDetroyTab ? Color.red : Color.white;
        }
    }

    private bool _canPosTableau;
    public bool CanPosTableau
    {
        get => _canPosTableau;
        set
        {
            if (_canPosTableau == value) return;

            _canPosTableau = value;

            //var ColorButt = GameObject.Find("ButtonCreateTableau").GetComponent<Image>();
            //ColorButt.color = _canPosTableau ? Color.red : Color.white;
            GenMurRef.ResetTableau();
            refSalleV2.ChangeLayerTab(_canPosTableau);
        }
    }

    private bool _canMoveTab;
    public bool CanMoveTab
    {
        get => _canMoveTab;
        set
        {
            if (_canMoveTab == value) return;

            _canMoveTab = value;

            //MenuDestroyWall.SetActive(_canDetroyWall);
            refSalleV2.ChangeLayerTab(_canMoveTab);
        }
    }

    private bool _canChangeMatWall;
    public bool CanChangeMatWall
    {
        get => _canChangeMatWall;
        set
        {
            if (_canChangeMatWall == value) return;

            _canChangeMatWall = value;

            var ColorButt = GameObject.Find("ButtonMatWall").GetComponent<Image>();
            ColorButt.color = _canChangeMatWall ? Color.red : Color.white;

            MenuButtonMatWall.SetActive(_canChangeMatWall);
            GenMurRef.StopTexture();
            refSalleV2.ChangeLayerTab(_canChangeMatWall);

            if (_canChangeMatWall)
            {
                refSM.SelectionnerPeinture();
            }
            else
            {
                refSM.DeselectionnerPeinture();
            }
        }
    }

    private bool _canChangeMatFloor;
    public bool CanChangeMatFloor
    {
        get => _canChangeMatFloor;
        set
        {
            if (_canChangeMatFloor == value) return;

            _canChangeMatFloor = value;

            var ColorButt = GameObject.Find("ButtonMatFloor").GetComponent<Image>();
            ColorButt.color = _canChangeMatFloor ? Color.red : Color.white;

            MenuButtonMatFloor.SetActive(_canChangeMatFloor);
            GenMurRef.StopTexture();

            if (_canChangeMatFloor)
            {
                refSM.SelectionnerPeinture();
            }
            else
            {
                refSM.DeselectionnerPeinture();
            }
        }
    }
    private bool _canCompareTab;
    public bool CanCompareTab
    {
        get => _canCompareTab;
        set
        {
            if (_canCompareTab == value) return;

            _canCompareTab = value;

            //refSalleV2.ChangeLayerTab(_canCompareTab);
        }
    }

    private bool _canUnlockSalle;
    public bool CanUnlockSalle
    {
        get => _canUnlockSalle;
        set
        {
            if (_canUnlockSalle == value) return;

            _canUnlockSalle = value;
            print("call unlock");
            refSalleV2.ChangeLayerTab(_canUnlockSalle);
            refUnlockSalle.UseBon();
            if(!_canUnlockSalle)refUnlockSalle.ForceDisablePanel();
        }
    }

    //Etat des menus
    //Options
    private bool _isOptions;
    public bool IsOptions
    {
        get => _isOptions;
        set
        {
            if (_isOptions == value) return;

            _isOptions = value;

        }
    }

    //Construction
    private bool _isConstruct;
    public bool IsConstruct
    {
        get => _isConstruct;
        set
        {
            if (_isConstruct == value) return;

            _isConstruct = value;

        }
    }

    //Statistique
    private bool _isStat;
    public bool IsStat
    {
        get => _isStat;
        set
        {
            if (_isStat == value) return;

            _isStat = value;

        }
    }

    //Collection
    private bool _isCollection;
    public bool IsCollection
    {
        get => _isCollection;
        set
        {
            if (_isCollection == value) return;

            _isCollection = value;
        }
    }


    //Marche
    private bool _isMarche;
    public bool IsMarche
    {
        get => _isMarche;
        set
        {
            if (_isMarche == value) return;

            _isMarche = value;

        }
    }


    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        allFloor = new List<GameObject>();

        this.fixedDeltaTime = Time.fixedDeltaTime;
    }
    void Start()
    {

        MenuConstructWall = GameObject.Find("MenuBuild");
        MenuDestroyWall = GameObject.Find("MenuDestroy");
        GenMurRef = GenerationMur._instance;
        refInventaire = Proto_Inventory._instance;
        ProtoTempoRef = Proto_Temporalite._instance;
        refSM = SoundManager._instance;
        refSalleV2 = SalleV2._instance;
        refPeonM = PeonManager._instance;
        refAnimLeft = Animation_UI_Left._instance;
        refUnlockSalle = UnlockSalle._instance;

        ListButtonWall.SetActive(false);
        MenuButtonMatWall.SetActive(false);
        MenuButtonMatFloor.SetActive(false);
        //MenuConstructWall.SetActive(false);

        CheckTab();
        PlayGame();
        //PauseGame();
        //MenuDestroyWall.SetActive(false);

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || (Input.GetKeyDown(KeyCode.Mouse1) && CanPosTableau == true))
        {
            QuitMenus();
        }
    }

    /// <summary>
    /// Fonction button destruction de mur 
    /// </summary>
    public void DestroyWall()
    {
        CanCreateWall = false;
        CanDestroyWall = CanDestroyWall ? false : true;
        CanPosTableau = false;
        CanChangeMatFloor = false;
        CanChangeMatWall = false;
        CanMoveTab = false;
        CanDestroyTab = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanModifiateTerrain = CanDestroyWall;
        //MenuConstructWall.SetActive(false);
        //DestroyLineWall();
    }


    /// <summary>
    /// Fonction button construction de mur 
    /// </summary>
    public void CreateWall()
    {
        CanCreateWall = CanCreateWall ? false : true;
        CanDestroyWall = false;
        CanPosTableau = false;
        CanChangeMatFloor = false;
        CanChangeMatWall = false;
        CanMoveTab = false;
        CanDestroyTab = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanModifiateTerrain = CanCreateWall;
        //MenuConstructWall.SetActive(true);
        CreateLineWall();
    }


    /// <summary>
    /// Fonction button Choix de construction ligne 
    /// </summary>
    public void CreateLineWall()
    {
        overridMode = OverrideMode.line;
        if (GameObject.Find("ButtonCreateLine") != null)
        {
            GameObject.Find("ButtonCreateLine").GetComponent<Image>().color = Color.red;
        }

        //GameObject.Find("ButtonCreateQuad").GetComponent<Image>().color = Color.white;
    }


    /// <summary>
    /// Fonction button Choix de construction quad 
    /// </summary>
    public void CreateQuadWall()
    {
        overridMode = OverrideMode.quad;
        GameObject.Find("ButtonCreateLine").GetComponent<Image>().color = Color.white;
        GameObject.Find("ButtonCreateQuad").GetComponent<Image>().color = Color.red;
    }


    //public void DestroyLineWall()
    //{
    //    overridMode = OverrideMode.line;
    //    GameObject.Find("ButtonDestroyLine").GetComponent<Image>().color = Color.red;
    //    GameObject.Find("ButtonDestroyQuad").GetComponent<Image>().color = Color.white;
    //}

    //public void DestroyQuadWall()
    //{
    //    overridMode = OverrideMode.quad;
    //    GameObject.Find("ButtonDestroyLine").GetComponent<Image>().color = Color.white;
    //    GameObject.Find("ButtonDestroyQuad").GetComponent<Image>().color = Color.red;
    //}

    public void PosTableau()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = false;
        CanChangeMatWall = false;
        CanDestroyTab = false;
        CanMoveTab = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanPosTableau = CanPosTableau ? false : true;
        CanModifiateTerrain = CanPosTableau;
    }

    public void MoveTab()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = false;
        CanChangeMatWall = false;
        CanDestroyTab = false;
        CanPosTableau = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanMoveTab = CanMoveTab ? false : true;
        CanModifiateTerrain = CanMoveTab;
    }

    public void DestroyTableau()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = false;
        CanChangeMatWall = false;
        CanMoveTab = false;
        CanPosTableau = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanDestroyTab = CanDestroyTab ? false : true;
        if (CanDestroyTab)
        {
            refSM.ClicATHFonctionOn();
        }
        else
        {
            refSM.ClicATHFonctionOff();
        }
        CanModifiateTerrain = CanDestroyTab;

    }


    public void ChangeWallRefConstruction()
    {
        var refBut = EventSystem.current.currentSelectedGameObject;
        var refButName = refBut.name;

        var listbutt = ListButtonWall.transform.GetChild(0);
        for (int i = 0; i < listbutt.childCount; i++)
        {
            if (listbutt.GetChild(i).name == refButName)
            {
                listbutt.GetChild(i).GetComponent<Image>().color = Color.red;

            }
            else
            {
                listbutt.GetChild(i).GetComponent<Image>().color = Color.white;
            }
        }



        switch (refButName)
        {
            case "WallSimple1":
                GenMurRef.AssignedWall(ArrayWallPrefab[0], ArrayPreviewWallPrefab[0]);
                break;
            case "WallSimple2":
                GenMurRef.AssignedWall(ArrayWallPrefab[1], ArrayPreviewWallPrefab[1]);
                break;
            case "WallSimple3":
                GenMurRef.AssignedWall(ArrayWallPrefab[2], ArrayPreviewWallPrefab[2]);
                break;
            case "WallMoulure1":
                GenMurRef.AssignedWall(ArrayWallPrefab[3], ArrayPreviewWallPrefab[3]);
                break;
            case "WallMoulure2":
                GenMurRef.AssignedWall(ArrayWallPrefab[4], ArrayPreviewWallPrefab[4]);
                break;

            default:
                GenMurRef.AssignedWall(ArrayWallPrefab[0], ArrayPreviewWallPrefab[0]);
                break;

        }

    }



    public void MaterialWall()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = false;
        CanChangeMatWall = CanChangeMatWall ? false : true;
        CanPosTableau = false;
        CanDestroyTab = false;
        CanMoveTab = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanModifiateTerrain = CanChangeMatWall;
    }

    public void MaterialFloor()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = CanChangeMatFloor ? false : true; 
        CanChangeMatWall = false;
        CanPosTableau = false;
        CanDestroyTab = false;
        CanMoveTab = false;
        CanCompareTab = false;
        CanUnlockSalle = false;
        CanModifiateTerrain = CanChangeMatFloor;
    }

    public void CompareTableaux()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = false;
        CanCompareTab = CanCompareTab ? false : true;
        CanChangeMatWall = false;
        CanPosTableau = false;
        CanDestroyTab = false;
        CanMoveTab = false;
        CanUnlockSalle = false;
        CanModifiateTerrain = false;
    }

    public void isUnlockSalle()
    {
        CanCreateWall = false;
        CanDestroyWall = false;
        CanChangeMatFloor = false;
        CanCompareTab = false;
        CanChangeMatWall = false;
        CanPosTableau = false;
        CanDestroyTab = false;
        CanMoveTab = false;
        CanUnlockSalle = CanUnlockSalle ? false : true;
        CanModifiateTerrain = false;
    }

    public void ChangeMaterialWall()
    {
        var refBut = EventSystem.current.currentSelectedGameObject;
        var refButName = refBut.name;

        var listbutt = ListButtonMatWall.transform;
        for (int i = 0; i < listbutt.childCount; i++)
        {
            if (listbutt.GetChild(i).name == refButName)
            {
                listbutt.GetChild(i).GetComponent<Image>().color = Color.red;

            }
            else
            {
                listbutt.GetChild(i).GetComponent<Image>().color = Color.white;
            }
        }

        for (int i = 0; i < ArrayMaterialWall.Length; i++)
        {
            if ($"Mat_Wall_{i}" == refButName) GenMurRef.AssignedMatWall(ArrayMaterialWall[i]);
        }
    }

    public void ChangeMaterialFloor()
    {
        var refBut = EventSystem.current.currentSelectedGameObject;
        var refButName = refBut.name;

        var listbutt = ListButtonMatFloor.transform;
        for (int i = 0; i < listbutt.childCount; i++)
        {
            if (listbutt.GetChild(i).name == refButName)
            {
                listbutt.GetChild(i).GetComponent<Image>().color = Color.red;
            }
            else
            {
                listbutt.GetChild(i).GetComponent<Image>().color = Color.white;
            }
        }

        for (int i = 0; i < ArrayMaterialFloor.Length; i++)
        {
            if ($"Mat_Floor_{i}" == refButName) GenMurRef.AssignedMatFloor(ArrayMaterialFloor[i]);
        }
    }


    public void QuitMenus()
    {
        //SoundManager._instance.ClicATHOff();
        if (CanPosTableau)
        {
            CanCreateWall = false;
            CanDestroyWall = false;
            CanChangeMatFloor = false;
            CanChangeMatWall = false;
            CanPosTableau = false;
            CanMoveTab = false;
            CanDestroyTab = false;
            CanCompareTab = false;
            CanUnlockSalle = false;
            CanModifiateTerrain = false;

            refInventaire.ResetButton();
            //ButConsruction.OnlyClose();
        }
        else if(CanChangeMatFloor)
        {
            CanCreateWall = false;
            CanDestroyWall = false;
            CanChangeMatFloor = false;
            CanChangeMatWall = false;
            CanPosTableau = false;
            CanMoveTab = false;
            CanDestroyTab = false;
            CanCompareTab = false;
            CanUnlockSalle = false;
            CanModifiateTerrain = false;

        }
        else if (CanChangeMatWall)
        {
            CanCreateWall = false;
            CanDestroyWall = false;
            CanChangeMatFloor = false;
            CanChangeMatWall = false;
            CanPosTableau = false;
            CanMoveTab = false;
            CanDestroyTab = false;
            CanCompareTab = false;
            CanUnlockSalle = false;
            CanModifiateTerrain = false;

            //ButInventaire.OnlyClose();
            //ButConsruction.OnlyClose();
        }
        else if (CanChangeMatFloor)
        {
            CanCreateWall = false;
            CanDestroyWall = false;
            CanChangeMatFloor = false;
            CanChangeMatWall = false;
            CanPosTableau = false;
            CanMoveTab = false;
            CanDestroyTab = false;
            CanCompareTab = false;
            CanUnlockSalle = false;
            CanModifiateTerrain = false;

            //ButInventaire.OnlyClose();
            //ButConsruction.OnlyClose();
        }else if (CanUnlockSalle)
        {
            CanCreateWall = false;
            CanDestroyWall = false;
            CanChangeMatFloor = false;
            CanChangeMatWall = false;
            CanPosTableau = false;
            CanMoveTab = false;
            CanDestroyTab = false;
            CanCompareTab = false;
            CanUnlockSalle = false;
            CanModifiateTerrain = false;
        }else if (CanMoveTab)
        {
            CanCreateWall = false;
            CanDestroyWall = false;
            CanChangeMatFloor = false;
            CanChangeMatWall = false;
            CanPosTableau = false;
            CanMoveTab = false;
            CanDestroyTab = false;
            CanCompareTab = false;
            CanUnlockSalle = false;
            CanModifiateTerrain = false;
            GenMurRef.toggleElementVisibility();
        }
        else if (IsOptions)
        {
            refAnimLeft.OpenCloseOption();
        }
        else if (IsConstruct)
        {
            refAnimLeft.OpenCloseConstruction();
        }
        else if (IsMarche)
        {
            refAnimLeft.OpenCloseMarket();
        }
        else if (IsCollection)
        {
            refAnimLeft.OpenCloseInventaire();
        }
        else if (IsStat)
        {
            refAnimLeft.OpenCloseStats();
        }
        else //Si aucun menu est ouvert -> ouvrir menu options
        {
            refAnimLeft.OpenCloseOption();
        }



    }

    public void QuitGame()
    {
        print("QuitGame");
        Application.Quit();
    }

    public void PauseGame()
    {
        //print("Pause");
        ProtoTempoRef.UpdatePeriod(0);
        var refBut = ParentTimeScale.transform.GetChild(0).gameObject;
        ChangeButColorTime(refBut);
        refPeonM.SpeedPeon(0);


        
    }

    public void PlayGame()
    {
        //print("Play");
        ProtoTempoRef.UpdatePeriod(1);
        var refBut = ParentTimeScale.transform.GetChild(1).gameObject;
        ChangeButColorTime(refBut);
        refPeonM.SpeedPeon(1);

    }

    public void Playx2Game()
    {
        //print("X2");

        ProtoTempoRef.UpdatePeriod(2);
        var refBut = ParentTimeScale.transform.GetChild(2).gameObject;
        ChangeButColorTime(refBut);
        refPeonM.SpeedPeon(2);

    }

    public void Playx3Game()
    {
        //print("X3");

        ProtoTempoRef.UpdatePeriod(3);
        var refBut = ParentTimeScale.transform.GetChild(3).gameObject;
        ChangeButColorTime(refBut);
        refPeonM.SpeedPeon(3);

    }

    private void ChangeButColorTime(GameObject refBut)
    {
        for (int i = 0; i < ParentTimeScale.transform.childCount; i++)
        {
            var child = ParentTimeScale.transform.GetChild(i).gameObject;

            if (refBut == child)
            {
                //if (child.GetComponent<Image>().color == child.GetComponent<Button>().colors.pressedColor)
                //{
                //    child.GetComponent<Image>().color = child.GetComponent<Button>().colors.normalColor;

                //    if (play == "play")
                //    {
                //        PauseGame();
                //    }
                //    else
                //    {
                //        PlayGame();
                //    }

                //}
                //else
                //{
                    child.GetComponent<Image>().color = child.GetComponent<Button>().colors.pressedColor;
                    child.GetComponent<Button>().interactable = false;
                //}

            }
            else
            {
                child.GetComponent<Image>().color = child.GetComponent<Button>().colors.normalColor;
                child.GetComponent<Button>().interactable = true;
            }
        }
    }


    public void CheckTab()
    {
        var nbTab = InventoryPanel.transform.childCount;
        var notifInventaire = ButInventaireGO.transform.GetChild(1).gameObject;

        if (nbTab == 0)
        {
            notifInventaire.SetActive(false);
        }
        else
        {
            int nbTabNoView = 0;
            for (int i = 0; i < nbTab; i++)
            {
                if (InventoryPanel.transform.GetChild(i).gameObject.GetComponent<Proto_Tableau_Inventaire>().IsTabNew)
                {
                    nbTabNoView++;
                }
            }

            if (nbTabNoView != 0)
            {
                notifInventaire.SetActive(true);
                notifInventaire.transform.GetChild(0).gameObject.GetComponent<Text>().text = $"{nbTabNoView}";

            }
            else
            {
                notifInventaire.SetActive(false);
            }

        }

    }

    public void CheckBonTableau()
    {
        var nbBon = UnlockSalle._instance.unlockBon;
        var notifConstruction = ButConstructionGO.transform.GetChild(1).gameObject;
        if (nbBon == 0)
        {
            notifConstruction.SetActive(false);
        }
        else
        {
            notifConstruction.SetActive(true);
            

                notifConstruction.transform.GetChild(0).gameObject.GetComponent<Text>().text = $"{nbBon}";
            
        }
    }
}

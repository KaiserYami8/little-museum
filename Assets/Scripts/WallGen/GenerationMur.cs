﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GenerationMur : MonoBehaviour
{

    public static GenerationMur _instance;

    //Reference to the MainCamera
    Camera Cam;

    bool IsConstruct { get; set; }




    [Header("Reference d'objets")]
    public GameObject Wall;
    public GameObject FakeWall;
    public GameObject Floor;
    public GameObject Pilier;
    public GameObject FakeTableau;
    public GameObject Tableau;
    public Proto_Tableau_Scriptable ScriptableTableau;
    public GameObject ParentFloor;



    [Header("Taille Grid")]
    public int GridWidth;
    public int GridHeight;
    public int RangeMinWall;

    [Header("Materials")]
    public Material MaterialObjectCantPlace;
    public Material MaterialObjectCanPlace;
    public Material ActualFloorMaterial;
    public Material ActualWallMaterial;

    private List<GameObject> ListFloorChangeMat;
    private List<GameObject> ListWallChangeMat;
    //Mouse Pos X & Y
    float PosCellX;
    float PosCellZ;

    float LastPosCellX;
    float LastPosCellZ;

    //Mouse Pos Vector
    Vector3 CoordoneeCurseur { get; set; }
    Vector3 LastCoordoneeCurseur { get; set; }

    //PosToStartConstruct
    Vector3 PosStartConstruct { get; set; }



    //Element unique de preview Wall
    public GameObject ActualPosFakeWall { get; set; }
    public GameObject LastPosFakeWall { get; set; }
    //Element unique de preview Tableau
    public GameObject ActualPosFakeTableau { get; set; }
    public GameObject LastPosFakeTableau { get; set; }


    //Permet de connaître l'élément de la grille que le joueur à over (ainsi que l'ancien pour enlever l'effet de over)
    public GameObject ActualOverGO { get; set; }
    public GameObject PreviousOverGO { get; set; }
    public GameObject ActualConstructGO { get; set; }
    public GameObject ActualChangeMatFloor { get; set; }
    public GameObject ActualChangeMatWall { get; set; }

    //
    private bool rotateObj { get; set; }
    private bool canPlaceObj { get; set; }
    private bool canPlaceTableau { get; set; }
    private bool SkipConstruct { get; set; }

    //Rajoute 0.5 en X ou Z pour snap les murs sur la grille
    private float diffbloc = 0.5f;

    //Tiles de sol X-Z Start-X-Z End
    public GameObject[,,,] FirstFloorElem { get; set; }

    //Grid de Mur Verticaux ou horizontaux
    [SerializeField]
    public GameObject[,] FirstFloorHorizontalWall { get; set; }
    public GameObject[,] FirstFloorVerticalWall { get; set; }


    public List<GameObject> FirstFloorWall { get; set; }

    //permet de visualiser un ensemble de mur sur la grille (preview)
    public List<GameObject> GridPreviewFakeWall { get; set; }
    public List<GameObject> PreviousGridPreviewFakeWall { get; set; }

    //Contenaire des blocs de mur preview
    public List<GameObject> GridPreviewFakeWallObject { get; set; }

    //permet de connaitre le le premier élément et dernier de grille pour créer une forme (ligne et quad ici)
    public Vector3 FirstElemFakeGrid { get; set; }
    public Vector3 LastElemFakeGrid { get; set; }

    public GameObject RefWallForTableau { get; set; }
    public int isFront;
    GameObject replacePilier;
    //Enum pour savoir comment un mur est créer
    public PositionCreationLine LineConstruct;

    //public AstarPath aStar;

    public bool[] canConstructSalle;
    //[Header("Reference")]
    private GameLibrary GameLibRef;
    private SalleV2 SalleV2Ref;
    private UIController UIcontrolRef;
    QuestManager refQuestManager;
    NarateurManager refNarrateur;


    private GameObject TabToDeplace;
    private GameObject refMenuTab;
    private int newIdSalle;

    private SoundManager refSM;
    public enum PositionCreationLine
    {
        None = 0,
        Up = 1,
        Down = 2,
        Right = 3,
        Left = 4
    }
    public PhasePosTableau PhaseTab { get; set; }
    public enum PhasePosTableau
    {
        PreviewOrNone = 0,
        FinalPos = 1
    }

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        Cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        FirstFloorElem = new GameObject[GridWidth, GridHeight, GridWidth, GridHeight];

        FirstFloorHorizontalWall = new GameObject[GridWidth+1, GridHeight+1];
        FirstFloorVerticalWall = new GameObject[GridWidth+1, GridHeight+1];

        GridPreviewFakeWall = new List<GameObject>();
        PreviousGridPreviewFakeWall = new List<GameObject>();
        GridPreviewFakeWallObject = new List<GameObject>();
        FirstFloorWall = new List<GameObject>();

        ListFloorChangeMat = new List<GameObject>();
        ListWallChangeMat = new List<GameObject>();

        


    }
    void Start()
    {
        GameLibRef = GameLibrary._instance;
        refSM = SoundManager._instance;
        SalleV2Ref = SalleV2._instance;
        UIcontrolRef = UIController._instance;
        refQuestManager = QuestManager._instance;
        refNarrateur = NarateurManager._instance;

        canConstructSalle = new bool[SalleV2Ref.NbrSalle];
        canConstructSalle[1] = true;
        GridElement gridGame = new GridElement(GridWidth, GridHeight);

        Wall = UIController._instance.ArrayWallPrefab[0];
        FakeWall = UIController._instance.ArrayPreviewWallPrefab[0];

        ActualFloorMaterial = UIController._instance.ArrayMaterialFloor[0];
        ActualWallMaterial = UIController._instance.ArrayMaterialWall[0];


    }

    void Update()
    {
        if (UIController._instance.CanModifiateTerrain)
        {

            //initialistion de la grille de preview et du premier élément de sélection
            if (UIController._instance.CanCreateWall && ActualOverGO != null)
            {
                //Initialise la position du pilier et la création de mur
                if (Input.GetKeyDown(KeyCode.Mouse0))
                {
                    if (!EventSystem.current.IsPointerOverGameObject())
                    {
                        FirstElemFakeGrid = ActualOverGO.transform.position;
                    }

                }
            }

            // Check if the mouse was clicked over a UI element
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                GetCoordonee();
            }
            

            //if(UIController.i,)
            CreateWall();
            CreateTableau();
            MoveTableau();
            


            RotatePreview();

            //Destroy Wall // Faire en sorte d'afficher une pop up si un mur supprimer possède un tableau
            if (Input.GetKeyDown(KeyCode.Mouse0) && UIController._instance.CanDestroyWall && ActualOverGO != null)
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (ActualOverGO.tag == "Wall")
                    {
                        if (!ActualOverGO.transform.parent.gameObject.GetComponent<WallScript>().IsBorder)
                        {
                            var parentGo = ActualOverGO.transform.parent.gameObject;
                            if (parentGo.GetComponent<WallScript>().RefTableauBehind.Count > 0 || parentGo.GetComponent<WallScript>().RefTableauFront.Count > 0)
                            {
                                if (parentGo.GetComponent<WallScript>().RefTableauBehind.Count > 0)
                                {
                                    foreach (var item in parentGo.GetComponent<WallScript>().RefTableauBehind)
                                    {
                                        Destroy(item);
                                    }

                                }

                                if (parentGo.GetComponent<WallScript>().RefTableauFront.Count > 0)
                                {
                                    foreach (var item in parentGo.GetComponent<WallScript>().RefTableauFront)
                                    {
                                        Destroy(item);
                                    }

                                }

                                refSM.RangerUnTableau();
                            }
                            Destroy(parentGo);
                        }
                    }
                }
                
            }

            //RecupererTableau
            if(Input.GetKeyDown(KeyCode.Mouse0) && UIController._instance.CanDestroyTab && ActualOverGO != null)
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    if (ActualOverGO.tag == "tableau")
                    {
                        var TabScript = ActualOverGO.GetComponent<DisplayTableau>().Tableau;
                        var TitleTab = TabScript.titre;

                        Proto_Inventory._instance.Inventaire.Add(AddTableauInInventory(SalleV2Ref.ListTabSalles[TabScript.idSalle], TitleTab));


                        SalleV2Ref.ListTabSalles[TabScript.idSalle].Remove(RemoveTableauInListSalle(SalleV2Ref.ListTabSalles[TabScript.idSalle], TitleTab));
                        SalleV2Ref.ListTabSalles_GO[TabScript.idSalle].Remove(RemoveTableauInListSalleGO(SalleV2Ref.ListTabSalles_GO[TabScript.idSalle], TitleTab));



                        RemoveTableauOnListPourCoef(TabScript);

                        SalleV2Ref.CalculateSalleCoef(TabScript.idSalle);

                        SalleV2Ref.NbrTableauParSalle[TabScript.idSalle]++;

                        Destroy(ActualOverGO);
                        refSM.RangerUnTableau();
                    }
                }
                
            }

            //Reset Construction/ Destruction(?)
            if (Input.GetKeyDown(KeyCode.Mouse1) && IsConstruct && !UIController._instance.CanPosTableau)
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    StopPreviewMouseExitConstruct();
                    SkipConstruct = true;
                }
                
            }

            if(Input.GetKeyDown(KeyCode.Mouse1) && PhaseTab == PhasePosTableau.FinalPos && UIController._instance.CanPosTableau)
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    ResetTableau();
                }
                
            }


            
        }
    }

    private Proto_Tableau_Scriptable RemoveTableauInListSalle(List<Proto_Tableau_Scriptable> listTabSalle, string nameTab)
    {
        for (int i =0; i < listTabSalle.Count; i++)
        {

                if (listTabSalle[i].titre == nameTab)
                {
                    return listTabSalle[i];
                }
            
            
        }

        return null;
    }
    private GameObject RemoveTableauInListSalleGO(List<GameObject> listTabSalleGO, string nameTab)
    {
        for (int i = 0; i < listTabSalleGO.Count; i++)
        {
            if (listTabSalleGO[i].GetComponent<DisplayTableau>().Tableau.titre == nameTab)
            {
                return listTabSalleGO[i];
            }


        }

        return null;
    }

    private Proto_Tableau_Scriptable AddTableauInInventory(List<Proto_Tableau_Scriptable> listTabSalle, string nameTab)
    {
            for (int j = 0; j < GameLibRef.ListEveryTableau.Count; j++)
            {
                if (GameLibRef.ListEveryTableau[j].titre == nameTab)
                {
                    return GameLibRef.ListEveryTableau[j];
                }
            }


        return null;
    }

    public void GetCoordonee()
    {

        Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {


            if (hit.collider.tag == "Wall" || hit.collider.tag == "Floor" || hit.collider.tag == "tableau")
            {
                if (hit.collider.gameObject != ActualOverGO)
                {
                    if (ActualOverGO == null)
                    {
                        ActualOverGO = hit.collider.gameObject;
                    }
                    else
                    {
                        PreviousOverGO = ActualOverGO;
                        ActualOverGO = hit.collider.gameObject;
                    }
                }
                ApplyModifTextureFloor();
                ApplyModifTextureWall();


            }
            else
            {
                StopTexture();
            }

          

            if (hit.collider.tag == "Floor")
            {
                var xPoint = Mathf.FloorToInt(hit.point.x);
                var yPoint = Mathf.Floor(hit.point.y);
                var zPoint = Mathf.FloorToInt(hit.point.z);
                var newCoord = new Vector3(xPoint, yPoint + 0.5f, zPoint);
                


                if (CoordoneeCurseur == Vector3.zero || CoordoneeCurseur == null)
                    {
                        CoordoneeCurseur = newCoord;
                        PosCellX = (Mathf.FloorToInt(hit.point.x) - hit.point.x);
                        PosCellZ = (Mathf.FloorToInt(hit.point.z) - hit.point.z);
                        ActualOverGO = hit.collider.gameObject;
                    }
                    else
                    {
                        LastCoordoneeCurseur = CoordoneeCurseur;
                        CoordoneeCurseur = newCoord;

                        PosCellX = (hit.point.x - Mathf.FloorToInt(hit.point.x));
                        PosCellZ = (hit.point.z - Mathf.FloorToInt(hit.point.z));

                        canPlaceObj = true;
                    }
                }
                else if (hit.collider.tag == "Wall")
                {
                    var xPoint = Mathf.FloorToInt(hit.point.x);
                    var yPoint = Mathf.Floor(hit.point.y);
                    var zPoint = Mathf.FloorToInt(hit.point.z);
                    var newCoord = new Vector3(xPoint, yPoint + 0.5f, zPoint);

                    if (CoordoneeCurseur == Vector3.zero || CoordoneeCurseur == null)
                    {
                        CoordoneeCurseur = newCoord;
                        PosCellX = (Mathf.FloorToInt(hit.point.x) - hit.point.x);
                        PosCellZ = (Mathf.FloorToInt(hit.point.z) - hit.point.z);
                    }
                    else
                    {

                        LastCoordoneeCurseur = CoordoneeCurseur;
                        CoordoneeCurseur = newCoord;

                        PosCellX = (hit.point.x - Mathf.FloorToInt(hit.point.x));
                        PosCellZ = (hit.point.z - Mathf.FloorToInt(hit.point.z));

                        canPlaceObj = false;

                    }
                }
                else
                {

                //StopPreviewMouseExitConstruct();

                }


                if (UIController._instance.CanCreateWall && ActualOverGO != null && !SkipConstruct)
                {
                    PreviewWall(hit.collider.gameObject);
                }
            if (UIController._instance.CanPosTableau && ActualOverGO != null ) 
            {

                if (hit.collider.tag == "Wall")
                        {
                            if (canConstructSalle[hit.collider.gameObject.GetComponent<WallMaterial>().IdSalle])
                            {
                                PreviewTableau(hit.collider.name, hit.collider.gameObject.transform.parent.gameObject, hit.point);
                            }
                            else
                            {
                            ResetTableau();
                            }

                }
            }

            if (UIController._instance.CanMoveTab && ActualOverGO != null)
            {
                if (hit.collider.tag == "Wall")
                {
                    if (canConstructSalle[hit.collider.gameObject.GetComponent<WallMaterial>().IdSalle])
                    {
                        PreviewTableau(hit.collider.name, hit.collider.gameObject.transform.parent.gameObject, hit.point);
                    }
                    else
                    {
                        ResetTableau();
                    }

                }



            }

            

            if (UIController._instance.CanChangeMatFloor && ActualOverGO != null)
            {

                if (hit.collider.tag == "Floor") 
                {
                    if (canConstructSalle[hit.collider.gameObject.GetComponent<FloorControler>().IdSalle]) PreviewFloorMaterial();
                    else StopTexture();
                }
            }

            if (UIController._instance.CanChangeMatWall && ActualOverGO != null)
            {
                if (hit.collider.tag == "Wall") 
                {
                    if (canConstructSalle[hit.collider.gameObject.GetComponent<WallMaterial>().IdSalle]) PreviewWallMaterial(hit.collider.gameObject);
                    else StopTexture();
                    
                }
            }
        }
            else
            {

            StopTexture();
            }

        }
    

        public void RotatePreview()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                rotateObj = rotateObj ? false : true;
            }
        }

        public void PreviewWall(GameObject GoOver)
        {
            if (Input.GetKey(KeyCode.Mouse0))
            {

            if (ActualOverGO != ActualConstructGO)
            {
                ActualConstructGO = ActualOverGO;


                LastElemFakeGrid = ActualOverGO.transform.position;
                IsConstruct = true;

                if (GridPreviewFakeWallObject.Count > 0)
                {
                    foreach (var fake in GridPreviewFakeWallObject)
                    {
                        Destroy(fake);
                    }
                }


                GridPreviewFakeWallObject.Clear();
                GridPreviewFakeWall.Clear();


                switch (UIController._instance.overridMode)
                {
                    case UIController.OverrideMode.none:

                        break;

                    case UIController.OverrideMode.line:
                        generateWallLine();

                        break;

                    case UIController.OverrideMode.quad:

                        break;
                }
            }
            
        }
        else
        {
            if (canPlaceObj && UIController._instance.CanCreateWall)
            {

                if(PosCellX != LastPosCellX || PosCellZ != LastPosCellZ)
                {

                    LastPosCellX = PosCellX;
                    LastPosCellZ = PosCellZ;

                    Destroy(ActualPosFakeWall);
                    ActualPosFakeWall = null;

                    ActualPosFakeWall = CreateWallFromMouse(Pilier);

                }
                
            }
            else
            {
                if (ActualPosFakeWall != null)
                {
                    Destroy(ActualPosFakeWall);
                }

                if (LastPosFakeWall != null)
                {
                    Destroy(LastPosFakeWall);
                }
            }
            ActualConstructGO = null;
        }



    }

    public void PreviewTableau(string colliderName, GameObject Wall, Vector3 pos)
    {

        //print("au dessus");
        if (RefWallForTableau != Wall)
        {
            //print("au dessous");

            WallScript.OrientationWall oldOrientation = WallScript.OrientationWall.Horizontal;
            Vector3 oldPos = Vector3.zero;
            if (RefWallForTableau != null)
            {
                oldOrientation = RefWallForTableau.GetComponent<WallScript>().orientation;
                oldPos = RefWallForTableau.transform.position;
            }
            

            RefWallForTableau = Wall;
            if(oldPos != Vector3.zero)
            {
                if (RefWallForTableau.GetComponent<WallScript>().orientation != oldOrientation)
                {
                    if (ActualPosFakeTableau != null) Destroy(ActualPosFakeTableau);

                    ActualPosFakeTableau = null;
                }
                else
                {
                    if (oldOrientation == WallScript.OrientationWall.Horizontal)
                    {

                        if (oldPos.x != RefWallForTableau.transform.position.x)
                        {
                            
                            if (ActualPosFakeTableau != null) Destroy(ActualPosFakeTableau);

                            ActualPosFakeTableau = null;
                        }
                    }
                    else
                    {
                        
                        if (oldPos.z != RefWallForTableau.transform.position.z)
                        {
                            
                            if (ActualPosFakeTableau != null) Destroy(ActualPosFakeTableau);

                            ActualPosFakeTableau = null;
                        }
                    }
                }
            }
            else
            {
                if (ActualPosFakeTableau != null) Destroy(ActualPosFakeTableau);

                ActualPosFakeTableau = null;
            }
            

            


            //0 pas un bon collider / 1 Front / 2 Behind
            
            //Behind puis front

            
           
                isFront = 0;
                switch (colliderName)
                {
                    case "FrontWall":
                        isFront = 1;
                        break;
                    case "BehindWall":
                        isFront = 2;
                        break;

                }
                //print(isFront);
                if (isFront == 0) return;
            int idSalle = 0;
            idSalle = isFront == 1 ? Wall.transform.GetChild(1).gameObject.GetComponent<WallMaterial>().IdSalle : Wall.transform.GetChild(0).gameObject.GetComponent<WallMaterial>().IdSalle;
            newIdSalle = idSalle;
            
            

            //Check le tableau selectionner, en fonction de ça taille, check les murs adjacents


            var largeurTab = ScriptableTableau.LargeurTableau;
            var hauteurTab = ScriptableTableau.HauteurTableau;

            var ratio = hauteurTab / largeurTab;

            var largeurTabRounded = calculScaleTab(largeurTab); /*Mathf.FloorToInt(ScaleLargeur(largeurTab)) + 1;*/
             var hauteurTabRounded = calculScaleTab(hauteurTab);

            var tabLarg = ScaleLargeur(largeurTab);
            var tabHaut = ScaleHauteur(tabLarg, ratio);
   
            if (ActualPosFakeTableau == null)
            {
                ActualPosFakeTableau = CreatePreviewTableau(isFront, Wall, largeurTabRounded, hauteurTabRounded, tabLarg, tabHaut, idSalle);

            }
        }
        else
        {

            if(ActualPosFakeTableau != null)
            {
                var hauteur = ScriptableTableau.HauteurTableau;
                var largeur = ScriptableTableau.LargeurTableau;
                var ratio = hauteur / largeur;

                var tabLarg = ScaleLargeur(largeur);
                var hauteurTab = ScaleHauteur(tabLarg, ratio);

                var largeurTabRounded = calculScaleTab(largeur);
                float newHauteur = pos.y;
                if (pos.y - (1 * hauteurTab / 2) <= 1f)
                {
                    newHauteur = (1 * hauteurTab / 2) + 1f;
                }
                else if (pos.y + (1 * hauteurTab / 2) >= 4f)
                {
                    newHauteur = (5f - (1 * hauteurTab / 2)) - 1f;

                }


                var valueX = isFront == 1 ? Wall.transform.GetChild(2).position : Wall.transform.GetChild(3).position;

                if (Wall.GetComponent<WallScript>().orientation == WallScript.OrientationWall.Horizontal)
                {
                    ActualPosFakeTableau.transform.position = new Vector3(pos.x, newHauteur, valueX.z);
                }
                else
                {
                    ActualPosFakeTableau.transform.position = new Vector3(valueX.x, newHauteur, pos.z);
                }





                int iteration = 0;
                bool canPos = false;
                var idSalle = isFront == 1 ? Wall.transform.GetChild(1).gameObject.GetComponent<WallMaterial>().IdSalle : Wall.transform.GetChild(0).gameObject.GetComponent<WallMaterial>().IdSalle;
                canPos = SalleV2Ref.NbrTableauParSalle[idSalle] <= 0 ? false : true;

                if (largeurTabRounded % 2 == 0)
                {
                    iteration = largeurTabRounded / 2;


                    if(canPos)canPos = isNextWallsValid(iteration, isFront, /*RefWallForTableau*/Wall.GetComponent<WallScript>());
                    if (canPos) ChangeColorPreview(ActualPosFakeTableau.GetComponent<PreRenderTableauCol>().IsCollision, ActualPosFakeTableau);
                    else ChangeColorPreview(true, ActualPosFakeTableau);

                }
                else
                {
                    iteration = (largeurTabRounded + 1) / 2;


                    if (canPos) canPos = isNextWallsValid(iteration, isFront, /*RefWallForTableau*/Wall.GetComponent<WallScript>());
                    if (canPos) ChangeColorPreview(ActualPosFakeTableau.GetComponent<PreRenderTableauCol>().IsCollision, ActualPosFakeTableau);
                    else ChangeColorPreview(true, ActualPosFakeTableau);
                }
            }
            else
            {
                RefWallForTableau = null;
            }
            

            


            //ActualPosFakeTableau.transform.position = new Vector3(CoordoneeCurseur.x, CoordoneeCurseur.y, CoordoneeCurseur.z) ;
            //}


        }

        
    }

    private void ChangeColorPreview(bool canCreate, GameObject obj)
    {
        if (!canCreate)
        {
            var mat = obj.transform.GetChild(1).GetComponent<Renderer>().materials;
            mat[0] = MaterialObjectCanPlace;
            mat[1] = MaterialObjectCanPlace;

            obj.transform.GetChild(1).GetComponent<Renderer>().materials = mat;
            canPlaceTableau = true;
        }
        else
        {
            var mat = obj.transform.GetChild(1).GetComponent<Renderer>().materials;
            mat[0] = MaterialObjectCantPlace;
            mat[1] = MaterialObjectCantPlace;

            obj.transform.GetChild(1).GetComponent<Renderer>().materials = mat;
            canPlaceTableau = false;
        }
    }


    public GameObject CreatePreviewTableau(int PosTableau, GameObject WallRef, int largeurTabRounded, int hauteurTabRounded, float largeurTab, float hauteurTab, int idSalle)
    {
        var refWallScript = WallRef.GetComponent<WallScript>();


        //Si Orientation vertical & front 
        //Front ==1
        //Behind == 2

        print($"idSalle : {idSalle}");
        print($"NbrTableauParSalle : {SalleV2Ref.NbrTableauParSalle[idSalle]}");

        if (PosTableau == 1)
        {
            
            if (SalleV2Ref.NbrTableauParSalle[idSalle] <= 0)
            {
                return ReturnPreViewTab(false, 1, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
            }
            else
            {
                if (refWallScript.CantPlaceTableauFront)
                {
                    return ReturnPreViewTab(false, 1, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
                }
                else
                {
                    if (refWallScript.WallRemain >= hauteurTab)
                    {
                        return ReturnPreViewTab(true, 1, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
                    }
                    else
                    {
                        //print("4");
                        return ReturnPreViewTab(false, 1, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);

                    }
                }
            }

            
            
        }
        else
        {
            if (SalleV2Ref.NbrTableauParSalle[idSalle] <= 0)
            {
                return ReturnPreViewTab(false, 2, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
            }
            else
            {
                if (refWallScript.CantPlaceTableauBehind)
                {
                    return ReturnPreViewTab(false, 2, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
                }
                else
                {
                    if (/*!WallRef.GetComponent<WallScript>().HaveTableauBehind*/refWallScript.WallRemain >= hauteurTab)
                    {
                        return ReturnPreViewTab(true, 2, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
                    }
                    else
                    {
                        return ReturnPreViewTab(false, 2, refWallScript, WallRef, largeurTabRounded, hauteurTabRounded, largeurTab, hauteurTab);
                    }
                }
            }
            
            
        }
    }

    public GameObject ReturnPreViewTab(bool canCreate, int posTab, WallScript refWallScript, GameObject WallRef, int largeurTab, int hauteurTab, float realLarge, float realHaut)
    {
        Vector3 newPos;
        //print($"largeurTab : {largeurTab}");
        bool canPos = true;
        if (posTab == 1)
        {
            newPos = refWallScript.PosTableauFront.transform.position;
        }
        else
        {
            newPos = refWallScript.PosTableauBehind.transform.position;
        }
        //var tabLarg = Mathf.FloorToInt(largeurTab) + 1;
        //var tabHaut = Mathf.FloorToInt(hauteurTab) + 1;
        int iteration;
        if (largeurTab % 2 == 0)
        {
            iteration = largeurTab / 2;
            canPos = isNextWallsValid(iteration, posTab, refWallScript);


        }
        else
        {
            iteration = (largeurTab + 1) / 2;
            canPos = isNextWallsValid(iteration, posTab, refWallScript);
        }

        if (canCreate)
        {
            //print(canPos);
            canCreate = canPos;
        }
        

        switch (posTab)
        {
            case 1:
                if (canCreate)
                {
                    var tableauRef = refWallScript.orientation == WallScript.OrientationWall.Vertical ? Instantiate(FakeTableau, /*refWallScript.PosTableauFront.transform.position*/newPos, Quaternion.identity) : Instantiate(FakeTableau, /*refWallScript.PosTableauFront.transform.position*/newPos, Quaternion.Euler(0, 90, 0));
                    //var mat = tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials;
                    //mat[0] = MaterialObjectCanPlace;
                    //mat[1] = MaterialObjectCanPlace;

                    //tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials = mat;
                    //canPlaceTableau = true;

                    if (canPlaceTableau) ChangeColorPreview(tableauRef.GetComponent<PreRenderTableauCol>().IsCollision, tableauRef);

                    tableauRef.transform.localScale = /*tableauRef.transform.localScale * 0.5f;*/new Vector3(/*Tableau.ProfondeurTableau*/1,  realHaut/** 0.5f*/, realLarge /** 0.5f*/);

                    return tableauRef;
                }
                else
                {
                    var tableauRef = refWallScript.orientation == WallScript.OrientationWall.Vertical ? Instantiate(FakeTableau, /*refWallScript.PosTableauFront.transform.position*/newPos, Quaternion.identity) : Instantiate(FakeTableau, /*refWallScript.PosTableauFront.transform.position*/newPos, Quaternion.Euler(0, 90, 0));
                    var mat = tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials;
                    mat[0] = MaterialObjectCantPlace;
                    mat[1] = MaterialObjectCantPlace;

                    tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials = mat;

                    tableauRef.transform.localScale = /*tableauRef.transform.localScale * 0.5f;*/new Vector3(/*Tableau.ProfondeurTableau*/1, realHaut /** 0.5f*/, realLarge /** 0.5f*/);

                    canPlaceTableau = false;

                    return tableauRef;
                }

            case 2:
                if (canCreate)
                {
                    var tableauRef = refWallScript.orientation == WallScript.OrientationWall.Vertical ? Instantiate(FakeTableau, /*refWallScript.PosTableauBehind.transform.position*/newPos, Quaternion.Euler(0, 180, 0)) : Instantiate(FakeTableau, /*refWallScript.PosTableauBehind.transform.position*/newPos, Quaternion.Euler(0, -90, 0));
                    //var mat = tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials;
                    //mat[0] = MaterialObjectCanPlace;
                    //mat[1] = MaterialObjectCanPlace;

                    //tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials = mat;
                    //canPlaceTableau = true;

                    if (canPlaceTableau) ChangeColorPreview(tableauRef.GetComponent<PreRenderTableauCol>().IsCollision, tableauRef);

                    tableauRef.transform.localScale = /*tableauRef.transform.localScale * 0.5f;*/new Vector3(/*Tableau.ProfondeurTableau*/1, realHaut /** 0.5f*/, realLarge /** 0.5f*/);

                    return tableauRef;
                }
                else
                {
                    var tableauRef = refWallScript.orientation == WallScript.OrientationWall.Vertical ? Instantiate(FakeTableau, /*refWallScript.PosTableauBehind.transform.position*/newPos, Quaternion.Euler(0, 180, 0)) : Instantiate(FakeTableau, /*refWallScript.PosTableauBehind.transform.position*/newPos, Quaternion.Euler(0, -90, 0));
                    var mat = tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials;
                    mat[0] = MaterialObjectCantPlace;
                    mat[1] = MaterialObjectCantPlace;

                    tableauRef.transform.GetChild(1).GetComponent<Renderer>().materials = mat;
                    canPlaceTableau = false;

                    tableauRef.transform.localScale = /*tableauRef.transform.localScale * 0.5f;*/new Vector3(/*Tableau.ProfondeurTableau*/1, realHaut /** 0.5f*/, realLarge /** 0.5f*/);

                    return tableauRef;
                }

               

        }

        return null;
        
    }

    public int calculScaleTab(float elemToScale)
    {
        if (elemToScale < 1.5f) return 1;
        if (elemToScale < 2.5f) return 1;
        else if (elemToScale < 3.5f) return 2;
        else if (elemToScale < 4.5f) return 2;

        return 0;

    }


    public float ScaleLargeur(float Largeur)
    {
        if (Largeur < 1.25f) return 1;
        else if (Largeur < 1.75f) return 1.15f;
        else if (Largeur < 2.25f) return 1.3f;
        else if (Largeur < 2.75f) return 1.45f;
        else if (Largeur < 3.25f) return 1.6f;
        else if (Largeur < 3.75f) return 1.8f;
        else if (Largeur < 4.25f) return 2;

        return 0;
    }
    public float ScaleHauteur(float largeur, float ratio)
    {
        return largeur * ratio;
    }

    private bool isNextWallsValid(int iteration, int posTab, WallScript refWallScript)
    {
        for (int i = (-iteration /*+ 1*/); i <= iteration; i++)
        {
            //if (i != 0)
            //{
                if (refWallScript.orientation == WallScript.OrientationWall.Horizontal)
                {
                    if (refWallScript.PosInArray.x + i > 0 && refWallScript.PosInArray.x + i < FirstFloorHorizontalWall.Length)
                    {
                        if(FirstFloorHorizontalWall[(int)refWallScript.PosInArray.x + i, (int)refWallScript.PosInArray.z] != null)
                        {
                            if (posTab == 1)
                            {
                                if (FirstFloorHorizontalWall[(int)refWallScript.PosInArray.x + i, (int)refWallScript.PosInArray.z].GetComponent<WallScript>().CantPlaceTableauFront)
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                if (FirstFloorHorizontalWall[(int)refWallScript.PosInArray.x + i, (int)refWallScript.PosInArray.z].GetComponent<WallScript>().CantPlaceTableauBehind)
                                {

                                    return false;
                                }
                            }
                        }
                        else
                        {

                            return false;
                        }
                        


                    }

                }
                else
                {
                    if (refWallScript.PosInArray.z + i > 0 && refWallScript.PosInArray.z + i < FirstFloorVerticalWall.Length)
                    {
                        if (FirstFloorVerticalWall[(int)refWallScript.PosInArray.x, (int)refWallScript.PosInArray.z+i] != null)
                        {
                            
                            if (posTab == 1)
                            {


                                if (FirstFloorVerticalWall[(int)refWallScript.PosInArray.x, (int)refWallScript.PosInArray.z+i].GetComponent<WallScript>().CantPlaceTableauFront)
                                {

                                    return false;
                                }


                            }
                            else
                            {
                                if (FirstFloorVerticalWall[(int)refWallScript.PosInArray.x, (int)refWallScript.PosInArray.z+i].GetComponent<WallScript>().CantPlaceTableauBehind)
                                {

                                    return false;
                                }
                            }
                        }
                        else
                        {

                            return false;
                        }
                            


                    }
                }
            }
            
        //}
        return true;
    }

    public void PositionPreviewTableau(Vector3 posCurseur)
    {
        if(PhaseTab == PhasePosTableau.FinalPos)
        {

            var posFakeTab = ActualPosFakeTableau.transform.position;

            //var hauteurTab = ScriptableTableau.HauteurTableau;

            var hauteur = ScriptableTableau.HauteurTableau;
            var largeur = ScriptableTableau.LargeurTableau;
            var ratio = hauteur / largeur;

            var tabLarg = ScaleLargeur(largeur);
            var hauteurTab = ScaleHauteur(tabLarg, ratio);

            float newHauteur=posCurseur.y;
            //print($"HauteurTab : {hauteurTab}");
            //print($"HauteurTabTruc : {1 * hauteurTab / 2}");
            
            if(posCurseur.y- (1 * hauteurTab/2) <=1f)
            {
                //print("1");
                //posCurseur.y = hauteurTab/2+0.5f;
                newHauteur = (1 * hauteurTab / 2) + 1f;
            }
            else if (posCurseur.y + (1 * hauteurTab / 2) >= 4f)
            {
                //print("2");
                newHauteur = (5f - (1 * hauteurTab / 2)) - 1f;
                //posCurseur.y = (5f- hauteurTab/2) -0.5f;
            }
            //print($"newHauteur : {newHauteur}");

            posFakeTab = new Vector3(posFakeTab.x, newHauteur, posFakeTab.z);
            //print($"Coord : {posCurseur}");

            ActualPosFakeTableau.transform.position = posFakeTab;
            if (!ActualPosFakeTableau.GetComponent<PreRenderTableauCol>().IsCollision)
            {

                
                var mat = ActualPosFakeTableau.transform.GetChild(1).GetComponent<Renderer>().materials;
                mat[0] = MaterialObjectCanPlace;
                mat[1] = MaterialObjectCanPlace;

                ActualPosFakeTableau.transform.GetChild(1).GetComponent<Renderer>().materials = mat;

                canPlaceTableau = true;
            }
            else 
            {
                var mat = ActualPosFakeTableau.transform.GetChild(1).GetComponent<Renderer>().materials;
                mat[0] = MaterialObjectCantPlace;
                mat[1] = MaterialObjectCantPlace;

                ActualPosFakeTableau.transform.GetChild(1).GetComponent<Renderer>().materials = mat;

                canPlaceTableau = false;
            }
        }

    }
    public bool IsTableauOver()
    {

        return true;
    } 

        public void CreateWall()
        {

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            if (UIController._instance.CanCreateWall && ActualOverGO != null)
            {
                foreach (var floor in GridPreviewFakeWallObject)
                {
                    if (floor.transform.GetChild(0).GetComponent<Renderer>().material.name.Contains(MaterialObjectCanPlace.name))
                    {
                        //print("CreateWall");
                        var newWall = Instantiate(Wall, floor.transform.position, floor.transform.rotation);
                        var xVal = newWall.transform.position.x;
                        var zVal = newWall.transform.position.z;
                        //refSM.PoserMur();

                        if (newWall.transform.rotation.eulerAngles.y > 0f)
                        {
                            FirstFloorHorizontalWall[(int)xVal, (int)zVal] = newWall;
                            newWall.GetComponent<WallScript>().orientation = WallScript.OrientationWall.Horizontal;
                            newWall.GetComponent<WallScript>().PosInArray = new Vector3(xVal, 0, zVal);
                        }
                        else
                        {
                            FirstFloorVerticalWall[(int)xVal, (int)zVal] = newWall;
                            newWall.GetComponent<WallScript>().orientation = WallScript.OrientationWall.Vertical;
                            newWall.GetComponent<WallScript>().PosInArray = new Vector3(xVal, 0, zVal);
                        }

                        //if (aStar != null)
                        //{
                        //    RegenerePathFinding();

                        //}

                        FirstFloorWall.Add(newWall);
                        
                    }
                    Destroy(floor);

                }
                GridPreviewFakeWallObject.Clear();
                

            }
            SkipConstruct = false;
            IsConstruct = false;
        }



    }

    public void MoveTableau()
    {
        if (UIController._instance.CanMoveTab)
        {
            //Valid Deplacement
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (canPlaceTableau)
                {
                    //CanMove
                    //TabToDeplace.transform.position = ActualPosFakeTableau.transform.position;

                    var wallScript = RefWallForTableau.GetComponent<WallScript>();
                    //print("CreateTableau");
                    var newTab = Instantiate(Tableau, ActualPosFakeTableau.transform.position, ActualPosFakeTableau.transform.rotation);
                    EZCameraShake.CameraShaker.Instance.ShakeOnce(2f, 4f, .1f, .5f);
                    newTab.GetComponent<DisplayTableau>().Tableau = TabToDeplace.GetComponent<DisplayTableau>().Tableau;
                    newTab.GetComponent<DisplayTableau>().GetMaterialsTableau();

                    refSM.TableauPose();

                    if (isFront == 1)
                    {
                        wallScript.RefTableauFront.Add(newTab);
                        //wallScript.WallRemain -= newTab.GetComponent<DisplayTableau>().Tableau.HauteurTableau;

                        //RefWallForTableau.GetComponent<WallScript>().HaveTableauFront = true;
                        var refWallFace = RefWallForTableau.transform.GetChild(1).gameObject.GetComponent<WallMaterial>().IdSalle;
                        AddTableauInListSalle(refWallFace, newTab.GetComponent<DisplayTableau>().Tableau, newTab);
                        SalleV2Ref.NbrTableauParSalle[refWallFace]--;
                    }
                    else if (isFront == 2)
                    {
                        wallScript.RefTableauBehind.Add(newTab);
                        //wallScript.WallRemain -= newTab.GetComponent<DisplayTableau>().Tableau.HauteurTableau;

                        //RefWallForTableau.GetComponent<WallScript>().HaveTableauBehind = true;
                        var refWallFace = RefWallForTableau.transform.GetChild(0).gameObject.GetComponent<WallMaterial>().IdSalle;

                        AddTableauInListSalle(refWallFace, newTab.GetComponent<DisplayTableau>().Tableau, newTab);
                        SalleV2Ref.NbrTableauParSalle[refWallFace]--;

                    }


                    

                    //AddReferenceTabFromSalle(newTab.GetComponent<DisplayTableau>().Tableau, newTab);
                    RemoveReferenceTabFromSalle(TabToDeplace.GetComponent<DisplayTableau>().Tableau, TabToDeplace);




                    Destroy(TabToDeplace);
                    Destroy(refMenuTab);
                    TabToDeplace = null;

                    Destroy(ActualPosFakeTableau);
                    ActualPosFakeTableau = null;

                    UIcontrolRef.MoveTab();
                    toggleElementVisibility();

                    //newTab.GetComponent<DisplayTableau>().reCreateMenu();
                    UIcontrolRef.refMenuTableau = null;

                }
                else
                {
                    //Musique?
                    refSM.TableauNePeutEtrePose();
                }
            }//Annul Deplacement
            else if (Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyDown(KeyCode.Escape))
            {
                UIcontrolRef.MoveTab();
                SalleV2Ref.NbrTableauParSalle[TabToDeplace.GetComponent<DisplayTableau>().Tableau.idSalle]--;
                toggleElementVisibility();
            }
        }
    }

    //Appeler cette fonction lorsqu'on veut déplacer un tableau
    public void enableMoveTab(GameObject refElemToDeplace, GameObject refMenuTab)
    {
        TabToDeplace = refElemToDeplace;
        this.refMenuTab = refMenuTab;
        UIcontrolRef.MoveTab();
        SalleV2Ref.NbrTableauParSalle[TabToDeplace.GetComponent<DisplayTableau>().Tableau.idSalle]++;
        toggleElementVisibility();
    }

    public void ResetMenuTableau()
    {
        Destroy(UIcontrolRef.refMenuTableau);
        UIcontrolRef.refMenuTableau = null;
    }
    public void toggleElementVisibility()
    {
        //TabToDeplace = elem;
        if (TabToDeplace != null)
        {
            if (TabToDeplace.activeSelf)
            {
                TabToDeplace.SetActive(false);
                refMenuTab.SetActive(false);
            }
            else
            {
                TabToDeplace.SetActive(true);
                refMenuTab.SetActive(true);
            }
        }

        
    }

    //Appeler cette fonction lorsqu'on veut supprimer un tableau
    public void ReturnTabToInventory(Proto_Tableau_Scriptable refTab, GameObject refGO)
    {
        
        //var refGO = TabToDeplace;
        //var refTab = TabToDeplace.GetComponent<DisplayTableau>().Tableau;

        var TitleTab = refTab.titre;

        Proto_Inventory._instance.Inventaire.Add(AddTableauInInventory(SalleV2Ref.ListTabSalles[refTab.idSalle], TitleTab));


        SalleV2Ref.ListTabSalles[refTab.idSalle].Remove(RemoveTableauInListSalle(SalleV2Ref.ListTabSalles[refTab.idSalle], TitleTab));
        SalleV2Ref.ListTabSalles_GO[refTab.idSalle].Remove(RemoveTableauInListSalleGO(SalleV2Ref.ListTabSalles_GO[refTab.idSalle], TitleTab));



        RemoveTableauOnListPourCoef(refTab);

        SalleV2Ref.CalculateSalleCoef(refTab.idSalle);

        SalleV2Ref.NbrTableauParSalle[refTab.idSalle]++;

        Destroy(refGO);
        refSM.RangerUnTableau();
    }


    public void RemoveReferenceTabFromSalle(Proto_Tableau_Scriptable refTab, GameObject refGO)
    {
        var TitleTab = refTab.titre;

        SalleV2Ref.ListTabSalles[refTab.idSalle].Remove(RemoveTableauInListSalle(SalleV2Ref.ListTabSalles[refTab.idSalle], TitleTab));
        SalleV2Ref.ListTabSalles_GO[refTab.idSalle].Remove(RemoveTableauInListSalleGO(SalleV2Ref.ListTabSalles_GO[refTab.idSalle], TitleTab));



        RemoveTableauOnListPourCoef(refTab);

        SalleV2Ref.CalculateSalleCoef(refTab.idSalle);

        SalleV2Ref.NbrTableauParSalle[refTab.idSalle]++;
    }


    public void AddReferenceTabFromSalle(Proto_Tableau_Scriptable refTab, GameObject refGO)
    {

        //RemoveTableauOnListPourCoef(refTab);
        AddTableauOnListPourCoef(refTab);

        SalleV2Ref.CalculateSalleCoef(refTab.idSalle);

        SalleV2Ref.NbrTableauParSalle[refTab.idSalle]--;
    }


    public void CreateTableau()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            // Check if the mouse was clicked over a UI element
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (UIController._instance.CanPosTableau && ActualPosFakeTableau != null && canPlaceTableau)
                {
                    var wallScript = RefWallForTableau.GetComponent<WallScript>();
                    //print("CreateTableau");
                    var newTab = Instantiate(Tableau, ActualPosFakeTableau.transform.position, ActualPosFakeTableau.transform.rotation);
                    EZCameraShake.CameraShaker.Instance.ShakeOnce(2f, 4f, .1f, .5f);
                    newTab.GetComponent<DisplayTableau>().Tableau = ScriptableTableau;
                    newTab.GetComponent<DisplayTableau>().GetMaterialsTableau();

                    refSM.TableauPose();

                    if (isFront == 1)
                    {
                        wallScript.RefTableauFront.Add(newTab);
                        //wallScript.WallRemain -= newTab.GetComponent<DisplayTableau>().Tableau.HauteurTableau;

                        //RefWallForTableau.GetComponent<WallScript>().HaveTableauFront = true;
                        var refWallFace = RefWallForTableau.transform.GetChild(1).gameObject.GetComponent<WallMaterial>().IdSalle;
                        AddTableauInListSalle(refWallFace, newTab.GetComponent<DisplayTableau>().Tableau, newTab);
                    }
                    else if (isFront == 2)
                    {
                        wallScript.RefTableauBehind.Add(newTab);
                        //wallScript.WallRemain -= newTab.GetComponent<DisplayTableau>().Tableau.HauteurTableau;

                        //RefWallForTableau.GetComponent<WallScript>().HaveTableauBehind = true;
                        var refWallFace = RefWallForTableau.transform.GetChild(0).gameObject.GetComponent<WallMaterial>().IdSalle;

                        AddTableauInListSalle(refWallFace, newTab.GetComponent<DisplayTableau>().Tableau, newTab);

                    }


                    canPlaceObj = false;
                    canPlaceTableau = false;
                    AddTableauOnListPourCoef(ScriptableTableau);



                    PhaseTab = PhasePosTableau.PreviewOrNone;

                    //SalleRef.NbrTableauParSalle[ScriptableTableau.idSalle]--;

                    //SalleV2Ref.CalculateSalleCoef(ScriptableTableau.idSalle);

                    //SalleV2Ref.NbrTableauParSalle[ScriptableTableau.idSalle]--;
                    GameLibrary._instance.PopTableau(newTab);
                    Destroy(ActualPosFakeTableau);
                    ActualPosFakeTableau = null;
                    //SUPPRIMER TABLEAU INVENTAIRE
                    Proto_Inventory._instance.ClearInventory(ScriptableTableau);

                    if (refQuestManager.quests[refQuestManager.activeQuest].tableauxBool)
                    {
                        if (!refQuestManager.quests[refQuestManager.activeQuest].poseTab) refQuestManager.CheckPoseTab();
                    }

                    if (!refNarrateur.isEventGone[3] && refNarrateur.NarrateurEnabled)
                    {
                        refNarrateur.AppelNarrateurEvent(3);
                        refNarrateur.isEventGone[3] = true;
                    }

                }
                else if (!canPlaceTableau)
                {
                    refSM.TableauNePeutEtrePose();
                }
                //SkipConstruct = false;
                //IsConstruct = false;
            }
        }
    }

    public void AddTableauInListSalle(int idSalleRef, Proto_Tableau_Scriptable tab,GameObject tabGO)
    {
        //SalleRef.ListTabSalles[idSalleRef].Add(tab);
        //SalleRef.ListTabSalles_GO[idSalleRef].Add(tabGO);

        //ajout de la V2
        SalleV2Ref.ListTabSalles[idSalleRef].Add(tab);
        SalleV2Ref.ListTabSalles_GO[idSalleRef].Add(tabGO);

        ScriptableTableau.idSalle = idSalleRef;



        AddTableauOnListPourCoef(tab);

        SalleV2Ref.CalculateSalleCoef(tab.idSalle);

        SalleV2Ref.NbrTableauParSalle[tab.idSalle]--;
    }

    public void ResetTableau()
    {

        //print("ResetTab");
        if(ActualPosFakeTableau != null) Destroy(ActualPosFakeTableau);

        ActualPosFakeTableau = null;
            PhaseTab = PhasePosTableau.PreviewOrNone;
        RefWallForTableau = null;
    }

    public void StopPreviewMouseExitConstruct()
    {
       //print("ResetAll");
        if(GridPreviewFakeWallObject.Count > 0)
        {
            foreach (var floor in GridPreviewFakeWallObject)
            {
                Destroy(floor);
            }
            GridPreviewFakeWallObject.Clear();
        }

        CoordoneeCurseur = Vector3.zero;
        LastCoordoneeCurseur = Vector3.zero;
        PosCellX = 0;
        PosCellZ = 0;
        canPlaceObj = false;
        PreviousOverGO = null;
        ActualOverGO = null;
        Destroy(ActualPosFakeWall);
        Destroy(LastPosFakeWall);
        ActualConstructGO = null;


        
    }

    public void StopTexture()
    {
        if (ListWallChangeMat != null)
        {
            foreach (var item in ListWallChangeMat)
            {
                item.GetComponent<WallMaterial>().ExitWall();
            }
            ListWallChangeMat.Clear();
        }

        if (ActualChangeMatFloor != null)
        {
            foreach (var item in ListFloorChangeMat)
            {
                item.GetComponent<FloorControler>().ExitFloor();
            }
            ListFloorChangeMat.Clear();
        }
        
        
    }


        //Create a single object (Wall) Preview & construct
        public GameObject CreateWallFromMouse(GameObject objectToConstruct)
        {
        
        if (PosCellX <= 0.5f)
        {
            if(PosCellZ <= 0.5f)
            {
                var newStart = Instantiate(objectToConstruct, new Vector3(CoordoneeCurseur.x, 0, CoordoneeCurseur.z), Quaternion.identity);
                PosStartConstruct = newStart.transform.position;
                return newStart;
            }
            else
            {
                var newStart = Instantiate(objectToConstruct, new Vector3(CoordoneeCurseur.x, 0, CoordoneeCurseur.z +1), Quaternion.identity);
                PosStartConstruct = newStart.transform.position;
                return newStart;
            }
            
        }
        else
        {
            if (PosCellZ <= 0.5f)
            {
                var newStart = Instantiate(objectToConstruct, new Vector3(CoordoneeCurseur.x+1, 0, CoordoneeCurseur.z), Quaternion.identity);
                PosStartConstruct = newStart.transform.position;
                return newStart;
            }
            else
            {
                var newStart = Instantiate(objectToConstruct, new Vector3(CoordoneeCurseur.x+1, 0, CoordoneeCurseur.z+1), Quaternion.identity);
                PosStartConstruct = newStart.transform.position;
                return newStart;
            }
        }
        

    }

        public GameObject CreateWallFromMouse(GameObject objectToConstruct, Vector3 posCreation, Quaternion quaternionObject)
        {
            bool canCreate = true;

        var posX = posCreation.x;
        var posY = posCreation.y;
        var posZ = posCreation.z;


        //if (quaternionObject.eulerAngles.y > 0f)
        //{
        //    if (FirstFloorHorizontalWall[(int)posX, (int)posZ] != null) canCreate = false;
        //}
        //else
        //{
        //    if (FirstFloorVerticalWall[(int)posX, (int)posZ] != null) canCreate = false;
        //}
        //print($"posX = {posX} & posZ = {posZ}");
        //print($"posX = {(int)posX} & posZ = {(int)posZ}");
        for (int i = -RangeMinWall+1; i < RangeMinWall; i++)
        {
            //if (i != 0)
            //{

                if (quaternionObject.eulerAngles.y > 0f)
                {
                    //if (FirstFloorHorizontalWall[(int)posX, (int)posZ] != null) canCreate = false;

                    //print($"Horizontal : {posZ + i >= 0 && posZ+i <=GridHeight+1}");
                    //print($"posZ+i : {posZ + i} & GridHeight+1 : {GridHeight + 1}");

                    if (posZ + i >= 0 && posZ+i <=GridHeight) 
                    {
                        //print($"i : {i} & posZ+i : {posZ + i} & GridHeight+1 : {GridHeight + 1}");
                        
                            if (FirstFloorHorizontalWall[(int)posX, (int)posZ + i] != null) canCreate = false;
                        
                        
                    }
                    
                }
                else
                {
                    //if (FirstFloorVerticalWall[(int)posX, (int)posZ] != null) canCreate = false;
                    //print($"Vertical : {!(posX + i < 0) && !(posX + i > GridWidth + 1)}");

                    if ((posX + i >= 0) && (posX + i <= GridWidth)) 
                    {
                        if (FirstFloorVerticalWall[(int)posX + i, (int)posZ] != null) canCreate = false;
                    }
                    
                }
            //}

        }

        var newWall = Instantiate(objectToConstruct, posCreation, quaternionObject);

        if (!canCreate)
        {
            newWall.transform.GetChild(0).GetComponent<Renderer>().material = MaterialObjectCantPlace;
            newWall.transform.GetChild(1).GetComponent<Renderer>().material = MaterialObjectCantPlace;
        }
        else 
        {
            newWall.transform.GetChild(0).GetComponent<Renderer>().material = MaterialObjectCanPlace;
            newWall.transform.GetChild(1).GetComponent<Renderer>().material = MaterialObjectCanPlace;
        }

        return newWall;
        }

        private void generateWallLine()
        { 

            var firstX = PosStartConstruct.x;
            var firstZ = PosStartConstruct.z;

            var lastX = LastElemFakeGrid.x;
            var lastZ = LastElemFakeGrid.z;



            var diffX = lastX - firstX;
            var diffZ = lastZ - firstZ;

            var Xpos = diffX >= 0 ? true : false;
            var Zpos = diffZ >= 0 ? true : false;

            var diffXPos = Xpos ? diffX : diffX * -1;
            var diffZPos = Zpos ? diffZ : diffZ * -1;

            var diffXPosSupdiffZPos = diffXPos >= diffZPos;

        //if(!Xpos && Zpos) diffXPosSupdiffZPos = !(diffXPos >= diffZPos);
        //print($"Xpos : {diffXPos} & Zpos : {diffZPos}");
        if (!Xpos && Zpos)
        { 
            if(diffZPos + 1 == diffXPos || diffZPos == diffXPos)
            {
                diffXPosSupdiffZPos = false;
                //print("wololo");
            }
            
        }

        if (diffXPosSupdiffZPos)
            {
            var x = 0;
            if (!Xpos) x = 1;

            for (int i = x; i <= diffXPos; i++)
                {

                if (i == diffXPos && Xpos)
                {
                    break;
                }
                var xTofind = Xpos ? firstX + i : firstX - i;
                var zTofind = firstZ;

                var posFloorTofind = new Vector3(xTofind, 0, zTofind);

                var quaternionToCreate = GetRotationWall(diffXPosSupdiffZPos);
                var posToCreate = GetPosWall(posFloorTofind, quaternionToCreate);
                   
                var wall = (CreateWallFromMouse(FakeWall, posToCreate, quaternionToCreate));
                    
                GridPreviewFakeWallObject.Add(wall);
                    

                }
            }
            else
            {
            var z = 0;
            if (!Zpos) z = 1;
            for (int i = z; i <= diffZPos; i++)
                {
                

                if (i == diffZPos && Zpos) 
                {
                    break;
                }
                var xTofind = firstX;
                    var zTofind = Zpos ? firstZ + i : firstZ - i;

                    var posFloorTofind = new Vector3(xTofind, 0, zTofind);

                var quaternionToCreate = GetRotationWall(diffXPosSupdiffZPos);
                var posToCreate = GetPosWall(posFloorTofind, quaternionToCreate);
                    
                    var wall = (CreateWallFromMouse(FakeWall, posToCreate, quaternionToCreate));
                        GridPreviewFakeWallObject.Add(wall);

                }
            }

        }

        public Quaternion GetRotationWall(bool isXsupZ)
        {

        if (isXsupZ)
        {
            return Quaternion.Euler(0, 90, 0);
        }
        else
        {
            return Quaternion.identity;
        }
    }
        public Vector3 GetPosWall(Vector3 startVector/*, Vector3 endVector, PositionCreationLine refCreationLine*/, Quaternion refQuater)
        {

        var endVector = new Vector3();
        if (refQuater.eulerAngles.y > 0f)
        {
            //endVector = new Vector3();
            endVector.x = startVector.x + diffbloc;
            endVector.z = startVector.z;
            //endVector.y = 0 ou 1st floor
            return endVector;
        }
        else
        {
            //endVector = new Vector3();
            endVector.x = startVector.x;
            endVector.z = startVector.z + diffbloc;
            //endVector.y = 0 ou 1st floor
            return endVector;
        }

    }

    public void PreviewFloorMaterial()
    {
        if(ActualOverGO != ActualChangeMatFloor)
        {
            
            if(ActualChangeMatFloor != null)
            {
                foreach (var item in ListFloorChangeMat)
                {
                    item.GetComponent<FloorControler>().ExitFloor();
                }
                ListFloorChangeMat.Clear();
            }
            
            ActualOverGO.GetComponent<FloorControler>().OverFloor(ActualFloorMaterial);

            ListFloorChangeMat.Add(ActualOverGO);
            ActualChangeMatFloor = ActualOverGO;
    

            
        }
    }

    public void ApplyModifTextureFloor()
    {
        if(UIController._instance.CanChangeMatFloor && Input.GetKey(KeyCode.Mouse0) && ActualOverGO.tag=="Floor")
        {
            if(ListFloorChangeMat.Count > 0)
            {
                foreach (var item in ListFloorChangeMat)
                {
                    item.GetComponent<FloorControler>().changeNormalMat(ActualFloorMaterial);
                }

                ListFloorChangeMat.Clear();
                refSM.PeindreUnSol();
            }
        }
    }

    public void PreviewWallMaterial(GameObject colliderGO)
    {
        if (colliderGO != ActualChangeMatWall)
        {
            var colliderName = colliderGO.name;

            if (ListWallChangeMat != null)
            {
                foreach (var item in ListWallChangeMat)
                {
                    item.GetComponent<WallMaterial>().ExitWall();
                }
                //ActualChangeMatWall.
            }
            ListWallChangeMat.Clear();
            

           
            colliderGO.GetComponent<WallMaterial>().OverWall(ActualWallMaterial);

            ListWallChangeMat.Add(colliderGO);
            ActualChangeMatWall = colliderGO;



        }
    }

    public void ApplyModifTextureWall()
    {
        if (UIController._instance.CanChangeMatWall && Input.GetKey(KeyCode.Mouse0) && ActualOverGO.tag =="Wall")
        {
            
            if (ListWallChangeMat.Count > 0)
            {
                foreach (var item in ListWallChangeMat)
                {
                    item.GetComponent<WallMaterial>().changeMat(ActualWallMaterial);
                }

                ListWallChangeMat.Clear();
                refSM.PeindreUnMur();
            }
        }
    }

    public void AssignedWall(GameObject Wall, GameObject PreviewWall)
    {
        this.Wall = Wall;
        this.FakeWall = PreviewWall;
    }

    public void AssignedMatFloor(Material matFloor)
    {
        this.ActualFloorMaterial = matFloor;
    }
    public void AssignedMatWall(Material matWall)
    {
        this.ActualWallMaterial = matWall;
    }



        private void CreateFloor(int[,] floorGrid)
        {

            for (int i = 0; i < floorGrid.GetLength(0); i++)
            {
                for (int j = 0; j < floorGrid.GetLength(1); j++)
                {
                    var posFloor = new Vector3(i, 0, j);
                    var newFloor = Instantiate(Floor, posFloor, Quaternion.identity, ParentFloor.transform);

                UIController._instance.allFloor.Add(newFloor);
                }
            }
        }

    //public void RegenerePathFinding()
    //{
    //    aStar.Scan();
    //}

    private void CreateWallBorder()
    {
        for (int i = 0; i < GridWidth+1; i++)
        {
            for (int j = 0; j < GridHeight+1; j++)
            {
                if((i == 0 || i == GridWidth ) && j != GridHeight)
                {
                   var newWall= Instantiate(Wall, new Vector3(i, 0, j+0.5f), Quaternion.identity);
                    newWall.GetComponent<WallScript>().IsBorder=true;
                    newWall.GetComponent<WallScript>().orientation = WallScript.OrientationWall.Vertical;
                    newWall.GetComponent<WallScript>().PosInArray = new Vector3(i, 0, j);
                    FirstFloorVerticalWall[i, j] = newWall;
                }

                if((j==0 || j == GridHeight) && i != GridWidth)
                {
                    var newWall = Instantiate(Wall, new Vector3(i+0.5f, 0, j), Quaternion.Euler(0,90,0));
                    newWall.GetComponent<WallScript>().IsBorder = true;
                    newWall.GetComponent<WallScript>().orientation = WallScript.OrientationWall.Horizontal;
                    newWall.GetComponent<WallScript>().PosInArray = new Vector3(i, 0, j);
                    FirstFloorHorizontalWall[i, j] = newWall;
                }
            }
        }
        //if (aStar != null)
        //{
        //    RegenerePathFinding();
           

        //}

    }

    public void AddTableauOnListPourCoef(Proto_Tableau_Scriptable tabScript)
    {
        switch (tabScript.courantArtTableau)
        {

            case GameLibrary.CourantArtistique.Manierisme:

                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][0]++;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Impressionnisme:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][1]++;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Cubisme:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][2]++;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Surrealisme:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][3]++;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Artnouveau:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][4]++;
                //print("test");
                break;
        }

        switch (tabScript.periode)
        {
            case GameLibrary.periode.XVI:
                SalleV2Ref.nbTableauParSalleParPeriode[tabScript.idSalle][0]++;
                break;
            case GameLibrary.periode.XIX:
                SalleV2Ref.nbTableauParSalleParPeriode[tabScript.idSalle][1]++;
                break;
            case GameLibrary.periode.XX:
                SalleV2Ref.nbTableauParSalleParPeriode[tabScript.idSalle][2]++;
                break;
        }

        switch (tabScript.style)
        {
            case GameLibrary.style.Portrait:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][0]++;
                break;
            case GameLibrary.style.Paysage:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][1]++;
                break;
            case GameLibrary.style.NatureMorte:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][2]++;
                break;
            case GameLibrary.style.Nu:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][3]++;
                break;
            case GameLibrary.style.Religion:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][4]++;
                break;
            case GameLibrary.style.Histoire:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][5]++;
                break;
            case GameLibrary.style.Mythologie:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][6]++;
                break;
            case GameLibrary.style.Abstrait:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][7]++;
                break;
        }

        switch (tabScript.elements)
        {
            case GameLibrary.Elements.Aliments:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][0]++;
                break;
            case GameLibrary.Elements.Vegetation:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][1]++;
                break;
            case GameLibrary.Elements.GroupeFemmes:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][2]++;
                break;
            case GameLibrary.Elements.GroupePersonnes:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][3]++;
                break;
            case GameLibrary.Elements.Animaux:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][4]++;
                break;
            case GameLibrary.Elements.Paysage:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][5]++;
                break;
            case GameLibrary.Elements.Eau:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][6]++;
                break;
            case GameLibrary.Elements.Champs:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][7]++;
                break;
            case GameLibrary.Elements.Village:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][8]++;
                break;
            case GameLibrary.Elements.Ville:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][9]++;
                break;
            case GameLibrary.Elements.Fleurs:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][10]++;
                break;
            case GameLibrary.Elements.InstrumentMusique:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][11]++;
                break;
            case GameLibrary.Elements.Bateau:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][12]++;
                break;
            case GameLibrary.Elements.Ciel:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][13]++;
                break;
            case GameLibrary.Elements.Texte:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][14]++;
                break;
            case GameLibrary.Elements.Mort:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][15]++;
                break;
            case GameLibrary.Elements.Mythologie:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][16]++;
                break;
            case GameLibrary.Elements.PortraitFemme:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][17]++;
                break;
            case GameLibrary.Elements.PortraitHomme:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][18]++;
                break;
            case GameLibrary.Elements.FigureReligieuse:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][19]++;
                break;
            case GameLibrary.Elements.Amour:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][20]++;
                break;
            case GameLibrary.Elements.Erotique:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][21]++;
                break;
            case GameLibrary.Elements.Objets:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][22]++;
                break;
        }
    }

    public void RemoveTableauOnListPourCoef(Proto_Tableau_Scriptable tabScript)
    {
        switch (tabScript.courantArtTableau)
        {

            case GameLibrary.CourantArtistique.Manierisme:

                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][0]--;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Impressionnisme:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][1]--;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Cubisme:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][2]--;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Surrealisme:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][3]--;
                //print("test");
                break;
            case GameLibrary.CourantArtistique.Artnouveau:
                SalleV2Ref.nbTableauParSalleParMouvement[tabScript.idSalle][4]--;
                //print("test");
                break;
        }

        switch (tabScript.periode)
        {
            case GameLibrary.periode.XVI:
                SalleV2Ref.nbTableauParSalleParPeriode[tabScript.idSalle][0]--;
                break;
            case GameLibrary.periode.XIX:
                SalleV2Ref.nbTableauParSalleParPeriode[tabScript.idSalle][1]--;
                break;
            case GameLibrary.periode.XX:
                SalleV2Ref.nbTableauParSalleParPeriode[tabScript.idSalle][2]--;
                break;
        }

        switch (tabScript.style)
        {
            case GameLibrary.style.Portrait:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][0]--;
                break;
            case GameLibrary.style.Paysage:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][1]--;
                break;
            case GameLibrary.style.NatureMorte:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][2]--;
                break;
            case GameLibrary.style.Nu:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][3]--;
                break;
            case GameLibrary.style.Religion:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][4]--;
                break;
            case GameLibrary.style.Histoire:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][5]--;
                break;
            case GameLibrary.style.Mythologie:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][6]--;
                break;
            case GameLibrary.style.Abstrait:
                SalleV2Ref.nbTableauParSalleParStyle[tabScript.idSalle][7]--;
                break;
        }

        switch (tabScript.elements)
        {
            case GameLibrary.Elements.Aliments:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][0]--;
                break;
            case GameLibrary.Elements.Vegetation:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][1]--;
                break;
            case GameLibrary.Elements.GroupeFemmes:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][2]--;
                break;
            case GameLibrary.Elements.GroupePersonnes:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][3]--;
                break;
            case GameLibrary.Elements.Animaux:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][4]--;
                break;
            case GameLibrary.Elements.Paysage:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][5]--;
                break;
            case GameLibrary.Elements.Eau:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][6]--;
                break;
            case GameLibrary.Elements.Champs:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][7]--;
                break;
            case GameLibrary.Elements.Village:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][8]--;
                break;
            case GameLibrary.Elements.Ville:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][9]--;
                break;
            case GameLibrary.Elements.Fleurs:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][10]--;
                break;
            case GameLibrary.Elements.InstrumentMusique:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][11]--;
                break;
            case GameLibrary.Elements.Bateau:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][12]--;
                break;
            case GameLibrary.Elements.Ciel:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][13]--;
                break;
            case GameLibrary.Elements.Texte:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][14]--;
                break;
            case GameLibrary.Elements.Mort:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][15]--;
                break;
            case GameLibrary.Elements.Mythologie:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][16]--;
                break;
            case GameLibrary.Elements.PortraitFemme:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][17]--;
                break;
            case GameLibrary.Elements.PortraitHomme:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][18]--;
                break;
            case GameLibrary.Elements.FigureReligieuse:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][19]--;
                break;
            case GameLibrary.Elements.Amour:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][20]--;
                break;
            case GameLibrary.Elements.Erotique:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][21]--;
                break;
            case GameLibrary.Elements.Objets:
                SalleV2Ref.nbTableauParSalleParElements[tabScript.idSalle][22]--;
                break;
        }
    }

}

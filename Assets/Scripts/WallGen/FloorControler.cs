﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorControler : MonoBehaviour
{
    // Start is called before the first frame update
    public Material NormalMat;
    public Material GridMat;


    private Renderer MaterialSlot;

    public int IdSalle;

    // ArrayMat[0] = TextureSol & ArrayMat[1] = grille
    public Material[] ArrayMat;

    //public bool HaveObject = false;

    //public GameObject WallRef;



    private void Awake()
    {
        MaterialSlot = GetComponent<MeshRenderer>();
        //Plane = gameObject.transform.GetChild(0).gameObject;
        //PlaneMaterialSlot = Plane.GetComponent<Renderer>();
        ArrayMat = MaterialSlot.materials;

    }

    void Start()
    {
        //MaterialSlot.material = NormalMat;
        ArrayMat[0] = NormalMat;
        ArrayMat[1] = null;
        MaterialSlot.materials = ArrayMat;
        //MaterialSlot.material = NormalPlaneMat;
    }


    void Update()
    {
        //checkCanConstruct();
        
        //if (MaterialSlot.materials != ArrayMat) MaterialSlot.materials = ArrayMat;
    }

    public void checkCanConstruct(bool terrainModifiate)
    {
        ArrayMat[1] = terrainModifiate ? GridMat : null;
        ApplyMaterials();
    }

    public void changeNormalMat(Material mat)
    {
        NormalMat = mat;
        ArrayMat[0] = NormalMat;

        ApplyMaterials();
        //MaterialSlot.material = NormalMat;
    }

    public void OverFloor(Material previewMat)
    {

        ArrayMat[0] = previewMat;
        ApplyMaterials();

    }


    public void ExitFloor()
    {
        ArrayMat[0] = NormalMat;
        ApplyMaterials();
    }


    private void ApplyMaterials()
    {
        MaterialSlot.materials = ArrayMat;
    }
}

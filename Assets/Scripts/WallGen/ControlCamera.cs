﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamera : MonoBehaviour
{
    
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            this.transform.Translate(new Vector3(0, 0, 1));
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.transform.Translate(new Vector3(0, 0, -1));
        }
        if (Input.GetKey(KeyCode.Q))
        {
            this.transform.Translate(new Vector3(-1, 0, 0));
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.Translate(new Vector3(1, 0, 0));
        }

        if (Input.GetKey(KeyCode.A))
        {
            this.transform.Rotate(new Vector3(0,-1,0));
        }
        if (Input.GetKey(KeyCode.E))
        {
            this.transform.Rotate(new Vector3(0, 1, 0));
        }

        if (Input.GetKey(KeyCode.R))
        {
            this.transform.Rotate(new Vector3(-1, 0, 0));
        }
        if (Input.GetKey(KeyCode.F))
        {
            this.transform.Rotate(new Vector3(1, 0, 0));
        }
        
        

        

    }

    

    
}

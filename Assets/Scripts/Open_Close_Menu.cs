﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Open_Close_Menu : MonoBehaviour
{

    public GameObject Panel;
    

    public void Open_Close()
    {
        if (!Panel.activeSelf)
        {
            Panel.SetActive(true);
        }
        else
        {
            Panel.SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Peons", order = 1)]
public class Peon : ScriptableObject
{
    public ClassPeon[] peons;
}

[System.Serializable]
public class ClassPeon
{
    public string LastName;
    public string FirstName;
    public GameManager.EnumGender Gender;
    public GameManager.EnumArtisticMovement FavArtMovement;
    public Mesh Body;
    public int Age;
    public string Job;

    ClassPeon(string _LastName)
    {
        LastName = _LastName;
    }
}
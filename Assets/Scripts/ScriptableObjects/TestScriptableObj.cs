﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[CreateAssetMenu(fileName = "Data", menuName = "GameManager/Library", order = 1)]
public class TestScriptableObj : MonoBehaviour
{
    public static TestScriptableObj instance = null;
    public enum testEnum
    {
        rien = 0,
        un = 1,
        deux = 2 
    }
    public int testInt;
    public bool testBool;
    public List<int> testList;
    public testEnum testenum;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        testList = new List<int>() { 1, 4, 8, 15 };
        testInt = 15;
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public (List<int> listint, bool booltest, int testint, testEnum testenum) GetListInt()
    {
        return (testList, testBool, testInt, testenum);
    }

    public int GetTheRandomElemInList(int randomElem)
    {
        if(testList != null && testList.Count > randomElem)
        {
            return testList[randomElem];
        }
        else
        {
            return -1;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class NarateurManager : MonoBehaviour
{
    public static NarateurManager _instance;

    [Header("Référence d'objet")]
    public GameObject refButtonSeeAgain;
    public GameObject Narrateur;
    public GameObject ZoneTexte;
    public Text TexteRef;
    public Image NarrateurFaciece;
    public GameObject refButTextBackward;
    public GameObject refButTextForward;

    //Ref des différents bouttons
    public GameObject refButConstruction;
    public GameObject refButStats;
    public GameObject refButCollection;
    public GameObject refButMarcher;
    public GameObject refButTime;
    public GameObject refButQuete;
    public GameObject refButPainter;

    public int IndexNarrateurQuest;
    public int IndexNarrateurEvent;
    public int IndexTextNarrateur;

    public string[][] TexteNarrateurQuest;
    public string[][] TexteNarrateurEvent;
    public bool[] isEventGone; /*{ get; set; }*/

    public bool isNarrateurEvent;
    public bool isNarrateurQuest;
    public bool NarrateurEnabled;
    public bool isQuestOver;

    QuestManager refQuestManager;
    UIController refUIControl;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        InitializeTexteNarrateurQuest();
        InitializeTexteNarrateurEvent();
        InitializeBoolEvent();
    }

    void Start()
    {
        refQuestManager = QuestManager._instance;
        refUIControl = UIController._instance;

        IndexNarrateurQuest = refQuestManager.activeQuest;


        if (!NarrateurEnabled) Narrateur.SetActive(false);
        else 
        { 
            refUIControl.PauseGame();
            SetButtonsInvisible();

        }

        refButtonSeeAgain.SetActive(false);

    }


    void Update()
    {
        if (NarrateurEnabled)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                //if (isNarrateurQuest)
                //{
                //    ContinueNarrateurQuest();
                //}
                //else if (isNarrateurEvent)
                //{
                //    ContinueNarrateurEvent();
                //}
            }
        }
            
    }

   

    public void AppelNarrateurQuest()
    {

        Narrateur.SetActive(true);
        isNarrateurQuest = true;
        IndexTextNarrateur = 0;
        TexteRef.text = TexteNarrateurQuest[IndexNarrateurQuest][IndexTextNarrateur];

        EventOnQuest(IndexNarrateurQuest, IndexTextNarrateur, false);

        refUIControl.PauseGame();
        CheckNarrateurTexte();

        refButtonSeeAgain.SetActive(false);
    }

    public void AppelNarrateurEvent(int indexNarrateur)
    {
        IndexNarrateurEvent = indexNarrateur;
        //print($"id Narrateur Event : {indexNarrateur}");
        Narrateur.SetActive(true);
        isNarrateurEvent = true;
        IndexTextNarrateur = 0;
        
        TexteRef.text = TexteNarrateurEvent[IndexNarrateurEvent][IndexTextNarrateur];
        EventOnNarrateurEvent(IndexNarrateurEvent, IndexTextNarrateur, false);

        if (IndexNarrateurEvent != 0) refUIControl.PauseGame();

        CheckNarrateurTexte();
        refButtonSeeAgain.SetActive(false);

    }

    public void DisparitionNarrateur(string modeNarrateur)
    {
        Narrateur.SetActive(false);
        refButtonSeeAgain.SetActive(true);

        if (modeNarrateur == "Quest")
        {
            isNarrateurQuest = false;
            EventOnQuest(IndexNarrateurQuest, IndexTextNarrateur, true);

        }
        else
        {
            isNarrateurEvent = false;
            EventOnNarrateurEvent(IndexNarrateurEvent, IndexTextNarrateur,true);
        }

        refUIControl.PlayGame();
        

    }

    public void ContinueNarrateurManager()
    {
        if (isNarrateurQuest)
        {
            ContinueNarrateurQuest();
        }
        else if (isNarrateurEvent)
        {
            ContinueNarrateurEvent();
        }
    }

    public void ContinueNarrateurQuest()
    {
        IndexTextNarrateur++;

        EventOnQuest(IndexNarrateurQuest, IndexTextNarrateur, false);

        if (IndexTextNarrateur < TexteNarrateurQuest[IndexNarrateurQuest].Length)
        {
            TexteRef.text = TexteNarrateurQuest[IndexNarrateurQuest][IndexTextNarrateur];
        }
        else DisparitionNarrateur("Quest");

        CheckNarrateurTexte();
    }

    public void ContinueNarrateurEvent()
    {
        IndexTextNarrateur++;

        EventOnNarrateurEvent(IndexNarrateurEvent, IndexTextNarrateur, false);

        if (IndexTextNarrateur < TexteNarrateurEvent[IndexNarrateurEvent].Length)
        {
            TexteRef.text = TexteNarrateurEvent[IndexNarrateurEvent][IndexTextNarrateur];
        }
        else 
        {
            DisparitionNarrateur("Event"); 
        }

        CheckNarrateurTexte();
    }

    public void RetourNarrateurManager()
    {
        if (isNarrateurQuest)
        {
            RetourNarrateurQuest();
        }
        else if (isNarrateurEvent)
        {
            RetourNarrateurEvent();
        }
    }

    public void RetourNarrateurQuest()
    {
        IndexTextNarrateur--;

        //EventOnQuest(IndexNarrateurQuest, IndexTextNarrateur, false);

        //if (IndexTextNarrateur < TexteNarrateurQuest[IndexNarrateurQuest].Length)
        //{
            TexteRef.text = TexteNarrateurQuest[IndexNarrateurQuest][IndexTextNarrateur];
        //}
        //else DisparitionNarrateur("Quest");
        CheckNarrateurTexte();
    }

    public void RetourNarrateurEvent()
    {
        IndexTextNarrateur--;

        //EventOnNarrateurEvent(IndexNarrateurEvent, IndexTextNarrateur, false);

        //if (IndexTextNarrateur < TexteNarrateurEvent[IndexNarrateurEvent].Length)
        //{
            TexteRef.text = TexteNarrateurEvent[IndexNarrateurEvent][IndexTextNarrateur];
        //}
        //else
        //{
        //    DisparitionNarrateur("Event");
        //}
        CheckNarrateurTexte();
    }

    public void CheckNarrateurTexte()
    {
        if(IndexTextNarrateur == 0)
        {
            refButTextBackward.SetActive(false);
        }
        else
        {
            refButTextBackward.SetActive(true);
        }

        if(IndexTextNarrateur == TexteNarrateurEvent[IndexNarrateurEvent].Length - 1)
        {
            //Changer le bouton forward pour indiquer que c'est le dernier text narrateur?
        }
    }



    public void InitializeTexteNarrateurQuest()
    {
        
        
        string path = Path.Combine(Application.streamingAssetsPath, "Text_Narrateur_Quete.csv");
        //var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();
        var csvTxt = File.ReadAllLines(path).Select(l => l.Split('\\').ToArray()).ToArray();

        //var nbrQuests = refQuestManager.quests.Length;
        TexteNarrateurQuest = new string[csvTxt.Length][];

        for (int i = 0; i < csvTxt.Length; i++)
        {

                TexteNarrateurQuest[i] = csvTxt[i];
        
        }

    }

    public void InitializeTexteNarrateurEvent()
    {
        
        string path = Path.Combine(Application.streamingAssetsPath, "Text_Narrateur_Event.csv");
        //var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();
        var csvTxt = File.ReadAllLines(path).Select(l => l.Split('\\').ToArray()).ToArray();

        //var nbrQuests = refQuestManager.quests.Length;
        TexteNarrateurEvent = new string[csvTxt.Length][];

        for (int i = 0; i < csvTxt.Length; i++)
        {

            TexteNarrateurEvent[i] = csvTxt[i];
            
        }

        //print(TexteNarrateurEvent.Length);
        //print(TexteNarrateurEvent[7].Length);
    }

    public void InitializeBoolEvent()
    {
        isEventGone = new bool[TexteNarrateurEvent.Length];
    }

    public void EventOnQuest(int indexNarrateurQuest, int indexText,bool OnEnd)
    {
        //Ajouter le Button Quete
        if (indexNarrateurQuest == 0 && indexText == 1)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButQuete.SetActive(false);
            
            // Mettre l'effet ici

            refButQuete.SetActive(true);

        }
        //Ajouter le Button Collection
        if (indexNarrateurQuest == 1 && indexText == 0)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButCollection.SetActive(false);

            // Mettre l'effet ici

            refButCollection.SetActive(true);

        }
        //Ajouter le Button Construction
        if (indexNarrateurQuest == 1 && indexText == 0)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButConstruction.SetActive(false);

            // Mettre l'effet ici

            refButConstruction.SetActive(true);

        }
        //Ajouter le Button Painter
        if (indexNarrateurQuest == 1 && indexText == 0)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButPainter.SetActive(false);

            // Mettre l'effet ici

            refButPainter.SetActive(true);

        }
        //Ajouter le Button Time
        if (indexNarrateurQuest == 1 && indexText == 0)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButTime.SetActive(false);

            // Mettre l'effet ici

            refButTime.SetActive(true);

        }
        //Ajouter le Button Marcher
        if (indexNarrateurQuest == 1 && indexText == 0)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButMarcher.SetActive(false);

            // Mettre l'effet ici

            refButMarcher.SetActive(true);

        }
        //Ajouter le Button Stats
        if (indexNarrateurQuest == 1 && indexText == 0)
        {
            //A la quete 0 au texte 2 Afficher le boutton quete
            refButStats.SetActive(false);

            // Mettre l'effet ici

            refButStats.SetActive(true);

        }

        //Fin du narrateur de quete
        if (indexNarrateurQuest == TexteNarrateurQuest.Length - 1 && indexText  == TexteNarrateurQuest[TexteNarrateurQuest.Length - 1].Length && OnEnd)
        {
            //print("DisableNarrateurQuest");
            if (!isQuestOver)
            {
                //print("oneTime");
                isQuestOver = true;
                refButtonSeeAgain.SetActive(false);
            }
            
        }
    }

    public void EventOnNarrateurEvent(int indexNarrateurSupp, int indexText, bool OnEnd)
    {
        if (indexNarrateurSupp == 0 && indexText  == TexteNarrateurEvent[0].Length && OnEnd)
        {
            AppelNarrateurQuest();
        }
    }

    public void SetButtonsInvisible()
    {
        refButConstruction.SetActive(false);
        refButStats.SetActive(false);
        refButCollection.SetActive(false);
        refButMarcher.SetActive(false);
        refButTime.SetActive(false);
        refButQuete.SetActive(false);
        refButPainter.SetActive(false);
}
}

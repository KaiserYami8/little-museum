﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class ADDLIST : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string inputsound;

    GameLibrary refGameLibrary;

    public GameObject InventaireObject;
    public Proto_Tableau_Scriptable Tableau;


    void Start()
    {
        refGameLibrary = GameLibrary._instance;
    }

    public void ADDDOBJECT()
    {
        if(refGameLibrary.tableau_never_use.Count > 0)
        {
            Tableau = refGameLibrary.tableau_never_use[Random.Range(0, refGameLibrary.tableau_never_use.Count)];
            refGameLibrary.tableau_never_use.Remove(Tableau);
            InventaireObject.GetComponent<Proto_Inventory>().Inventaire.Add(Tableau);
        }      
    }
}

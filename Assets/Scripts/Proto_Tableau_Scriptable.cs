﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Tableau", menuName = "Tableau")]
public class Proto_Tableau_Scriptable : ScriptableObject
{
    [Header("Informations Tableau")]
    public string titre;
    public string auteur;
    public string Annee;
    [TextArea(15, 20)]
    public string description;
    public string technique;
    public string lieuConservation;
    public int idSalle;
    public GameLibrary.CourantArtistique courantArtTableau;
    public GameLibrary.periode periode;
    public GameLibrary.Ambiance ambiance;
    public GameLibrary.Elements elements;
    public GameLibrary.style style;

    [Header("Aspect")]
    public GameObject Cadre;
    public Sprite imageOeuvre;
    public Material MatTableau;
    public Material MatCadre;


    [Header("Taille Tableau")]
    public float LargeurTableau;
    public float HauteurTableau;
    public float ProfondeurTableau;


    [Header("Value Reputation")]
    public float OriginalValue;
    public float TransformedValue;

    [Header("Coef")]
    public float MouvementCoef = 1;
    public float PeriodCoef = 1;
    public float StyleCoef = 1;
    public float AmbianceCoef = 1;
    public float ElementsCoef = 1;
    public float CoefGlobal = 1;

    // Start is called before the first frame update
    void Start()
    {

    }


    public void CalculateReputationUpdate()
    {
        CoefGlobal = (MouvementCoef * 5 + PeriodCoef * 2 + StyleCoef * 2 + ElementsCoef * 1) / 10;
        TransformedValue = (OriginalValue * CoefGlobal);
    }
}

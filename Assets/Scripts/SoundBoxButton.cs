﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundBoxButton : MonoBehaviour
{
    public bool empty;

    [FMODUnity.EventRef]
    public string soundEffect;

    // Start is called before the first frame update
    void Start()
    {
        if (empty == true)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(soundEffect);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DisplayTableau : MonoBehaviour
{
    public Proto_Tableau_Scriptable Tableau;
    public GameObject RefMenuCompare;
    UI_Tableau refUITableau;
    UIController refUIControl;
    QuestManager refQuestManager;
    NarateurManager refNarrateur;
    //UIDebug refDebug;
    public GameObject toile;
    Camera cam;

    float sizeZCollider;
    float sizeYCollider;

    //PanelInfo
    public  GameObject panel;
    GameObject currPanel;
    bool panelIsActive;
    Vector3 target;
    GameObject canvas;

    [Header("Valeur des marges des tableaux")]
    public float LargeurMarge;
    public float HauteurMarge;

    private void Start()
    {
        refUIControl = UIController._instance;
        refQuestManager = QuestManager._instance;
        refNarrateur = NarateurManager._instance;
        //print(Tableau.MatTableau);
        RefMenuCompare = refUIControl.refMenuCompareTableau;
        var mats = toile.GetComponent<Renderer>().materials;
        //Here, you have to assign with the exact order!
        mats[0] = Tableau.MatCadre;
        mats[1] = Tableau.MatTableau;
        toile.GetComponent<Renderer>().materials = mats;

        var hauteur = Tableau.HauteurTableau;
        var largeur = Tableau.LargeurTableau;
        var ratio = hauteur / largeur;

        var tabLarg = ScaleLargeur(largeur);

        var tabHaut = ScaleHauteur(tabLarg, ratio);
        //var tabLarg = Mathf.FloorToInt(Tableau.LargeurTableau) + 1;
        sizeZCollider = getZScale(tabLarg, LargeurMarge);
        sizeYCollider = getZScale(tabHaut, HauteurMarge);


     

        //GetComponent<Transform>().localScale = new Vector3(/*Tableau.ProfondeurTableau*/1, Tableau.HauteurTableau * 0.5f, Tableau.LargeurTableau * 0.5f);
        GetComponent<Transform>().localScale = new Vector3(/*Tableau.ProfondeurTableau*/1, tabHaut/**0.5f*/, tabLarg/**0.5f*/);
        var sizecol = GetComponent<BoxCollider>().size;
        //sizecol = new Vector3(sizecol.x * 1, sizecol.y * Tableau.HauteurTableau * 0.5f, sizecol.z * (Tableau.LargeurTableau * 0.5f));

        sizecol = new Vector3(0.25f, /*sizecol.y*/  sizeYCollider, sizeZCollider);

        GetComponent<BoxCollider>().size = sizecol;
        var centercol = GetComponent<BoxCollider>().center;
        centercol = new Vector3(0.05f, centercol.y, centercol.z);
        GetComponent<BoxCollider>().center = centercol;

        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        canvas = GameObject.FindGameObjectWithTag("canvas");
        refUITableau = UI_Tableau._instance;
        //refDebug = UIDebug._instance;
    }

    public void GetMaterialsTableau()
    {
        //print(Tableau.MatTableau);
        var mats = toile.GetComponent<Renderer>().materials;
        //Here, you have to assign with the exact order!
        mats[0] = Tableau.MatCadre;
        mats[1] = Tableau.MatTableau;
        toile.GetComponent<Renderer>().materials = mats;


        var hauteur = Tableau.HauteurTableau;
        var largeur = Tableau.LargeurTableau;
        var ratio = hauteur / largeur;

        var tabLarg = ScaleLargeur(largeur);

        var tabHaut = ScaleHauteur(tabLarg, ratio);

        //var tabLarg = Mathf.FloorToInt(Tableau.LargeurTableau) + 1;
        sizeZCollider = getZScale(tabLarg, LargeurMarge);
        sizeYCollider = getZScale(tabHaut, HauteurMarge);


        // GetComponent<Transform>().localScale = new Vector3(/*Tableau.ProfondeurTableau*/1, Tableau.HauteurTableau * 0.5f, Tableau.LargeurTableau * 0.5f);
        GetComponent<Transform>().localScale = new Vector3(/*Tableau.ProfondeurTableau*/1, tabHaut/**0.5f*/, tabLarg/**0.5f*/);

        

        var sizecol = GetComponent<BoxCollider>().size;
         sizecol = new Vector3(0.25f, /*sizecol.y*/  sizeYCollider, sizeZCollider);
        GetComponent<BoxCollider>().size = sizecol;

        var centercol = GetComponent<BoxCollider>().center;
        centercol = new Vector3(0.05f, centercol.y, centercol.z);
        GetComponent<BoxCollider>().center = centercol;
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (refUIControl.CanCompareTab)
                {
                    if(RefMenuCompare.GetComponent<CompareTabScript>().FirstTab != Tableau)
                    {
                        RefMenuCompare.GetComponent<CompareTabScript>().SecondTab = Tableau;
                        RefMenuCompare.SetActive(true);

                        if (refQuestManager.quests[refQuestManager.activeQuest].compareDeuxTabBool)
                        {
                            if (!refQuestManager.quests[refQuestManager.activeQuest].compareDeuxTab) refQuestManager.CheckCompareTab();
                        }
                    }
                    
                }
                else
                {
                    if (refUIControl.refMenuTableau == null)
                    {
                        currPanel = Instantiate(panel, canvas.transform);
                        currPanel.transform.position = this.transform.GetChild(4).transform.position;
                        //target = currPanel.transform.position;


                        var menuTabScript = currPanel.GetComponent<MenuTableaux>();
                        menuTabScript.refTableau = Tableau;
                        menuTabScript.refTableauGO = gameObject;
                    }
                    else if (refUIControl.refMenuTableau != null && refUIControl.refMenuTableau.GetComponent<MenuTableaux>().refTableau != Tableau)
                    {
                        currPanel = Instantiate(panel, canvas.transform);
                        currPanel.transform.position = this.transform.GetChild(4).transform.position;
                        //target = currPanel.transform.position;


                        var menuTabScript = currPanel.GetComponent<MenuTableaux>();
                        menuTabScript.refTableau = Tableau;
                        menuTabScript.refTableauGO = gameObject;
                    }

                    if (refQuestManager.quests[refQuestManager.activeQuest].combinaisonBool)
                    {
                        if (!refQuestManager.quests[refQuestManager.activeQuest].clickTab) refQuestManager.CheckClickedTab();
                    }

                    Invoke("AppelNarra", 0.1f);
                }
                

                
            }
                
        }
    }

    private void AppelNarra()
    {
        if (!refNarrateur.isEventGone[4] && refNarrateur.NarrateurEnabled)
        {

            refNarrateur.AppelNarrateurEvent(4);
            refNarrateur.isEventGone[4] = true;
        }
    }

    public void reCreateMenu()
    {
        currPanel = Instantiate(panel, canvas.transform);
        currPanel.transform.position = this.transform.GetChild(4).transform.position;


        var menuTabScript = currPanel.GetComponent<MenuTableaux>();
        menuTabScript.refTableau = Tableau;
        menuTabScript.refTableauGO = gameObject;
    }

    public float ScaleLargeur(float Largeur)
    {
        if (Largeur < 1.25f) return 1;
        else if(Largeur < 1.75f) return 1.15f;
        else if(Largeur < 2.25f) return 1.3f;
        else if(Largeur < 2.75f) return 1.45f;
        else if(Largeur < 3.25f) return 1.6f;
        else if(Largeur < 3.75f) return 1.8f;
        else if(Largeur < 4.25f) return 2;

        return 0;
    }

    //Hauteur/larg

    public float ScaleHauteur(float largeur, float ratio)
    {
        return largeur * ratio;
    }

    public float getZScale(float tabLarg, float Scale)
    {
        return tabLarg + Scale;
    }


    public void ToogleLayer(bool normal)
    {
        if (normal)
        {
            //ignore raycast layer
            gameObject.layer = 2;
        }
        else
        {
            //default layer
            gameObject.layer = 0;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClassTableau : GameLibrary
{

    public CourantArtistique courantart;

    
    public ClassTableau()
    {
        this.courantart = GiveCourantArt();
    }
    private CourantArtistique GiveCourantArt()
    {
        courantart = (CourantArtistique)Random.Range(3, 4);
        return courantart;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;


//SINGLETON BIATCH
public class GameLibrary : MonoBehaviour
{

    public static GameLibrary _instance;

    public List<GameObject> tableau;

    [Header("Prototype")]
    public List<Proto_Tableau_Scriptable> tableau_never_use;
    public List<Proto_Tableau_Scriptable> ListEveryTableau;
    public float reputation;

    public List<GameObject> enter;
    public List<GameObject> exit;

    public List<string> FirstName_Men;
    public List<string> FirstName_Women;
    public List<string> FirstName_Neutral;
    public List<string> LastName;

    // déclaration des listes de parties du corps, mettre les gameobjects dedans via l'inspecteur

    // neutre (tout)
    [Header("Pantalon (neutre)")]
    public List<GameObject> L_legs;
    [Header("Torse (neutre)")]
    public List<GameObject> L_torso;
    [Header("Nez (neutre)")]
    public List<GameObject> L_nose;
    [Header("Chapeau (neutre)")]
    public List<GameObject> L_hat;
    [Header("Cheveux (neutre)")]
    public List<GameObject> L_hair;
    [Header("Oreille (neutre)")]
    public List<GameObject> L_ears;
    [Header("Chaussure (neutre)")]
    public List<GameObject> L_shoes;
    [Header("Accessoire (neutre)")]
    public List<GameObject> L_Accesoires;

    // femme
    [Header("Torse (Femme)")]
    public List<GameObject> L_Female_torso;
    [Header("Cheveux (Femme)")]
    public List<GameObject> L_Female_hair;
    [Header("Oreille (Femme)")]
    public List<GameObject> L_Female_ears;
    [Header("Chaussure (Femme)")]
    public List<GameObject> L_Female_shoes;

    // homme
    [Header("Torse (Homme)")]
    public List<GameObject> L_male_torso;
    [Header("Cheveux (Homme)")]
    public List<GameObject> L_male_hair;
    [Header("Oreille (Homme)")]
    public List<GameObject> L_male_ears;
    [Header("Chaussure (Homme)")]
    public List<GameObject> L_male_shoes;

    //Tete Queue de Cheval
    [Header("Tete Queue de Cheval")]
    public GameObject HatExeption;

    /// LIBRARIE TABLEAU
    public enum CourantArtistique
    {
        Manierisme = 0,
        Impressionnisme = 1,
        Cubisme = 2,
        Surrealisme = 3,
        Artnouveau = 4
    }

    public enum periode
    {
        XVI = 0,
        XIX = 1,
        XX = 2
    }

    //[Flags]
    public enum style
    {
        Portrait = 0,
        Paysage = 1,
        NatureMorte = 2,
        Nu = 3,
        Religion = 4,
        Histoire = 5,
        Mythologie = 6,
        Abstrait = 7,
    }

    //[Flags]
    public enum Ambiance
    {
        Romantisme = 0,
        Fantastique = 1,
        Bucolique = 2,
        Religieux = 3,
        Abstrait = 4,
        Réaliste = 5,
        Onirique = 6,
        Portrait = 7,
    }

    //[Flags]
    public enum Elements
    {
        Aliments = 0,
        Vegetation = 1,
        GroupeFemmes = 2,
        GroupePersonnes = 3,
        Animaux = 4,
        Paysage = 5,
        Eau = 6,
        Champs = 7,
        Village = 8,
        Ville = 9,
        Fleurs = 10,
        InstrumentMusique = 11,
        Bateau = 12,
        Ciel = 13,
        Texte = 14,
        Mort = 15,
        Mythologie = 16,
        PortraitFemme = 17,
        PortraitHomme = 18,
        FigureReligieuse = 19,
        Amour = 20,
        Erotique = 21,
        Objets = 22
    }
    /// </summary>


    public enum Sexe
    {
        homme = 0,
        femme = 1,
        neutre = 2
    }

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        var tab = GameObject.FindGameObjectsWithTag("tableau");
        foreach (var item in tab)
        {
            tableau.Add(item);
        }

        ReadJson();
    }

    void ReadJson()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "name_visiteurs.json");
        string json = File.ReadAllText(path);
        RootVisiteurName root = JsonUtility.FromJson<RootVisiteurName>(json);

        foreach (var item in root.firstname_men_List)
        {
            FirstName_Men.Add(item.firstnamemen);
        }
        foreach (var item in root.firstname_women_List)
        {
            FirstName_Women.Add(item.firstnamewomen);
        }
        foreach (var item in root.firstname_neutral_List)
        {
            FirstName_Neutral.Add(item.firstnameneutral);
        }
        foreach (var item in root.lastname_List)
        {
            LastName.Add(item.lastname);
        }
    }

    [System.Serializable]
    private class RootVisiteurName
    {
        public FirstnameMen[] firstname_men_List;
        public FirstnameWomen[] firstname_women_List;
        public FirstnameNeutral[] firstname_neutral_List;
        public Lastname[] lastname_List;
    }

    [System.Serializable]
    private class FirstnameMen
    {
        public string firstnamemen;
    }

    [System.Serializable]
    private class FirstnameWomen
    {
        public string firstnamewomen;
    }

    [System.Serializable]
    private class FirstnameNeutral
    {
        public string firstnameneutral;
    }

    [System.Serializable]
    private class Lastname
    {
        public string lastname;
    }




    public void PopTableau(GameObject item)
    {
        tableau.Add(item);
    }
}

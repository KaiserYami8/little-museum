﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Web;
using System.IO;

public class ClassVisiteur : GameLibrary
{
    string firstname;
    string lastname;

    Sexe sexe { get; set; }// 0 = homme 1 = femme 2= neutre
    CourantArtistique courantart { get; set; }


    List<GameObject> L_constructorSelection;

    

    public ClassVisiteur()
    {
        this.sexe = GiveSexe();
        this.courantart = GiveCourantArtistique();
        GenerateName();
        BodyConstructor();
    }


    private Sexe GiveSexe()
    {
        float rand = Random.value;
        if (rand <= 0.465f)
            return Sexe.homme;
        if (rand > .465f && rand <= 0.93f)
            return Sexe.femme;
        if (rand > 0.93f)
            return Sexe.neutre;

        return Sexe.neutre;
    }
    private CourantArtistique GiveCourantArtistique()
    {
        var cart = (CourantArtistique)Random.Range(3, 4);
        return cart;
    }
    private  void GenerateName()
    {

        switch (this.sexe)
        {
            case Sexe.homme:
                firstname = GameLibrary._instance.FirstName_Men[Random.Range(0, GameLibrary._instance.FirstName_Men.Count)];
                break;
            case Sexe.femme:
                firstname = GameLibrary._instance.FirstName_Women[Random.Range(0, GameLibrary._instance.FirstName_Women.Count)];
                break;
            case Sexe.neutre:
                firstname = GameLibrary._instance.FirstName_Neutral[Random.Range(0, GameLibrary._instance.FirstName_Neutral.Count)];
                break;
        }

        lastname = GameLibrary._instance.LastName[Random.Range(0, GameLibrary._instance.LastName.Count)];
    }
    public CourantArtistique GetCourantArtPref()
    {
        return courantart;
    }
    public Sexe GetSexe()
    {
        return sexe;
    }
    public (string Lastname, string Firstname) GetNames()
    {
        return (lastname, firstname);
    }
    public List<GameObject> GetSkin()
    {
        return L_constructorSelection;
    }

    public void BodyConstructor() // Construire un corp 
    {
        L_constructorSelection = new List<GameObject>();

        if (sexe == Sexe.neutre) // si le corps est de type neutre
        {
            // pick torso
            L_constructorSelection.Add(GameLibrary._instance.L_torso[Random.Range(0, GameLibrary._instance.L_torso.Count)]);
            // pick hair
            L_constructorSelection.Add(GameLibrary._instance.L_hair[Random.Range(0, GameLibrary._instance.L_hair.Count)]);
            // pick ears
            L_constructorSelection.Add(GameLibrary._instance.L_ears[Random.Range(0, GameLibrary._instance.L_ears.Count)]);
            // pick shoes
            L_constructorSelection.Add(GameLibrary._instance.L_shoes[Random.Range(0, GameLibrary._instance.L_shoes.Count)]);

        }


        if (sexe == Sexe.homme) // pick des élléments dans la liste des objets homme
        {
            // pick torso
            L_constructorSelection.Add(GameLibrary._instance.L_male_torso[Random.Range(0, GameLibrary._instance.L_male_torso.Count)]);
            // pick hair
            L_constructorSelection.Add(GameLibrary._instance.L_male_hair[Random.Range(0, GameLibrary._instance.L_male_hair.Count)]);
            // pick ears
            L_constructorSelection.Add(GameLibrary._instance.L_male_ears[Random.Range(0, GameLibrary._instance.L_male_ears.Count)]);
            // pick shoes
            L_constructorSelection.Add(GameLibrary._instance.L_male_shoes[Random.Range(0, GameLibrary._instance.L_male_shoes.Count)]);
        }

        if (sexe == Sexe.femme)
        {
            // pick torso
            L_constructorSelection.Add(GameLibrary._instance.L_Female_torso[Random.Range(0, GameLibrary._instance.L_Female_torso.Count)]);
            // pick hair
            L_constructorSelection.Add(GameLibrary._instance.L_Female_hair[Random.Range(0, GameLibrary._instance.L_Female_hair.Count)]);
            // pick ears
            L_constructorSelection.Add(GameLibrary._instance.L_Female_ears[Random.Range(0, GameLibrary._instance.L_Female_ears.Count)]);
            // pick shoes
            L_constructorSelection.Add(GameLibrary._instance.L_Female_shoes[Random.Range(0, GameLibrary._instance.L_Female_shoes.Count)]);
        }

        // PICK HAT
        if (Random.Range(0, 2) == 1 && L_constructorSelection.Contains(GameLibrary._instance.HatExeption) == false) // a-t-il un chapeau ? sa coupe est elle compatible ?
        {
            L_constructorSelection.Add(GameLibrary._instance.L_hat[Random.Range(0, GameLibrary._instance.L_hat.Count)]);
        }

        // pick legs
        L_constructorSelection.Add(GameLibrary._instance.L_legs[Random.Range(0, GameLibrary._instance.L_legs.Count)]);

        // pick nose
        L_constructorSelection.Add(GameLibrary._instance.L_nose[Random.Range(0, GameLibrary._instance.L_nose.Count)]);

        // pick accesoires
        if (Random.Range(0, 3) == 1) // a-t-il un acessoire ?
        {
            L_constructorSelection.Add(GameLibrary._instance.L_Accesoires[Random.Range(0, GameLibrary._instance.L_Accesoires.Count)]);
        }

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Proto_Inventory : MonoBehaviour
{
    //Singelton de l'UI
    public static Proto_Inventory _instance;

    [Header("INVENTAIRE")]
    public List<Proto_Tableau_Scriptable> Inventaire;
    public List<GameObject> buttonInventaire;
    public List<Proto_Tableau_Scriptable> TabAlreadySeen;

    [Header("Settings Inventaire")]
    public GameObject prefab_Boutton;
    public Transform parent;
    public int temp;

    UIController refUIControl;


    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        refUIControl = UIController._instance;

        foreach (var item in Inventaire)
        {
            GameObject button = Instantiate(prefab_Boutton, parent);
            button.name = item.name + "_Button";
            var protoTab = button.GetComponent<Proto_Tableau_Inventaire>();
            protoTab.refTableau = item;
            button.transform.GetChild(3).GetComponent<Text>().text = item.titre;
            button.transform.GetChild(2).GetComponent<Image>().sprite = item.imageOeuvre;
            //button.GetComponent<Button>().onClick.AddListener(UIController._instance.PosTableau);

            if (TabAlreadySeen.Contains(item))
            {
                protoTab.initializeTabBool(false);
            }
            else
            {
                protoTab.initializeTabBool(true);
                TabAlreadySeen.Add(item);
            }
            buttonInventaire.Add(button);
        }
        temp = Inventaire.Count;
    }

    void Update()
    {
        
        
        if (temp < Inventaire.Count)
        {
            
            InventaireChanged();
        }
        else
        {
            temp = Inventaire.Count;
        }


    }

    public void ResetButton()
    {
        for (int i = 0; i < buttonInventaire.Count; i++)
        {
            buttonInventaire[i].GetComponent<Proto_Tableau_Inventaire>().SetNormalStatuts();
        }
    }

    void InventaireChanged()
    {

        temp = Inventaire.Count;
        Proto_Tableau_Scriptable item = Inventaire[Inventaire.Count - 1];
        GameObject button = Instantiate(prefab_Boutton, parent);
        button.name = item.name + "_Button";
        var protoTab = button.GetComponent<Proto_Tableau_Inventaire>();
        protoTab.refTableau = item;
        button.transform.GetChild(3).GetComponent<Text>().text = item.titre;
        button.transform.GetChild(2).GetComponent<Image>().sprite = item.imageOeuvre;
        //button.GetComponent<Button>().onClick.AddListener(UIController._instance.PosTableau);


        if(TabAlreadySeen.Contains(item))
        {
            protoTab.initializeTabBool(false);
        }
        else
        {
            protoTab.initializeTabBool(true);
            TabAlreadySeen.Add(item);
        }

        
        buttonInventaire.Add(button);

        refUIControl.CheckTab();
    }

    public void ClearInventory(Proto_Tableau_Scriptable Tableau)
    {
        foreach (var item in buttonInventaire)
        {
            if(item.name == Tableau.name + "_Button")
            {
                buttonInventaire.Remove(item);
                Inventaire.Remove(Tableau);
                Destroy(GameObject.Find(Tableau.name + "_Button"));
                //GenerationMur._instance.Tableau = null;
                UIController._instance.PosTableau();
                break;
            }
        }
    }
}

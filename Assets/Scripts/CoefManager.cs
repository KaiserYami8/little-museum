﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class CoefManager : MonoBehaviour
{
    public static CoefManager _instance;

    public float[,] MouvementCoef;
    public float[,] AmbianceCoef;
    public float[,] ElementsCoef;
    public float[,] PeriodeCoef;
    public float[,] TailleCoef;
    public float[,] StyleCoef;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {

        ChangeMovementCoef();
        //ChangeAmbianceCoef();
        ChangeElementCoef();
        ChangePeriodeCoef();
        ChangeTailleCoef();
        ChangeStyleCoef();

    }

    //Move
    public void ChangeMovementCoef()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "CoefMouvement.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();
        //print($"ROWSL Move: {rows.Length}");
        MouvementCoef = new float[rows.Length, rows.Length];

        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows.Length; j++)
            {
                //print(rows[i][j]);
                MouvementCoef[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
                //print(MouvementCoef[i, j]);
            }
        }
    }

    //Ambi
    public void ChangeAmbianceCoef()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "CoefAmbiance.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();

        //print($"ROWSL Ambi: {rows.Length}");

        AmbianceCoef = new float[rows.Length, rows.Length];

        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows.Length; j++)
            {
                //print(rows[i][j]);
                AmbianceCoef[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
                //print(MouvementCoef[i, j]);
            }
        }
    }

    //Elem
    public void ChangeElementCoef()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "CoefElements.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();

        //print($"ROWSL Elem: {rows.Length}");

        ElementsCoef = new float[rows.Length, rows.Length];

        //print(rows[rows.Length-1][rows.Length-1]);

        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows.Length; j++)
            {
                ElementsCoef[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
            }
        }
    }

    //peri
    public void ChangePeriodeCoef()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "CoefPeriode.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();

        //print($"ROWSL Peri: {rows.Length}");

        PeriodeCoef = new float[rows.Length, rows.Length];

        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows.Length; j++)
            {
                //print(rows[i][j]);
                PeriodeCoef[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
                //print(MouvementCoef[i, j]);
            }
        }
    }

    //Taille
    public void ChangeTailleCoef()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "CoefTaille.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();

        //print($"ROWSL Tail: {rows.Length}");

        TailleCoef = new float[rows.Length, rows.Length];

        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows.Length; j++)
            {
                //print(rows[i][j]);
                TailleCoef[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
                //print(MouvementCoef[i, j]);
            }
        }
    }

    //style
    public void ChangeStyleCoef()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "CoefStyle.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();

        //print($"ROWSL Styl: {rows.Length}");

        StyleCoef = new float[rows.Length, rows.Length];

        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows.Length; j++)
            {
                //print(rows[i][j]);
                StyleCoef[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
                //print(MouvementCoef[i, j]);
            }
        }
    }

    void Update()
    {
        
    }
}

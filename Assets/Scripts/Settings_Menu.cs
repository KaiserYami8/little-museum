﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings_Menu : MonoBehaviour
{
    SoundManager refSM;
    SettingsManager refSettingsManager;
    Resolution[] resolutions;
    public Dropdown resolutionDropdown;

    //AUDIO SETTINGS
    FMOD.Studio.Bus Master;
    FMOD.Studio.Bus Music;
    FMOD.Studio.Bus SFX;

    float MusicVolume = 0.5f;
    float SFXVolume = 0.5f;
    float MasterVolume = 1f;


    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider sfxVolumeSlider;
    public Toggle fullScreenToggle;
    public Dropdown qualityDropdown;


    void Awake()
    {
        Master = FMODUnity.RuntimeManager.GetBus("bus:/master");
        Music = FMODUnity.RuntimeManager.GetBus("bus:/master/music");
        SFX = FMODUnity.RuntimeManager.GetBus("bus:/master/sfx");
        
    }

    void Start()
    {

        refSM = SoundManager._instance;
        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();
        refSettingsManager = SettingsManager._instance;

        List<string> options = new List<string>();
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = $"{resolutions[i].width} x {resolutions[i].height}";
            options.Add(option);
        }
        resolutionDropdown.AddOptions(options);
        if(refSettingsManager != null)
        {
            OnStartSettings();
        }
        resolutionDropdown.RefreshShownValue();
        qualityDropdown.RefreshShownValue();


        
    }

    void Update()
    {
        Master.setVolume(MasterVolume);
        Music.setVolume(MusicVolume);
        SFX.setVolume(SFXVolume);
    }

    public void OnStartSettings()
    {
        masterVolumeSlider.value = refSettingsManager.MasterVolume_SM;
        musicVolumeSlider.value = refSettingsManager.MusicVolume_SM;
        sfxVolumeSlider.value = refSettingsManager.SFXVolume_SM;
        fullScreenToggle.isOn = refSettingsManager.FullScreen_SM;
        qualityDropdown.value = refSettingsManager.qualityIndex_SM;
        resolutionDropdown.value = refSettingsManager.ResolutionIndex_SM;

        MasterVolumeLevel(refSettingsManager.MasterVolume_SM);
        MusicVolumeLevel(refSettingsManager.MusicVolume_SM);
        SFXVolumeLevel(refSettingsManager.SFXVolume_SM);
        SetQuality(refSettingsManager.qualityIndex_SM);
        SetFullScreen(refSettingsManager.FullScreen_SM);
        SetScreenSize(refSettingsManager.ResolutionIndex_SM);
    }
    public void MasterVolumeLevel(float newMasterVolume)
    {
        MasterVolume = newMasterVolume;
        refSettingsManager.MasterVolume_SM = newMasterVolume;
    }
    public void MusicVolumeLevel(float newMusicLevel)
    {
        MusicVolume = newMusicLevel;
        refSettingsManager.MusicVolume_SM = newMusicLevel;
    }
    public void SFXVolumeLevel(float newSFXVolume)
    {
        SFXVolume = newSFXVolume;
        refSettingsManager.SFXVolume_SM = newSFXVolume;
    }

    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
        refSettingsManager.qualityIndex_SM = qualityIndex;
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
        refSettingsManager.FullScreen_SM = isFullScreen;

        if (isFullScreen)
        {
            refSM.ClicATHFonctionOn();
        }
        else
        {
            refSM.ClicATHFonctionOff();

        }
    }

    public void SetScreenSize(int ResolutionIndex)
    {
        Resolution resolution = resolutions[ResolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        refSettingsManager.ResolutionIndex_SM = ResolutionIndex;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Proto_Temporalite : MonoBehaviour
{
    public static Proto_Temporalite _instance;

    public float nextActionTime; // valeur qui définis la longueur d'une journée en secondes

    public float timer; // valeur du temps passé actuelle

    public int TimerStateRef; // valeur du multiplicateur de temps (play, pose, x2, x3)

    public int actualDay = 1; // jour actuel dans le mois

    public int TotalPassedDays;

    bool isNight = false;

    //public float period = 0.1f;

    //public float periodRef = 0.1f;

    int i;

    Salle salleRef;
    SalleV2 salleV2Ref;
    PainterManager refPainter;

    //UIDebug refUIDeubg;

    GameLibrary refGL;
    SoundManager refSM;
    HistoryReput refHistory;

    public Slider RefSlider;
    public Text textLol;

    [Header("Gestion du calendrier")]

    string[] months = new string[] { "Jan." , "Fev." ,"Mars" , "Avril" , "Mai" , "Juin" , "Juil." , "Aout" , "Sep." , "Oct." , "Nov." , "Dec." }; // liste des mois

    string[] days = new string[] { "Lun.", "Mar.", "Mer.", "Jeu.", "Ven.", "Sam.", "Dim."}; // liste des jours

    public int ActualDayName = 4; // jour de la semaine

    public int ActualMonth = 6; // mois de l'année

    public List<string> calendrier;

    void Awake() 
    {
     
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    void Start()
    {

        refPainter = PainterManager._instance;
        salleRef = Salle._instance;
        salleV2Ref = SalleV2._instance;
        //refUIDeubg = UIDebug._instance;
        refGL = GameLibrary._instance;
        refSM = SoundManager._instance;
        refHistory = HistoryReput._instance;
        i = 0;
        //periodRef = period;
        RefSlider.maxValue = nextActionTime;
        TimerStateRef = 1;

        textLol.text = days[ActualDayName] + " " + actualDay.ToString() + " " + months[ActualMonth];
        calendrier.Add(textLol.text);
    }

    

    void Update()
    {
        //RefSlider.value = Time.time;

        
            timer += Time.deltaTime * TimerStateRef; // opération qui permet au timer d'avancer d' 1 * le multipicateur par seconde

            RefSlider.value = timer; // met à jour l'avancée du slider en fonction du temps qui passe

        GameObject.FindGameObjectWithTag("light").GetComponent<Animator>().SetFloat("Speed", TimerStateRef);

        if (timer >= nextActionTime / 9 * 8 && !isNight)
        {
            GameObject.FindGameObjectWithTag("light").GetComponent<Animator>().SetTrigger("Night");

            isNight = true;
        }
        

        if (timer >= nextActionTime) // si le timer dépasse la valeur définie d'une journée complète
        {
            
            timer = 0; // reset de la journée

            isNight = false;

            ChangeDate();

            CheckReput(); // check la réputation a la fin de la journée + lance une instance painter
                      
        }
    }

    void CheckReput()
    {
        var reputmuseeV2 = salleV2Ref.ReputationAllSalle;
        refGL.reputation += reputmuseeV2;
        refSM.ChangementDeJournee();
        refPainter.PostPainter();

        refHistory.UpdateHistory();
        refHistory.UpdateArrow();
        if(refHistory.LittleHisto.activeSelf)
        refHistory.HideLittleHisto();
    }

    public void UpdatePeriod(int updateTime)
    {
        TimerStateRef = updateTime; 
    }

    public void ChangeDate()
    {
        actualDay++; // mettre plus 1 a l'indicateur de jours (au mois)

        if (ActualMonth == 1) // si le mois est février
        {
            if (actualDay > 28) // changer le mois aprés le 28
            {
                actualDay = 1;
                ActualMonth++;

            }
        }
        else if (actualDay > 31)
        {
            actualDay = 1;
            ActualMonth++;

            if (ActualMonth > 11)
            {
                ActualMonth = 0;
            }
        }

        TotalPassedDays++; // plus 1 au nombre total de jours passé (au total)

        if (ActualDayName < 6) // si pas dimanche(6) continuer la semaine
        {
            ActualDayName++;
        }
        else // reset si dimanche(6), pour repasser a lundi(0)
        {
            ActualDayName = 0;
        }

        textLol.text = days[ActualDayName] + " " + actualDay.ToString() + " " + months[ActualMonth];
        calendrier.Add(textLol.text);
    }


}

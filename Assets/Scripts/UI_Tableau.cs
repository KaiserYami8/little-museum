﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Tableau : MonoBehaviour
{
    public static UI_Tableau _instance;
    Camera cam;

    public GameObject window;

    [HideInInspector]
    public Proto_Tableau_Scriptable tab;

    Vector3 tabPos;
    bool posToScreen;

    private UIController refUIControl;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    private void Start()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        refUIControl = UIController._instance;
    }

    public void EnableWindow(Proto_Tableau_Scriptable tableau)
    {
        window.SetActive(true);
        tab = tableau;

        window.GetComponent<FlyInfoPreview>().AfficherInfo(tab);
    }
    public void DisableWindow()
    {
        window.SetActive(false);
        tab = null;
    }
}

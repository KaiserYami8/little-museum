﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class CoefManagerV2 : MonoBehaviour
{
    public static CoefManagerV2 _instance;

    public float[,] CoefficientNbTableau;
    //0 Mouvement/1 Periode/2 Style/3 Elements
    public float[] CoefMultiplicateur;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        ChangeCoefNbTableau();
    }

    //Move
    public void ChangeCoefNbTableau()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "coef_nb_tableau.csv");
        var rows = File.ReadAllLines(path).Select(l => l.Split(',').ToArray()).ToArray();
        //print($"ROWSL Move: {rows.Length}");
        //print($"ROWSL 0: {rows[0].Length}");
        CoefficientNbTableau = new float[rows.Length, rows[0].Length];
        for (int i = 0; i < rows.Length; i++)
        {
            for (int j = 0; j < rows[i].Length; j++)
            {
                //print($"ROWSL j: {rows[j].Length}");
                //print(rows[i][j]);
                CoefficientNbTableau[i, j] = float.Parse(rows[i][j], System.Globalization.CultureInfo.InvariantCulture);
               
            }
        }
        //print(CoefficientNbTAbleau[NbTableau, Type De Tags];
        //print(CoefficientNbTableau[1, 0]);
        //print(CoefficientNbTableau[5, 0]);
    }
}

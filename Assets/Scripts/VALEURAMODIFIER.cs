﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class VALEURAMODIFIER : MonoBehaviour
{

    public static VALEURAMODIFIER _instance;

    public float coefTableauSolo;
    public float valueUpdateTableau;

    [Header("l'index du tableau corespont à la salle du même index")]
    public float[] reputForSalles;
    //public float reputForSalle2;
    //public float reputForSalle3;
    [Header("Gere la couleur des lignes selon les coefs")]
    public float coefForGreenLine;
    public float coefForRedLine;
    private float tempcoefForGreenLine;
    private float tempcoefForRedLine;



    private float tempCoefTableau;
    private float tempUpdateTableeau;


    
    
    [Header("Index 1 = salle 1 / Index 2 = Salle 2")]
    public int[] nbTableauMax;
    private int[] tempNbTableauParSalle;


    private SalleV2 refSalle;
    private Proto_Temporalite refProtoTemp;
    private ArenaMode refArenaMode;
    private Proto_Line refProtoLine;
    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);

        

    }

    private void Start()
    {
        
        refSalle = SalleV2._instance;
        refProtoTemp = Proto_Temporalite._instance;
        refArenaMode = ArenaMode._instance;
        refProtoLine = Proto_Line._instance;


        tempNbTableauParSalle = new int[refSalle.NbrSalle];

    }
    public void TableauSolo()
    {
        refSalle.defaultCoef = coefTableauSolo;
        tempCoefTableau = coefTableauSolo;
        
    }
    public void UpdateTableauReput() 
    {
        refProtoTemp.nextActionTime = valueUpdateTableau;
        tempUpdateTableeau = valueUpdateTableau;
    }

    public void NbTableauMax()
    {
        refSalle.NbrTableauParSalle = nbTableauMax;
        tempNbTableauParSalle = nbTableauMax;
    }

    public void SetCoefLineGreen()
    {
        refProtoLine.coefForGreen = coefForGreenLine;
        tempcoefForGreenLine = coefForGreenLine;
    }
    public void SetCoefLineRed()
    {
        refProtoLine.coefForRed = coefForRedLine;
        tempcoefForRedLine = coefForRedLine;
    }


    public void Update()
    {
        if (tempCoefTableau != coefTableauSolo)
            TableauSolo();

        //if (tempUpdateTableeau != valueUpdateTableau)
        //    UpdateTableauReput();

        if (!Enumerable.SequenceEqual(nbTableauMax, tempNbTableauParSalle))
            NbTableauMax();

        //if (tempcoefForGreenLine != coefForGreenLine)
        //    SetCoefLineGreen();

        //if (tempcoefForRedLine != coefForRedLine)
        //    SetCoefLineRed();
    }


    


}

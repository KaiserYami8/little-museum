﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebug : MonoBehaviour
{
    public static UIDebug _instance;

    public bool VisibilityDebug;
    public GameObject MenuDebug;

    public Text TextReputGlobal;
    public Text TextReputMusee;

    [Header("Nbr Salle+1")]
    public Text[] TextReputSalles;

    public bool showLinks;


    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    void Start()
    {

    }

    public void ShowLinks(bool show)
    {
        showLinks = show;
    }

    public void ChangeTextReputGlobal(float value)
    {
        TextReputGlobal.text = "Reputation Totale : " + value;
    }

    public void ChangeTextReputMusee(float value)
    {
        TextReputMusee.text = "Reputation Musée : " + value;
    }


    public void ChangeTextReputSalle(float value, int idSalle)
    {
        //print("affichereput");
        var valueInInt = (int)value;
        TextReputSalles[idSalle].text = $"Reputation Salle{idSalle} : " + valueInInt;

    }
}

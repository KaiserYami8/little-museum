﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Playables;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{
    //Singelton
    public static QuestManager _instance;

    public Quest[] quests;

    public int activeQuest;

    public Text questText;

    public GameObject paintingSelection;
    public GameObject butCloseReward;
    private Vector3 OpenPosArenLM;
    private Vector3 PosArenLM;
    public int numberToPick;

    [Header("ButtQuest")]
    public GameObject ButtonNotifQuest;
    public Sprite checkImage;
    public Sprite unCheck;

    GameLibrary GL;
    Proto_Inventory PI;
    GenerationMur GenM;
    UnlockSalle refUnlockSalle;
    SalleV2 SALLE;
    SoundManager refSound;
    UIController UIControlRef;
    NarateurManager refNarrateur;

    public int nbFinishQuest;
    public List<GameObject> ListButtReward;
    public List<GameObject> ListButtRewardSelected;
    public GameObject PrefabButReward;
    public Transform parentButReward;
    public Button ButtonValidQuest;
    public Text refTabToTake;
    public GameObject refObjectifModeAreneLM;
    private bool triggerOnce;


    public List<GameObject> ListQuestObjectif;
    public GameObject RefParentQuestObjectif;

    //UI QUEST
    public GameObject questEmpty;
    public GameObject descriptionQuest;
    public GameObject objectifPrefab;
    GameObject objectifGO;
    public Button questReward;
    private bool isSpawn;
    public GameObject notifIcon;
    public GameObject PanelQuest;

    [Header("Feedbacks")]
    public GameObject feedbackGetPaint;


    private bool isClicked;
    private bool isClicked2;
    private Vector3 originPosCurseur;
    private Vector3 originPosCurseur2;
    public bool isQuestsOver;
    public Button refButGeneralQuest;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        GL = GameLibrary._instance;
        PI = Proto_Inventory._instance;
        SALLE = SalleV2._instance;
        GenM = GenerationMur._instance;
        refSound = SoundManager._instance;
        UIControlRef = UIController._instance;
        refUnlockSalle = UnlockSalle._instance;
        refNarrateur = NarateurManager._instance;

        //numberToPick = quests[activeQuest].numberToPick;
        //ListButtRewardSelected = new List<GameObject>();
        OpenPosArenLM = new Vector3(10000, 10000, 0);
        PosArenLM = paintingSelection.transform.position;

        HardResetQuests();

        UpdateInfoQuest();
        SetupQuest();
    }

    void Update()
    {
            QuestUpdate();
    }

    void QuestUpdate()
    {
        //Quest Completed
        //print(CheckIfQuestValid());
        if (/*GL.reputation >= quests[activeQuest].reputationGoal*/ activeQuest < quests.Length && CheckIfQuestValid())
        {  
            QuestReward();
        }
    }

    void QuestReward()
    {

        if (!triggerOnce)
        {
            refSound.QueteReussie();
            questReward.interactable = true;
            //questEmpty.transform.GetChild(1).GetComponentInChildren<Image>().sprite = checkImage;
            notifIcon.SetActive(true);
            triggerOnce = true;
            CheckIfCanReward();

            UpdateObjectifMenuReward();

            if (!refNarrateur.isEventGone[2] && refNarrateur.NarrateurEnabled)
            {
                refNarrateur.AppelNarrateurEvent(2);
                refNarrateur.isEventGone[2] = true;
            }
        }
        
    }
    public void NotificationQuest()
    {
        //paintingSelection.SetActive(true);
        questReward.interactable = false;
        unCheck = checkImage;
        UIControlRef.PauseGame();
        StartCoroutine("ShowWindow");

    }

    public IEnumerator ShowWindow()
    {
        paintingSelection.transform.position = OpenPosArenLM;
        paintingSelection.SetActive(true);
        yield return new WaitForSeconds(.001f);
        paintingSelection.SetActive(false);
        paintingSelection.SetActive(true);
        yield return new WaitForSeconds(.001f);
        paintingSelection.SetActive(false);
        paintingSelection.SetActive(true);
        yield return new WaitForSeconds(.001f);
        paintingSelection.SetActive(false);
        yield return new WaitForSeconds(.001f);
        paintingSelection.SetActive(true);
        questReward.interactable = false;
        unCheck = checkImage;
        UIControlRef.PauseGame();
        paintingSelection.transform.position = PosArenLM;
        yield return null;

    }


    public void QuestCompleted()
    {
        paintingSelection.SetActive(false);
        PanelQuest.SetActive(false);
        ResetQuests();

        if (numberToPick != 0)
        {
            Invoke("AddTabToInventory", 0.1f);
            //print("numberToPick != 0");
        }
        else
        {
            triggerOnce = false;
            Invoke("SetupQuest", 0.1f);
            //print("numberToPick == 0");
        }
           


        if (quests[activeQuest].UnlockSalle)
        {
            refUnlockSalle.unlockBon += quests[activeQuest].nbBON;
            UIControlRef.CheckBonTableau();
            //SetupQuest();
        }

        if (activeQuest + 1 < quests.Length) activeQuest++;
        else isQuestsOver = true;

        //SetupQuest();

        isSpawn = false;
        
        questReward.interactable = false;
        notifIcon.SetActive(false);
        nbFinishQuest++;

        foreach (var item in ListQuestObjectif)
        {
            Destroy(item);
        }

        UpdateInfoQuest();
        UIControlRef.PlayGame();
        PanelQuest.SetActive(true);
        
        refSound.RecompenseObtenue();
        //Destroy(questEmpty.transform.GetChild(1).gameObject);

        //SetupQuest();
        //triggerOnce = false;
    }

    private void AddTabToInventory()
    {
        var refTab = ListButtRewardSelected[0].GetComponent<RewardBut>().refTableau;
        //print(refTab);
        GL.tableau_never_use.Remove(refTab);
        PI.Inventaire.Add(refTab);

        var fgp = Instantiate(feedbackGetPaint, ListButtRewardSelected[0].transform.position, Quaternion.identity, ListButtRewardSelected[0].transform.root.GetChild(0));
        fgp.GetComponent<GetTabTween>().rect.GetComponent<Image>().sprite = ListButtRewardSelected[0].GetComponent<RewardBut>().imageTableau.sprite;

        ListButtRewardSelected.RemoveAt(0);

        refSound.TableauIndividuelGagne();
        if (ListButtRewardSelected.Count == 0)
        {
            //refSound.AcquisitionTableau();
            if (refNarrateur.isQuestOver)
            {
                if (!refNarrateur.isEventGone[7] && refNarrateur.NarrateurEnabled)
                {
                    refNarrateur.AppelNarrateurEvent(7);
                    refNarrateur.isEventGone[7] = true;
                }
            }

            SetupQuest();

            triggerOnce = false;
        }
        else
        {
            Invoke("AddTabToInventory", 0.1f);
        }
        
    }

    void UpdateInfoQuest()
    {

        //SET PANEL QUEST
        descriptionQuest.GetComponent<Text>().text = quests[activeQuest].description;
        //Remplacer par initialize quest Objectif

        ListQuestObjectif = InitializeQuestObjectif();
        //var objquest = Instantiate(objectifPrefab, questEmpty.transform);
        //objquest.transform.SetSiblingIndex(1);
        //objquest.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text;
    }

    //A compléter
    public void CloseMenuReward()
    {
        UIControlRef.PlayGame();
        paintingSelection.SetActive(false);
        questReward.interactable = true;
    }

    public void SetupQuest()
    {
        if (!isQuestsOver)
        {
            if (ListButtReward.Count > 0) DestroyBut();



            ListButtRewardSelected = new List<GameObject>();
            int nbrTabMax;

            if (quests[activeQuest].Tableau)
            {
                nbrTabMax = quests[activeQuest].paintings.Length;
                numberToPick = quests[activeQuest].numberToPick;
                //print("tableau is active");
            }
            else
            {
                nbrTabMax = 0;
                numberToPick = 0;
                //print("tableau is not active");

            }
            //print("numberToPick " + numberToPick); 
            //print("nbrTabMax " + nbrTabMax);


            refTabToTake.text = $"Vous pouvez récupérer {numberToPick} Tableau(x)";

            ListButtReward = new List<GameObject>();

            for (int i = 0; i < nbrTabMax; i++)
            {
                var newBut = Instantiate(PrefabButReward, parentButReward);
                newBut.GetComponent<RewardBut>().refTableau = quests[activeQuest].paintings[i];
                ListButtReward.Add(newBut);
            }

            ButtonValidQuest.interactable = false;

            if (!refNarrateur.isQuestOver)
            {
                if (refNarrateur.NarrateurEnabled)
                {
                    refNarrateur.IndexNarrateurQuest = activeQuest;
                    if (!refNarrateur.isEventGone[0])
                    {
                        refNarrateur.AppelNarrateurEvent(0);
                        refNarrateur.isEventGone[0] = true;
                    }
                    else
                    {
                        refNarrateur.AppelNarrateurQuest();
                    }

                    //UIControlRef.PauseGame();

                }
            }
        }
        else
        {
            PanelQuest.SetActive(false);
            refButGeneralQuest.interactable = false;
        }
        

       

    }

    public void DestroyBut()
    {
        foreach (var item in ListButtReward)
        {
            Destroy(item);
        }
    }

    public void CheckSelectedButtons(GameObject itemToAdd)
    {
        if(ListButtRewardSelected.Count < numberToPick)
        {
            ListButtRewardSelected.Add(itemToAdd);
        }
        else if(ListButtRewardSelected.Count == numberToPick)
        {
            ListButtRewardSelected.Add(itemToAdd);
            ListButtRewardSelected[0].GetComponent<RewardBut>().Deselected();
            ListButtRewardSelected.RemoveAt(0);
        }

        CheckIfCanReward();
    }

    public void DeselectButton(GameObject itemToRemove)
    {
        //print(ListButtRewardSelected.Contains(itemToRemove));
        if(ListButtRewardSelected.Contains(itemToRemove)) ListButtRewardSelected.Remove(itemToRemove);

        CheckIfCanReward();



    }

    public void CheckIfCanReward()
    {
        if (quests[activeQuest].Tableau)
        {
            if (ListButtRewardSelected.Count == numberToPick)
            {
                ButtonValidQuest.interactable = true;
            }
            else
            {
                ButtonValidQuest.interactable = false;
            }
        }
        else
        {
            ButtonValidQuest.interactable = true;
        }
        
    }

    public void UpdateObjectifMenuReward()
    {
        var maxObj = ListQuestObjectif.Count;
        for (int i = 0; i < 3; i++)
        {
            var child = refObjectifModeAreneLM.transform.GetChild(i);
            if (i < maxObj)
            {
                
                child.gameObject.SetActive(true);
                child.GetComponentInChildren<Text>().text = ListQuestObjectif[i].GetComponentInChildren<Text>().text;

            }
            else
            {
                child.gameObject.SetActive(false);
            }
        }
    }

    public List<GameObject> InitializeQuestObjectif()
    {
        //initialize les quêtes (élément avec l'image check)
        List<GameObject> listQuest = new List<GameObject>();

        if (quests[activeQuest].controlesBool)
        {
            for (int i = 0; i < 3; i++)
            {
               var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
                switch (i)
                {
                    case 0:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text1;
                        break;
                    case 1:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text2;
                        break;
                    case 2:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text3;
                        break;

                }
                
                listQuest.Add(newObj);
            }

            return listQuest;
        }
        else if (quests[activeQuest].tableauxBool)
        {
            for (int i = 0; i < 3; i++)
            {
                var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
                switch (i)
                {
                    case 0:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text1;
                        break;
                    case 1:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text2;
                        break;
                    case 2:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text3;
                        break;

                }
                listQuest.Add(newObj);
            }
            return listQuest;
        }
        else if (quests[activeQuest].combinaisonBool)
        {
            for (int i = 0; i < 2; i++)
            {
                var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
                switch (i)
                {
                    case 0:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_text1;
                        break;
                    case 1:
                        newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_textSameCourant;
                        break;

                }
                listQuest.Add(newObj);
            }
            return listQuest;
        }

        if (quests[activeQuest].reputationBool)
        {
            var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
            newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_textReput;
            listQuest.Add(newObj);
        }

        if (quests[activeQuest].compareDeuxTabBool)
        {
            var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
            newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_textcomparaison;
            listQuest.Add(newObj);
        }

        if (quests[activeQuest].deblocageSalleBool)
        {
            var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
            newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_textAggrandissement;
            listQuest.Add(newObj);
        }

        if (quests[activeQuest].sameCourantTabsBool && !quests[activeQuest].combinaisonBool)
        {
            var newObj = Instantiate(objectifPrefab, RefParentQuestObjectif.transform);
            newObj.GetComponentInChildren<Text>().text = quests[activeQuest].objectif_textSameCourant;
            listQuest.Add(newObj);
        }


        return listQuest;
    }


    private bool CheckIfQuestValid()
    {
        //GL.reputation >= quests[activeQuest].reputationGoal && activeQuest < quests.Length
        bool isValid = false;
        int isError = 0;
        if (quests[activeQuest].controlesBool)
        {
            if (!quests[activeQuest].deplacementCam)
            {
                if (CheckMouvement())
                {
                    isValid = true;
                    quests[activeQuest].deplacementCam = true;
                }
                else isError++;
            }
            else isValid = true;

            if (!quests[activeQuest].zoom)
            {
                if (CheckZoom())
                {
                    isValid = true;
                    quests[activeQuest].zoom = true;
                }
                else isError++;
            }
            else isValid = true;

            if (!quests[activeQuest].pivoterCam)
            {
                if (CheckRotate())
                {
                    isValid = true;
                    quests[activeQuest].pivoterCam = true;
                }
                else isError++;
            }
            else isValid = true;

            return resultIsGood(isValid, isError);
        }
        else if (quests[activeQuest].tableauxBool)
        {
 
             if (!quests[activeQuest].ouvertureCollection)
            {
                isError++;
            }
            else isValid = true;

            if (!quests[activeQuest].selectionTab)
            {
                isError++;
            }
            else isValid = true;

            if (!quests[activeQuest].poseTab)
            {
                isError++;
            }
            else isValid = true;

            return resultIsGood(isValid, isError);
}
        else if (quests[activeQuest].combinaisonBool)
        {
            if (!quests[activeQuest].clickTab)
            {
                isError++;
            }
            else isValid = true;

            //Faire une fonction dans SalleV2 qui check si il y a bien X tableau avec le courant Y et qui set le bool sur true
            if (!quests[activeQuest].sameCourantTabs)
            {
                isError++;
            }
            else isValid = true;



            return resultIsGood(isValid, isError);
        }

        if (quests[activeQuest].reputationBool)
        {
            if (!quests[activeQuest].Reputation)
            {
                if (GL.reputation >= quests[activeQuest].reputationGoal && activeQuest < quests.Length)
                {
                    isValid = true;
                    quests[activeQuest].Reputation = true;
                    ListQuestObjectif[CheckObjectifPos(objectif.reputation)].GetComponentInChildren<Image>().sprite = checkImage;
                }
                else
                {
                    isValid = false;
                    isError++;
                }
            }
            else
            {
                isValid = true;
            }


        }

        if (quests[activeQuest].compareDeuxTabBool)
        {
            if (!quests[activeQuest].compareDeuxTab)
            {
                isError++;
            }
            else isValid = true;
        }

        

        if (quests[activeQuest].deblocageSalleBool)
        {
            if (!quests[activeQuest].deblocageSalle)
            {
                isError++;
            }
            else isValid = true;
        }

        if (quests[activeQuest].sameCourantTabsBool && !quests[activeQuest].combinaisonBool)
        {
            if (!quests[activeQuest].sameCourantTabs)
            {
                isError++;
            }
            else isValid = true;

        }
        
        return resultIsGood(isValid, isError);
    }

    private bool resultIsGood(bool isValid, int isError)
    {
        if (isError>0) return false;
        else return isValid;
    }





    private bool CheckMouvement()
    {
        if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D) || CheckMouseMovement())
        {
            //questEmpty.transform.GetChild(1).GetComponentInChildren<Image>().sprite = checkImage;
            ListQuestObjectif[0].GetComponentInChildren<Image>().sprite = checkImage;
            return true;
        }
        else return false;
    }
    private bool CheckZoom()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            //questEmpty.transform.GetChild(1).GetComponentInChildren<Image>().sprite = checkImage;
            ListQuestObjectif[1].GetComponentInChildren<Image>().sprite = checkImage;
            return true;
        }
        else return false;
    }

    private bool CheckRotate()
    {
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.E) || CheckRotateMouse())
        {
            //questEmpty.transform.GetChild(1).GetComponentInChildren<Image>().sprite = checkImage;
            ListQuestObjectif[2].GetComponentInChildren<Image>().sprite = checkImage;
            return true;
        }
        else return false;
    }

    private bool CheckMouseMovement()
    {
        if (Input.GetKeyDown(KeyCode.Mouse2) && !isClicked2)
        {
            isClicked2 = true;
            originPosCurseur2 = Input.mousePosition;
        }

        if (Input.GetKeyUp(KeyCode.Mouse2))
        {
            isClicked2 = false;
        }

        if (Input.mousePosition != originPosCurseur2 && isClicked2) return true;
        else return false;
    }

    private bool CheckRotateMouse()
    {
        if(Input.GetKeyDown(KeyCode.Mouse1) && !isClicked)
        {
            isClicked = true;
            originPosCurseur = Input.mousePosition;
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            isClicked = false;
        }

        if (Input.mousePosition != originPosCurseur && isClicked) return true;
        else return false;
    }
    public void CheckOpenInventary()
    {
        quests[activeQuest].ouvertureCollection = true;
        ListQuestObjectif[0].GetComponentInChildren<Image>().sprite = checkImage;
    }

    //Check si un tableau à été cliquer dans l'inventaire
    public void CheckSelectTab()
    {
        quests[activeQuest].selectionTab = true;
        ListQuestObjectif[1].GetComponentInChildren<Image>().sprite = checkImage;
    }

    public void CheckPoseTab()
    {
        quests[activeQuest].poseTab = true;
        ListQuestObjectif[2].GetComponentInChildren<Image>().sprite = checkImage;
    }

    //Check si un tableau à été cliquer dans le musée
    public void CheckClickedTab()
    {
        quests[activeQuest].clickTab = true;
        ListQuestObjectif[0].GetComponentInChildren<Image>().sprite = checkImage;
    }

    //Quand la fonction de comparaison sera ajouter lui faire appeler cette fonction
    public void CheckCompareTab()
    {
        quests[activeQuest].compareDeuxTab = true;
        
            ListQuestObjectif[CheckObjectifPos(objectif.compareDeuxTab)].GetComponentInChildren<Image>().sprite = checkImage;
     
    }

    public void CheckNbTabByCourant()
    {
        quests[activeQuest].sameCourantTabs = true;

        ListQuestObjectif[CheckObjectifPos(objectif.sameCourantTabs)].GetComponentInChildren<Image>().sprite = checkImage;
    }


    public void CheckDeblocageSalle()
    {
        quests[activeQuest].deblocageSalle = true;
        ListQuestObjectif[CheckObjectifPos(objectif.deblocageSalle)].GetComponentInChildren<Image>().sprite = checkImage;
    }


    public int CheckObjectifPos(objectif typeObjectif)
    {
        string txtObj = "";
        switch (typeObjectif)
        {
            case objectif.sameCourantTabs:
                txtObj = quests[activeQuest].objectif_textSameCourant;
                break;
            case objectif.compareDeuxTab:
                txtObj = quests[activeQuest].objectif_textcomparaison;
                break;
            case objectif.deblocageSalle:
                txtObj = quests[activeQuest].objectif_textAggrandissement;
                break;
            case objectif.reputation:
                txtObj = quests[activeQuest].objectif_textReput;
                break;
            default:
                txtObj = "";
                break;
        }

        for (int i = 0; i < ListQuestObjectif.Count; i++)
        {
            var txt = ListQuestObjectif[i].GetComponentInChildren<Text>().text;
            if(txt == txtObj)
            {
                return i;
            }
        }

        return -1;
    }

    public enum objectif
    {
        sameCourantTabs =0,
        compareDeuxTab =1,
        deblocageSalle = 2,
        reputation = 3
    }

    public void ResetQuests()
    {
        
            quests[activeQuest].deplacementCam = false;
            quests[activeQuest].zoom = false;
            quests[activeQuest].pivoterCam = false;
            quests[activeQuest].ouvertureCollection = false;
            quests[activeQuest].selectionTab = false;
            quests[activeQuest].poseTab = false;
            quests[activeQuest].clickTab = false;
            quests[activeQuest].sameCourantTabs = false;
            quests[activeQuest].compareDeuxTab = false;
            quests[activeQuest].deblocageSalle = false;
            quests[activeQuest].Reputation = false;
        
    }

    public void HardResetQuests()
    {
        foreach (var item in quests)
        {
            item.deplacementCam = false;
            item.zoom = false;
            item.pivoterCam = false;
            item.ouvertureCollection = false;
            item.selectionTab = false;
            item.poseTab = false;
            item.clickTab = false;
            item.sameCourantTabs = false;
            item.compareDeuxTab = false;
            item.deblocageSalle = false;
            item.Reputation = false;
        }
        

    }
}

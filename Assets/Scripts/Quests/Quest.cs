﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

[CreateAssetMenu(fileName = "Quest", menuName = "Quest", order = 1)]
public class Quest : ScriptableObject
{
    public string description;
    //Text Objectif des quêtes primaire du tuto (Controle, tableaux et Combinaison)
    public string objectif_text1;
    public string objectif_text2;
    public string objectif_text3;

    //Txt par objectif Spécial
    public string objectif_textReput;
    public string objectif_textSameCourant;
    public string objectif_textcomparaison;
    public string objectif_textAggrandissement;

    //OBJECTIF
    public enum ValueCourant
    {
        Manierisme = 0,
        Impressionnisme = 1,
        Cubisme = 2,
        Surrealisme = 3,
        Artnouveau = 4
    }



    [Header("Objectif Section")]
    //Quête avec un nombre de réputation à avoir
    public bool reputationBool;
    public bool Reputation;



    [Header("Si Reputation est activé remplir en dessous")]
    //Need Reputation
    public int reputationGoal;

    //Need sameCourantTabs
    public ValueCourant CourantRequired;
    public int NbTabRequired;

    [Header("Objectif Section Controles")]
    public bool controlesBool;
    public bool deplacementCam;
    public bool zoom;
    public bool pivoterCam;

    [Header("Objectif Section Tableaux")]
    public bool tableauxBool;
    public bool ouvertureCollection;
    public bool selectionTab;
    public bool poseTab;

    [Header("Objectif Section Combinaison")]
    //Combinaison comprend clickTab & sameCourantTabs
    public bool combinaisonBool;
    public bool clickTab;


    [Header("Objectif Section NonPrincipaux")]
    //Quête avec un nbr X de tableau ayant un courant Y
    public bool sameCourantTabsBool;
    public bool sameCourantTabs;

    [Header("Objectif Section Comparaison")]
    public bool compareDeuxTabBool;
    public bool compareDeuxTab;

    [Header("Objectif Section Aggrandissment")]
    public bool deblocageSalleBool;
    public bool deblocageSalle;



    [Header("Rewards Section")]
    public bool Tableau;
    public bool UnlockSalle;


    [Header("Si Tableau est activé remplir en dessous")]
    //VALEUR SI TABLEAU A GAGNER
    [Min(1)]
    public int numberToPick;
    public Proto_Tableau_Scriptable[] paintings;

    [Header("Si UnlockSalle est activé remplir en dessous")]
    //VALEUR SI SALLE A GAGNER
    public int nbBON;
}
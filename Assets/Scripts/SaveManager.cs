﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{


    Salle refSalle;
    GameLibrary refGL;
    Proto_Inventory refProtoInventory;
    public static SaveManager _instance;
    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    void Start()
    {
        refSalle = Salle._instance;
        refGL = GameLibrary._instance;
        refProtoInventory = Proto_Inventory._instance;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J)) LoadTableau();
        if (Input.GetKeyDown(KeyCode.H)) SavePositionTableaux();
    }


    public void SavePositionTableaux()
    {
        ES3.Save<List<GameObject>[]>("ListTableauGO", refSalle.ListTabSalles_GO);
        ES3.Save<List<Proto_Tableau_Scriptable>[]>("ListTableauScript", refSalle.ListTabSalles);
        ES3.Save<float>("Reputation", refGL.reputation);
        ES3.Save<List<Proto_Tableau_Scriptable>>("Inventaire", refProtoInventory.Inventaire);
        print("Save");
    }

    public void LoadTableau()
    {
        refProtoInventory.Inventaire = ES3.Load<List<Proto_Tableau_Scriptable>>("Inventaire");
        refSalle.ListTabSalles_GO = ES3.Load<List<GameObject>[]>("ListTableauGO");
        refSalle.ListTabSalles = ES3.Load<List<Proto_Tableau_Scriptable>[]>("ListTableauScript");
        refGL.reputation = ES3.Load<float>("Reputation");

        for (int i = 0; i < refSalle.ListTabSalles_GO.Length; i++)
        {
            if( i !=0)
            {
                var arrayGO = refSalle.ListTabSalles_GO[i].ToArray();
                var arrayScript = refSalle.ListTabSalles[i].ToArray();

                for (int j = 0; j < arrayGO.Length; j++)
                {
                    arrayGO[j].GetComponent<DisplayTableau>().Tableau = arrayScript[j];
                }
            }   
        }

        foreach (var item in refProtoInventory.Inventaire)
        {
            GameObject button = Instantiate(refProtoInventory.prefab_Boutton, refProtoInventory.parent);
            button.name = item.name + "_button";
            button.GetComponent<Proto_Tableau_Inventaire>().refTableau = item;
            button.GetComponentInChildren<Text>().text = item.name;
            button.GetComponent<Button>().onClick.AddListener(UIController._instance.PosTableau);

            refProtoInventory.buttonInventaire.Add(button);
            refProtoInventory.temp = refProtoInventory.Inventaire.Count;
        }
        print("Load");
    }
}

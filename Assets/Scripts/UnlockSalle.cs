﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockSalle : MonoBehaviour
{

    public static UnlockSalle _instance;
    //Reference to the MainCamera
    Camera Cam;

    public Material colorDestruct;

    public GameObject acceptPanel;
    public Text descriptionAccept;
    public Text countUnlockBon;

    public int unlockBon;
    public Button refBoutonUnlock;
    public GameObject[] allWallUnlock;
    public GameObject[][] wallUnlock;
    public Material[][][] wallUnlockMAT;

    SalleV2 refSalleV2;
    SoundManager refSM;
    Proto_Inventory refInventary;
    GenerationMur refGenMur;
    QuestManager refQuestManager;
    NarateurManager refNarrateur;
    UIController refUIControl;

    public int nbRoomUnlocked;
    private int nbElemToCheck;

    bool isBOBactive;


    RaycastHit hit2;
    int zr;
    int yr;

    private CollisionWallUnlock refToHoverUnlock;
    private GameObject refToHoverUnlockGO;
    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        Cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        refSalleV2 = SalleV2._instance;
        refSM = SoundManager._instance;
        refInventary = Proto_Inventory._instance;
        refGenMur = GenerationMur._instance;
        refQuestManager = QuestManager._instance;
        refNarrateur = NarateurManager._instance;
        refUIControl = UIController._instance;

        UpdatePlan();



    }

    // Update is called once per frame
    void Update()
    {
        if (countUnlockBon.text != unlockBon.ToString())
            UpdateCountText();

        if (unlockBon < 1) refBoutonUnlock.interactable = false;
        else refBoutonUnlock.interactable = true;

        if (isBOBactive) UnlockWall();
    }

    void UpdateCountText()
    {
        //print("infini");
        countUnlockBon.text = unlockBon.ToString();
    }

    public void unlockSalleBut()
    {
        refUIControl.isUnlockSalle();
    }

    public void ForceDisablePanel()
    {
        acceptPanel.SetActive(false);
        refToHoverUnlock.SetupBool(false);
    }

    public void UseBon()
    {

        
        if (!isBOBactive)
        {
            UpdatePlan();

            for (int i = 0; i < wallUnlock.Length; i++)
            {
                if (refGenMur.canConstructSalle[i])
                {
                    for (int j = 0; j < wallUnlock[i].Length; j++)
                    {
                        wallUnlock[i][j].transform.GetChild(0).GetComponent<MeshRenderer>().material = colorDestruct;
                        wallUnlock[i][j].transform.GetChild(1).GetComponent<MeshRenderer>().material = colorDestruct;
                        //print(wallUnlock[i][j].name);
                    }
                }
            }
            isBOBactive = true;
            refSM.ModeExtentionActive();
        }
        else
        {
            isBOBactive = false;

            for (int i = 0; i < wallUnlock.Length; i++)
            {
                if (refGenMur.canConstructSalle[i])
                {
                    for (int j = 0; j < wallUnlock[i].Length; j++)
                    {
                        for (int k = 0; k < 2; k++)
                        {
                            wallUnlock[i][j].transform.GetChild(k).GetComponent<MeshRenderer>().material = wallUnlockMAT[i][j][k];
                            //print(wallUnlock[i][j].name);
                        }
                    }
                }
            }
            refSM.ModeExtentionDesactive();

        }
        //refSalleV2.ChangeLayerTab(isBOBactive);
        
        //print(isBOBactive);
    }

    void UpdatePlan()
    {
        wallUnlock = new GameObject[refSalleV2.NbrSalle][];
        wallUnlockMAT = new Material[wallUnlock.Length][][];

        for (int i = 0; i < wallUnlock.Length; i++)
        {
            List<GameObject> listGO = new List<GameObject>();
            List<Material> listGOMAT = new List<Material>();

            for (int j = 0; j < wallUnlock.Length; j++)
            {
                for (int k = 0; k < allWallUnlock.Length; k++)
                {
                    if (allWallUnlock[k].name == $"UnlockSalle{i}to{j}" || allWallUnlock[k].name == $"UnlockSalle{j}to{i}")
                    {
                        listGO.Add(allWallUnlock[k]);

                    }
                }
            }
            if (listGO.Count > 0)
            {
                wallUnlock[i] = listGO.ToArray();
                //print($"{i} = {wallUnlock[i].Length}");
                wallUnlockMAT[i] = new Material[wallUnlock[i].Length][];
                for (int l = 0; l < wallUnlockMAT[i].Length; l++)
                {
                    wallUnlockMAT[i][l] = new Material[2];
                    for (int m = 0; m < 2; m++)
                    {
                        wallUnlockMAT[i][l][m] = wallUnlock[i][l].transform.GetChild(m).GetComponent<MeshRenderer>().materials[0];
                    }
                }

            }

        }
        //print(wallUnlock.Length);
        //print(refSalleV2.NbrSalle);
        //print(wallUnlock[1].Length);
    }


    void UnlockWall()
    {
        Ray ray = Cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.tag == "Wall")
            {
                for (int i = 0; i < allWallUnlock.Length; i++)
                {
                    if (hit.transform.parent.gameObject == allWallUnlock[i])
                    {
                        ClickSalle(hit);


                    }
                }

            }
        }
    }

    void ClickSalle(RaycastHit hit)
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            //print("coucou");
            acceptPanel.SetActive(true);
            hit2 = hit;
            var name = hit2.transform.parent.name;
            var nameref = name + "Collider";
            refToHoverUnlock = GameObject.Find(nameref).GetComponent<CollisionWallUnlock>();
            refToHoverUnlock.SetupBool(true);


            var x = name.Remove(0, 11);
            var y = x.Remove(1);
            var z = x.Remove(0, 3);
            int.TryParse(y, out yr);
            int.TryParse(z, out zr);
            if (!refGenMur.canConstructSalle[yr])
            {
                descriptionAccept.text = $"VOUS ETES SUR LE POINT DE DEBLOQUER LA SALLE {yr}"/* QUI POSSEDE {refSalleV2.NbrTableauParSalle[yr]} SLOT DE TABLEAU"*/;
            }
            else if (!refGenMur.canConstructSalle[zr])
            {
                descriptionAccept.text = $"VOUS ETES SUR LE POINT DE DEBLOQUER LA SALLE {zr}"/* QUI POSSEDE {refSalleV2.NbrTableauParSalle[zr]} SLOT DE TABLEAU"*/;
            }
            else if (refGenMur.canConstructSalle[yr] && refGenMur.canConstructSalle[zr])
            {
                descriptionAccept.text = $"VOUS ETES SUR LE POINT DE DEBLOQUER UN PASSAGE ENTRE LA SALLLE {yr} ET LA SALLE {zr} QUE VOUS POSSEDEZ DEJA";
            }

            refSM.MurSelectionne();

        }
    }

    public void AcceptDestroy()
    {

        if (zr != 1) refSalleV2.UnlockRoom(zr);
        if (yr != 1) refSalleV2.UnlockRoom(yr);

        foreach (var item in allWallUnlock)
        {
            if (item.name == hit2.transform.parent.name)
            {
                CheckVoisinPourTogglePosTab(item, true);
                item.SetActive(false);
                
            }
        }

        unlockBon--;
        acceptPanel.SetActive(false);
        //UseBon();
        nbRoomUnlocked ++;

        unlockSalleBut();
        PutTabInInventory();

        
        if (refQuestManager.quests[refQuestManager.activeQuest].deblocageSalleBool)
        {
            if (!refQuestManager.quests[refQuestManager.activeQuest].deblocageSalle) refQuestManager.CheckDeblocageSalle();
        }

        refSM.ExtentionDeSalle();
        
    }

    public void CancelDestroy()
    {
        acceptPanel.SetActive(false);
        //UseBon();
        unlockSalleBut();
        refToHoverUnlock.SetupBool(false);
        refToHoverUnlock = null;
    }


    private void PutTabInInventory()
    {
        if(refToHoverUnlock.listTabGO.Count > 0)
        {
            Invoke("tabAdd", 0.1f);
        }
        else
        {
            refToHoverUnlock.SetupBool(false);
        }
    }

    private void tabAdd()
    {

        var numSalle = refToHoverUnlock.listTabGO[0].GetComponent<DisplayTableau>().Tableau.idSalle;
        refInventary.Inventaire.Add(refToHoverUnlock.listTab[0]);

        refSalleV2.ListTabSalles[numSalle].Remove(refToHoverUnlock.listTab[0]);
        refSalleV2.ListTabSalles_GO[numSalle].Remove(refToHoverUnlock.listTabGO[0]);


        refGenMur.RemoveTableauOnListPourCoef(refToHoverUnlock.listTab[0]);

        refSalleV2.CalculateSalleCoef(numSalle);

        refSalleV2.NbrTableauParSalle[numSalle]++;

        Destroy(refToHoverUnlock.listTabGO[0]);

        refToHoverUnlock.listTab.RemoveAt(0);
        refToHoverUnlock.listTabGO.RemoveAt(0);

        if (refToHoverUnlock.listTab.Count == 0)
        {
            refSM.RangerUnTableau();
            refToHoverUnlock.SetupBool(false);
            refToHoverUnlock = null;
        }
        else
        {
            Invoke("tabAdd", 0.1f);
        }
    }


    private void CheckVoisinPourTogglePosTab(GameObject wall, bool enable)
    {
        for (int i = -1; i <= 1; i++)
        {
            
            if (i != 0)
            {
                var refWallScript = wall.GetComponent<WallScript>();
                var orientation = refWallScript.orientation;
                var refPos = refWallScript.PosInArray;
                if (orientation == WallScript.OrientationWall.Horizontal)
                {
                    var refWall = refGenMur.FirstFloorHorizontalWall[(int)refPos.x+i,(int)refPos.z];
                    if(refWall.name != wall.name)
                    {
                        refWall.GetComponent<WallScript>().CantPlaceTableauBehind = enable;
                        refWall.GetComponent<WallScript>().CantPlaceTableauFront = enable;
                    }
                    
                }
                else
                {
                    var refWall = refGenMur.FirstFloorVerticalWall[(int)refPos.x, (int)refPos.z+i];
                    if (refWall.name != wall.name)
                    {
                        refWall.GetComponent<WallScript>().CantPlaceTableauBehind = enable;
                        refWall.GetComponent<WallScript>().CantPlaceTableauFront = enable;
                    }
                }
                
            }
        }
    }
}

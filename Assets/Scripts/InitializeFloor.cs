﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitializeFloor : MonoBehaviour
{
    UIController uiControlRef;
    GenerationMur GenMurRef;
    public int idSalle;

    private bool oneShoot;

    void Start()
    {
        uiControlRef = UIController._instance;
        GenMurRef = GenerationMur._instance;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!oneShoot)
        {

            oneShoot = true;
            for (int i = 0; i < transform.childCount; i++)
            {
                var enfant = transform.GetChild(i).gameObject;
                uiControlRef.allFloor.Add(enfant);
                enfant.GetComponent<FloorControler>().IdSalle = idSalle;

                //Behind puisFront

                //MurAGauche
                if (GenMurRef.FirstFloorVerticalWall[(int)enfant.transform.position.x, (int)enfant.transform.position.z] != null)
                    GenMurRef.FirstFloorVerticalWall[(int)enfant.transform.position.x, (int)enfant.transform.position.z].transform.GetChild(1).gameObject.GetComponent<WallMaterial>().IdSalle = idSalle;

                //MurEnBas
                if (GenMurRef.FirstFloorHorizontalWall[(int)enfant.transform.position.x, (int)enfant.transform.position.z] != null)
                    GenMurRef.FirstFloorHorizontalWall[(int)enfant.transform.position.x, (int)enfant.transform.position.z].transform.GetChild(0).gameObject.GetComponent<WallMaterial>().IdSalle = idSalle;

                //MurEnHaut
                if (GenMurRef.FirstFloorHorizontalWall[(int)enfant.transform.position.x, (int)enfant.transform.position.z + 1] != null)
                    GenMurRef.FirstFloorHorizontalWall[(int)enfant.transform.position.x, (int)enfant.transform.position.z + 1].transform.GetChild(1).gameObject.GetComponent<WallMaterial>().IdSalle = idSalle;

                //MurADroite
                if (GenMurRef.FirstFloorVerticalWall[(int)enfant.transform.position.x + 1, (int)enfant.transform.position.z] != null)
                    GenMurRef.FirstFloorVerticalWall[(int)enfant.transform.position.x + 1, (int)enfant.transform.position.z].transform.GetChild(0).gameObject.GetComponent<WallMaterial>().IdSalle = idSalle;

            }
        }
    }
}

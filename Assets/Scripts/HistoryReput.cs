﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class HistoryReput : MonoBehaviour
{
    public static HistoryReput _instance;


    SalleV2 refSalle;
    Proto_Temporalite refTempo;
    GameLibrary refGL;

    List<float>[] reputHistoryList;
    public List<float> allReputSalle;
    public Sprite[] arrowList;
    public GameObject arrowReput;
    public Text reputText;

    public GameObject LittleHisto;
    public Text LittleHistoToday;
    public Text LittleHistoHier;

    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        refSalle = SalleV2._instance;
        refTempo = Proto_Temporalite._instance;
        refGL = GameLibrary._instance;

        reputHistoryList = new List<float>[refSalle.NbrSalle];
        for (int i = 0; i < reputHistoryList.Length; i++)
        {
            reputHistoryList[i] = new List<float>();
        }
        allReputSalle = new List<float>();

        allReputSalle.Add(refSalle.ReputationAllSalle);
    }

    

    public void UpdateHistory()
    {
        for (int i = 0; i < refSalle.NbrSalle; i++)
        {
            reputHistoryList[i].Add(refSalle.ReputationSalles[i]);
        }

        allReputSalle.Add(refSalle.ReputationAllSalle);

        reputText.text = $"{refGL.reputation}";
    }


    public void UpdateArrow()
    {
        if(allReputSalle.Count >= 2)
        {
            var actualDay = refTempo.actualDay;
            arrowReput.SetActive(true);



            if(actualDay >= 3)
            { 
                if (allReputSalle[actualDay - 2] > allReputSalle[actualDay - 3])
                {
                    arrowReput.GetComponent<Image>().sprite = arrowList[0];
                    arrowReput.GetComponent<Image>().color = Color.green;

                }
                else if (allReputSalle[actualDay - 2] == allReputSalle[actualDay - 3])
                {
                    arrowReput.GetComponent<Image>().sprite = arrowList[1];
                    arrowReput.GetComponent<Image>().color = Color.yellow;

                }
                else if (allReputSalle[actualDay - 2] < allReputSalle[actualDay - 3])
                {
                    arrowReput.GetComponent<Image>().sprite = arrowList[2];
                    arrowReput.GetComponent<Image>().color = Color.red;

                }
            }
            
        }
        else
        {
            arrowReput.SetActive(false);
        }
    }

    public void HideLittleHisto()
    {
        if (allReputSalle.Count > 2)
        {
            var actualDay = refTempo.actualDay;

            LittleHisto.SetActive(true);
            LittleHistoToday.text = $"{refTempo.calendrier[actualDay - 2]} : {allReputSalle[actualDay - 1]}";
            LittleHistoHier.text = $"{refTempo.calendrier[actualDay - 3]} : {allReputSalle[actualDay - 2]}";
        }
        
    }
    public void UnHideLittleHisto()
    {
        LittleHisto.SetActive(false);
    }

}

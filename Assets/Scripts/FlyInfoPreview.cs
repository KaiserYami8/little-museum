﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlyInfoPreview : MonoBehaviour
{
    public Image previewTableau;

    public Text mouvementText;
    public Text elementsText;
    public Text styleText;

    public Text nomTableau;
    public Text artisteTableau;

    public Text dateTableau;
    public Text dimensionsTableau;
    public Text descriptionTableau;

    public void AfficherInfo(Proto_Tableau_Scriptable refTableau)
    {
        previewTableau.sprite = refTableau.imageOeuvre;
        nomTableau.text = $"Nom : {refTableau.titre}";
        artisteTableau.text = $"Auteur : {refTableau.auteur}";
        dateTableau.text = $"Annee : {refTableau.Annee}";
        descriptionTableau.text = $"Description : {refTableau.description}";
        dimensionsTableau.text = $"Dimension (HxL) : {refTableau.HauteurTableau} x {refTableau.LargeurTableau} cm";
        mouvementText.text = refTableau.courantArtTableau.ToString();
        elementsText.text = refTableau.elements.ToString();
        styleText.text = refTableau.style.ToString();
    }
}

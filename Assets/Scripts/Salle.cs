﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Salle : MonoBehaviour
{
    public static Salle _instance;

    [Header("nbrSalle+1")]
    public int NbrSalle;

    public List<Proto_Tableau_Scriptable>[] ListTabSalles;  
    public List<GameObject>[] ListTabSalles_GO;
    public int[] NbrTableauParSalle;


    public float ReputationAllSalle;


    public float[] ReputationSalles;


    private GenerationMur GenMurRef;
    private CoefManager CoefRef;
    private UIDebug UiDebugRef;


    private int ElementCheck1;
    private int ElementCheck2;

    private GameObject tabCheck1;
    private GameObject tabCheck2;

    private float Coef=0;
    float iteration=0;

    [Header("Value Default")]
    public float defaultCoef;


    [Header("ElemSalleToSuppr")]
    //public GameObject BazarSalle2;
    //public GameObject BazarSalle3;
    public GameObject[] BazarSalles;
    public List<GameObject>[] stockLineRenderer;



    [Header("CoefPourChaqueTableau")]
    //1 = SALLE CIBLÉ
    //2 = Tableau 1
    //3 = TAbleau 2
    public float[][,] coefPerTableauSalles;


    private void Awake()
    {

        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    void Start()
    {
        GenMurRef = GenerationMur._instance;
        CoefRef = CoefManager._instance;
        UiDebugRef = UIDebug._instance;

       

        ListTabSalles = new List<Proto_Tableau_Scriptable>[NbrSalle];
        ListTabSalles_GO = new List<GameObject>[NbrSalle];
        ReputationSalles = new float[NbrSalle];
        NbrTableauParSalle = new int[NbrSalle];
        stockLineRenderer = new List<GameObject>[NbrSalle];
        coefPerTableauSalles = new float[NbrSalle][,];

        for (int i = 0; i < ListTabSalles.Length; i++)
        {
            ListTabSalles[i] = new List<Proto_Tableau_Scriptable>();
        }

        for (int i = 0; i < ListTabSalles_GO.Length; i++)
        {
            ListTabSalles_GO[i] = new List<GameObject>();
        }

        for (int i = 0; i < stockLineRenderer.Length; i++)
        {
            stockLineRenderer[i] = new List<GameObject>();
        }

    }



    public void CalculateSalleCoef(int idSalle)
    {
        coefPerTableauSalles[idSalle] = new float[ListTabSalles[idSalle].Count, ListTabSalles[idSalle].Count];
        ReputationSalles[idSalle] = 0;
        GetCoefMouvement(ListTabSalles[idSalle], idSalle);
        GetCoefPeriode(ListTabSalles[idSalle], idSalle);
        GetCoefStyle(ListTabSalles[idSalle], idSalle);
        //GetCoefAmbiance(ListTabSalles[idSalle], idSalle);
        GetCoefElements(ListTabSalles[idSalle], idSalle);
        CalculateTableauxReputation(ListTabSalles[idSalle], idSalle);
        CheckCoefPerTableau(idSalle);
        DeleteLine(idSalle);
        DrawLine(ListTabSalles_GO[idSalle], coefPerTableauSalles[idSalle], idSalle);


        //UiDebugRef.ChangeTextReputSalle(ReputationSalles[idSalle], idSalle);
        //UiDebugRef.ChangeTextReputMusee(ReputationAllSalle);

    }


    private void DrawLine(List<GameObject> listToUse, float[,] coef, int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            tabCheck1 = listToUse[i];
            for (int j = 0; j < listToUse.Count; j++)
            {
                if (i != j)
                {
                    tabCheck2 = listToUse[j];
                    var coefperso = coef[i, j];
                    var line = Proto_Line._instance.CreateLine(tabCheck1.transform.position, tabCheck2.transform.position,tabCheck1 ,coefperso);
                    stockLineRenderer[idSalle].Add(line);
                }
            }
        }
    }
    private void DeleteLine(int idSalle)
    {
        if(stockLineRenderer[idSalle].Count > 0)
        {
            for (int i = 0; i < stockLineRenderer[idSalle].Count; i++)
            {
                foreach (var item in stockLineRenderer[idSalle])
                {
                    Destroy(item);
                }
            }
        }  
    }
    private void GetCoefMouvement(List<Proto_Tableau_Scriptable> listToUse, int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            ElementCheck1 = (int)listToUse[i].courantArtTableau;

            if (listToUse.Count > 1)
            {
                for (int j = 0; j < listToUse.Count; j++)
                {
                    if (i != j)
                    {
                        iteration++;
                        ElementCheck2 = (int)listToUse[j].courantArtTableau;

                        Coef += CoefRef.MouvementCoef[ElementCheck1, ElementCheck2];
                        coefPerTableauSalles[idSalle][i, j] += CoefRef.MouvementCoef[ElementCheck1, ElementCheck2];

                    }
                }
            }
            else
            {
                //if(ListTabSalle1[i].GetComponent<Tableau>()) check si le tableau est important (si oui pas de baisse de populariter sinon baisse de populariter)
                listToUse[i].MouvementCoef = defaultCoef;
            }

            if (iteration > 0)
            {
                var result = Coef / iteration;
                listToUse[i].MouvementCoef = result;
            }

            Coef = 0;
            iteration = 0;
        }
    }

    private void GetCoefPeriode(List<Proto_Tableau_Scriptable> listToUse, int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            ElementCheck1 = (int)listToUse[i].periode;

            if (listToUse.Count > 1)
            {
                for (int j = 0; j < listToUse.Count; j++)
                {
                    if (i != j)
                    {
                        iteration++;
                        ElementCheck2 = (int)listToUse[j].periode;

                        Coef += CoefRef.PeriodeCoef[ElementCheck1, ElementCheck2];
                        coefPerTableauSalles[idSalle][i, j] += CoefRef.PeriodeCoef[ElementCheck1, ElementCheck2];

                    }
                }
            }
            else
            {
                //if(ListTabSalle1[i].GetComponent<Tableau>()) check si le tableau est important (si oui pas de baisse de populariter sinon baisse de populariter)
                listToUse[i].PeriodCoef = defaultCoef;
            }

            if (iteration > 0)
            {
                var result = Coef / iteration;
                listToUse[i].PeriodCoef = result;
            }

            Coef = 0;
            iteration = 0;
        }
    }

    private void GetCoefTaille(List<Proto_Tableau_Scriptable> listToUse, int idSalle)
    {

    }

    private void GetCoefStyle(List<Proto_Tableau_Scriptable> listToUse, int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            ElementCheck1 = (int)listToUse[i].style;

            if (listToUse.Count > 1)
            {
                for (int j = 0; j < listToUse.Count; j++)
                {
                    if (i != j)
                    {
                        iteration++;
                        ElementCheck2 = (int)listToUse[j].style;
                        //print($"Elem1 : {ElementCheck1} & Elem2 : {ElementCheck2}");
                        Coef += CoefRef.StyleCoef[ElementCheck1, ElementCheck2];
                        coefPerTableauSalles[idSalle][i, j] += CoefRef.StyleCoef[ElementCheck1, ElementCheck2];

                    }
                }
            }
            else
            {
                //if(ListTabSalle1[i].GetComponent<Tableau>()) check si le tableau est important (si oui pas de baisse de populariter sinon baisse de populariter)
                listToUse[i].StyleCoef = defaultCoef;
            }

            if (iteration > 0)
            {
                var result = Coef / iteration;
                listToUse[i].StyleCoef = result;
            }

            Coef = 0;
            iteration = 0;
        }
    }

    private void GetCoefAmbiance(List<Proto_Tableau_Scriptable> listToUse,int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            ElementCheck1 = (int)listToUse[i].ambiance;

            if (listToUse.Count > 1)
            {
                for (int j = 0; j < listToUse.Count; j++)
                {
                    if (i != j)
                    {
                        iteration++;
                        ElementCheck2 = (int)listToUse[j].ambiance;

                        Coef += CoefRef.AmbianceCoef[ElementCheck1, ElementCheck2];




                        coefPerTableauSalles[idSalle][i, j] += CoefRef.AmbianceCoef[ElementCheck1, ElementCheck2];

                    }
                }
            }
            else
            {
                //if(ListTabSalle1[i].GetComponent<Tableau>()) check si le tableau est important (si oui pas de baisse de populariter sinon baisse de populariter)
                listToUse[i].AmbianceCoef = defaultCoef;
            }

            if (iteration > 0)
            {
                var result = Coef / iteration;
                listToUse[i].AmbianceCoef = result;
            }

            Coef = 0;
            iteration = 0;
        }
    }

    private void GetCoefElements(List<Proto_Tableau_Scriptable> listToUse, int idSalle)
    {
        for (int i = 0; i < listToUse.Count; i++)
        {
            ElementCheck1 = (int)listToUse[i].elements;

            if (listToUse.Count > 1)
            {
                for (int j = 0; j < listToUse.Count; j++)
                {
                    if (i != j)
                    {
                        iteration++;
                        ElementCheck2 = (int)listToUse[j].elements;

                        Coef += CoefRef.ElementsCoef[ElementCheck1, ElementCheck2];
                        coefPerTableauSalles[idSalle][i, j] += CoefRef.ElementsCoef[ElementCheck1, ElementCheck2];

                    }
                }
            }
            else
            {
                //if(ListTabSalle1[i].GetComponent<Tableau>()) check si le tableau est important (si oui pas de baisse de populariter sinon baisse de populariter)
                listToUse[i].ElementsCoef = defaultCoef;
            }

            if (iteration > 0)
            {
                var result = Coef / iteration;
                listToUse[i].ElementsCoef = result;
            }

            Coef = 0;
            iteration = 0;
        }
    }



    public void CalculateTableauxReputation(List<Proto_Tableau_Scriptable> listToUse, int idSalle)
    {
        
        for (int i = 0; i < listToUse.Count; i++)
        {
            var tableau = listToUse[i];
            tableau.CalculateReputationUpdate();
            ReputationSalles[idSalle] += tableau.TransformedValue;

        }
        CalculateRepAllSalle();
    }

    public void CalculateRepAllSalle()
    {
        ReputationAllSalle = 0;
        ReputationAllSalle = AddAllRepValue();
    }

    private float AddAllRepValue()
    {
        float value = 0;
        for (int i = 0; i < ReputationSalles.Length; i++)
        {
            value += ReputationSalles[i];
        }

        return value;
    }

    public void UnlockSalle()
    {

        var refBut = EventSystem.current.currentSelectedGameObject;
        var refButName = refBut.name;

        switch (refButName)
        {
            case "UnlockSalle1":
                refBut.SetActive(false);
                GenMurRef.canConstructSalle[1] = true;
                break;
            case "UnlockSalle2":
                refBut.SetActive(false);
                GenMurRef.canConstructSalle[2] = true;
                break;
            case "UnlockSalle3":
                refBut.SetActive(false);
                GenMurRef.canConstructSalle[3] = true;
                break;
        }
    }


    public void UnlockSalleReput()
    {


        for(int i=0; i< VALEURAMODIFIER._instance.reputForSalles.Length; i++)
        {
            if(i > 1 && GenMurRef.canConstructSalle[i] != true)
            {
                if(GameLibrary._instance.reputation >= VALEURAMODIFIER._instance.reputForSalles[i])
                {
                    GenMurRef.canConstructSalle[i] = true;
                    BazarSalles[i].SetActive(false);
                }
            }
        }
    }

    public void CheckCoefPerTableau(int idSalle)
    {

        for (int i = 0; i < coefPerTableauSalles[idSalle].GetLength(0); i++)
        {
            for (int j = 0; j < coefPerTableauSalles[idSalle].GetLength(0); j++)
            {
                if (i != j) coefPerTableauSalles[idSalle][i, j] = coefPerTableauSalles[idSalle][i, j] / 4;
            }
        }

        
    }
}

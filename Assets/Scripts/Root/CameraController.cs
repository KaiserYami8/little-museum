﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    public Transform mainCamera;

    public float maxCamSpeed = 30;

    public float speedH = 2.0f;
    public float speedV = 2.0f;

    public float sensibility;

    public float lerp = .5f;

    public Material Cutout;
    public Material Default;
    public float var;
    public LayerMask layerMask;

    public GameObject[] floors;
    public GameObject[] floorPans;

    public GameObject center;

    private Controls controls = null;

    Vector2 posVelocity;
    Vector2 rotVelocity;

    float rotX;
    float rotY;

    public float radius;
    public Vector3 offset;
    public float maxZoomDistance;

    public Vector3 posStart;

    private void Awake()
    {
        controls = new Controls();

        rotX = transform.eulerAngles.x;
        rotY = transform.eulerAngles.y;
    }

    private void OnEnable()
    {
        controls.Camera.Enable();
    }

    private void OnDisable()
    {
        controls.Camera.Disable();
    }

    void Start()
    {
        posStart = this.transform.position;
    }
    void Update()
    {

        //MOVEMENT
        var movementInput = controls.Camera.Movement.ReadValue<Vector2>();

        if (Mathf.Abs(movementInput.x) > .1f)
            posVelocity = new Vector2(movementInput.x, posVelocity.y);
        if (Mathf.Abs(movementInput.y) > .1f)
            posVelocity = new Vector2(posVelocity.x, movementInput.y);


        //MOVEMENT MOUSE
        if (Input.GetMouseButton(2))
        {
            // Check if the mouse was clicked over a UI element
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                posVelocity = new Vector2(-Input.GetAxis("Mouse X"), -Input.GetAxis("Mouse Y"));
            }

        }

        //CALCULATION MOVEMENT
        posVelocity = Vector2.Lerp(posVelocity, Vector2.zero, .1f);

        var xRot = transform.eulerAngles.x;

        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

        transform.Translate(new Vector3(posVelocity.x, 0f, posVelocity.y) * Time.deltaTime * sensibility, Space.Self);

        transform.eulerAngles = new Vector3(xRot, transform.eulerAngles.y, 0);


        if (Vector3.Distance(transform.position, offset) > radius)
        {
            Vector3 v = transform.position - offset;
            transform.position = v.normalized * radius + offset;
        }

        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        //ROTATION
        var rotationInput = controls.Camera.Rotation.ReadValue<Vector2>();

        if (Mathf.Abs(rotationInput.x) > .1f)
            rotVelocity = new Vector2(rotVelocity.x, rotationInput.x);
        if (Mathf.Abs(rotationInput.y) > .1f)
            rotVelocity = new Vector2(rotationInput.y, rotVelocity.y);

        //ROTATION MOUSE
        if (Input.GetMouseButton(1))
        {
            // Check if the mouse was clicked over a UI element
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                rotVelocity = new Vector2(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"));
            }
            
        }

        //CALCULATION ROTATION
        rotVelocity = Vector2.Lerp(rotVelocity, Vector2.zero, .1f);

        if (rotX > 180)
            rotX -= 360;

        rotX += speedV * -rotVelocity.x;
        rotX = Mathf.Clamp(rotX, -60, 20);

        rotY += speedH * rotVelocity.y;

        transform.eulerAngles = new Vector3(rotX, rotY, transform.eulerAngles.z);


        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        //RESET
        if (Input.GetKey(KeyCode.C))
            transform.position = posStart;

        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        //ZOOM

        // Check if the mouse was clicked over a UI element
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            float zoom = Input.GetAxis("Mouse ScrollWheel");

            if (Vector3.Distance(transform.position, mainCamera.position) > maxZoomDistance && zoom < 0)
            {
                zoom = 0;
            }

            mainCamera.Translate(new Vector3(0, 0, zoom * Mathf.Abs(mainCamera.localPosition.z)), Space.Self);
        }
        

        mainCamera.localPosition = new Vector3(mainCamera.localPosition.x, Mathf.Clamp(mainCamera.localPosition.y, 0, 2000), mainCamera.localPosition.z);

        mainCamera.LookAt(transform.position);

        //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        //ShaderWall();
    }

    void ShaderWall()
    {
        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(mainCamera.position, mainCamera.TransformDirection(Vector3.forward), out hit, Mathf.Infinity, layerMask))
        {
            //var = Mathf.Lerp(var, Vector3.Distance(mainCamera.position, hit.point) + 3, .4f);
            Cutout.SetFloat("_Distance", var);

            Debug.DrawRay(mainCamera.position, mainCamera.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
        }
        else
        {
            Debug.DrawRay(mainCamera.position, mainCamera.TransformDirection(Vector3.forward) * 1000, Color.white);
        }

        if (Input.GetKey(KeyCode.Keypad1))
        {
            floors[1].SetActive(false);
            floors[2].SetActive(false);
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }
        if (Input.GetKey(KeyCode.Keypad2))
        {
            floors[1].SetActive(true);
            floors[2].SetActive(false);
            transform.position = new Vector3(transform.position.x, 2, transform.position.z);
        }
        if (Input.GetKey(KeyCode.Keypad3))
        {
            floors[1].SetActive(true);
            floors[2].SetActive(true);
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
        }

        Vector3 pos = Vector3.zero;
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Wall"))
        {
            pos += go.transform.position;
        }
        if (GameObject.FindGameObjectsWithTag("Wall").Length > 0)
            center.transform.position = pos / GameObject.FindGameObjectsWithTag("Wall").Length;
        else
            center.transform.position = Vector3.zero;

        float y = 0;

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Wall"))
        {
            if (go.GetComponent<MeshRenderer>() != null)
            {
                y = 3 - Mathf.Clamp(
                    Vector3.Distance(mainCamera.position, center.transform.position) - Vector3.Distance(mainCamera.position, go.transform.position),
                    0, 3
                    );
                go.GetComponent<MeshRenderer>().material.SetVector("_SectionPoint", new Vector3(0, y, 0));
            }
        }

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("tableau"))
        {
            if (go.GetComponent<MeshRenderer>() != null)
            {
                y = 3 - Mathf.Clamp(
                    Vector3.Distance(mainCamera.position, center.transform.position) - Vector3.Distance(mainCamera.position, go.transform.position),
                    0, 3
                    );
                go.GetComponent<MeshRenderer>().material.SetVector("_SectionPoint", new Vector3(0, y, 0));
            }
        }
    }
}
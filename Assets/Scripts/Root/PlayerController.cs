﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public FloatVariable playerHealth;

    public GameEvent call;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            playerHealth.RuntimeValue--;
        }
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            playerHealth.RuntimeValue++;
            GameManager.Instance.SayHi();
        }

        playerHealth.RuntimeValue = Mathf.Clamp(playerHealth.RuntimeValue, 0, 10);

        if (playerHealth.RuntimeValue <= 0)
            call.Raise();
    }
}

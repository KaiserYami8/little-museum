﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDebug : MonoBehaviour
{
    public void Log(string text)
    {
        Debug.Log(text);
    }
    public void Warning(string text)
    {
        Debug.LogWarning(text);
    }
    public void Error(string text)
    {
        Debug.LogError(text);
    }
}
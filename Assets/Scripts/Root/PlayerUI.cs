﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public FloatVariable playerHealth;
    public Image gauge;

    private void Update()
    {
        float health = playerHealth.RuntimeValue;

        gauge.fillAmount = health / 10;
        gauge.color = new Color((Mathf.Clamp(health / 10, .5f, 1) - 1) * -2, (Mathf.Clamp(health / 10, 0, .5f)) * 2, 0);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CameraGyzmo : MonoBehaviour
{
    CameraController cc;

    private void Start()
    {
        cc = GetComponent<CameraController>();
    }

    void Update()
    {
        cc.mainCamera.LookAt(transform.position);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(cc.offset, cc.radius);
    }
}
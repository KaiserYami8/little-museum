﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class RewardBut : MonoBehaviour
{
    [Header("Reférences")]
    public GameObject Preview_Tableau;
    public Image imageTableau;
    //public Text DescriptionTableau;
    public Text TitreTableau;
    public Button refButton;
    public Image background_Image;
    public GameObject ImgSelectedTab;

    public Proto_Tableau_Scriptable refTableau;
    private QuestManager refQuestManager;

    public bool IsSelected;

    bool seeInfo;

    //SPAWN PANEL INFO
    public GameObject prefabFlyInfo;
    GameObject actualInfo;
    GameObject Canvas;
    Vector3 positionCursor;


    void Start()
    {
        refQuestManager = QuestManager._instance;
        //DescriptionTableau.text = $"Courant : {refTableau.courantArtTableau}\n\nPeriode : {refTableau.periode}\n\nStyle : {refTableau.style}\n\nElement : {refTableau.elements}";
        TitreTableau.text = $"{refTableau.titre}";
        imageTableau.sprite = refTableau.imageOeuvre;
        Canvas = GameObject.FindGameObjectWithTag("canvas");
        ImgSelectedTab.SetActive(false);
    }

    
    void Update()
    {
        if (seeInfo)
        {
            positionCursor = Input.mousePosition;
            actualInfo.transform.position =new Vector3(positionCursor.x + 1, positionCursor.y + 1 ,0);
        }
        
    }

    public void GetHitted()
    {
        //print("hitted");
        IsSelected = IsSelected ? false : true;
        if (IsSelected)
        {
            refQuestManager.CheckSelectedButtons(gameObject);
            ImgSelectedTab.SetActive(true);
        }
        else 
        { 
            refQuestManager.DeselectButton(gameObject); 
            ImgSelectedTab.SetActive(false); 
        }

        }

    public void Deselected()
    {
        IsSelected = false;
        background_Image.color = refButton.colors.normalColor;
        ImgSelectedTab.SetActive(false);

    }

    public void isMouseOver()
    {
        background_Image.color = refButton.colors.pressedColor;
        //FondTitreTableau.color = ButHover;
        //Preview_Tableau.SetActive(false);
        seeInfo = true;
        actualInfo = Instantiate(prefabFlyInfo,Canvas.transform);
        actualInfo.layer = 2;
        actualInfo.GetComponent<FlyInfoPreview>().AfficherInfo(refTableau);
        
    }

    public void isMouseExit()
    {
        background_Image.color = IsSelected ? refButton.colors.highlightedColor : refButton.colors.normalColor;
        //FondTitreTableau.color = ButNoHover;
        Preview_Tableau.SetActive(true);
        seeInfo = false;
        Destroy(actualInfo);
    }
}

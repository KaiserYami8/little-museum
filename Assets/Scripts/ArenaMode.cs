﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaMode : MonoBehaviour
{

    public static ArenaMode _instance;
    GameLibrary GL;
    public GameObject tableauArena;
    public float reputationForUp;
    float temp;
    float lasttemp;


    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }
    private void Start()
    {
        

        GL = GameLibrary._instance;
        temp = GL.reputation;
    }

    void Update()
    {
        temp = GL.reputation;
        if (temp >= lasttemp + reputationForUp)
        {
            tableauArena.SetActive(true);
            UIController._instance.CanModifiateTerrain = false;
            lasttemp = GL.reputation;
        }
    }



}

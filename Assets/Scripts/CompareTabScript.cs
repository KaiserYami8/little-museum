﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CompareTabScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject refComparateur;
    private Proto_Tableau_Scriptable _firstTab;
    public Proto_Tableau_Scriptable FirstTab
    {
        get => _firstTab;
        set
        {
           if (_firstTab == value) return;

            _firstTab = value;
            UpdateTableauPrincipal();
  
        }
    }
    private Proto_Tableau_Scriptable _secondTab;
    public Proto_Tableau_Scriptable SecondTab
    {
        get => _secondTab;
        set
        {
            if (_secondTab == value) return;

            _secondTab = value;
            UpdateTableauSecondaire();
        }
    }

    [Header("Tableau Gauche")]
    public Image refImageLeft; 
    public Text refTitreTabLeft; 
    public Text refArtisteLeft; 
    public Text refDateLeft; 
    public Text refMouvementLeft; 
    public Text refElemLeft; 
    public Text refStyleLeft;

    [Header("Tableau Droite")]
    public Image refImageRight;
    public Text refTitreTabRight;
    public Text refArtisteRight;
    public Text refDateRight;
    public Text refMouvementRight;
    public Text refElemRight;
    public Text refStyleRight;

    UIController refUIControl;

    void Start()
    {
        refUIControl = UIController._instance;
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    public void UpdateTableauPrincipal()
    {
        refImageLeft.sprite = FirstTab.imageOeuvre;
        refTitreTabLeft.text = FirstTab.titre;
        refArtisteLeft.text = FirstTab.auteur;
        refDateLeft.text = FirstTab.Annee;
        refMouvementLeft.text = FirstTab.courantArtTableau.ToString();
        refElemLeft.text = FirstTab.elements.ToString();
        refStyleLeft.text = FirstTab.style.ToString();
    }

    public void UpdateTableauSecondaire()
    {
        refImageRight.sprite = SecondTab.imageOeuvre;
        refTitreTabRight.text = SecondTab.titre;
        refArtisteRight.text = SecondTab.auteur;
        refDateRight.text = SecondTab.Annee;
        refMouvementRight.text = SecondTab.courantArtTableau.ToString();
        refElemRight.text = SecondTab.elements.ToString();
        refStyleRight.text = SecondTab.style.ToString();
    }

    public void QuitterCompareMenu()
    {
        refComparateur.SetActive(false);
        refUIControl.CanCompareTab = false;
        //Blyat Tanguy
    }
}

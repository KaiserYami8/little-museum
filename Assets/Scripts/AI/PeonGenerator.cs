﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeonGenerator : MonoBehaviour
{
    // déclaration des listes de parties du corps, mettre les gameobjects dedans via l'inspecteur

    // neutre (tout)
    public List<GameObject> L_legs;
    public List<GameObject> L_torso;
    public List<GameObject> L_nose;
    public List<GameObject> L_hat;
    public List<GameObject> L_hair;
    public List<GameObject> L_ears;
    public List<GameObject> L_shoes;
    public List<GameObject> L_Accesoires;

    // femme
    public List<GameObject> L_Female_torso;
    public List<GameObject> L_Female_hair;
    public List<GameObject> L_Female_ears;
    public List<GameObject> L_Female_shoes;

    // homme
    public List<GameObject> L_male_torso;
    public List<GameObject> L_male_hair;
    public List<GameObject> L_male_ears;
    public List<GameObject> L_male_shoes;

    // Liste des objets séléctionnés par le Constructeur

    public List<GameObject> L_constructorSelection;

    public GameObject HatExeption;

    public int GenderValue; // définis le genre de la personne, 0 = femme, 1 = homme, 2 = neutre, a relier avec taguy

    // Start is called before the first frame update
    void Start()
    {

        BodyConstructor(); // appel de la construction
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(0, 25 * Time.deltaTime, 0); // p'tite rota sisi
    }

    public void BodyConstructor() // Construire un corp 
    {
        GenderValue = Random.Range(0, 3); // pick du genre

        if (GenderValue == 2) // si le corps est de type neutre
        {
            print("Gender Value = Neutral");
            // pick torso
            L_constructorSelection.Add(L_torso[Random.Range(0, L_torso.Count)]);
            // pick hair
            L_constructorSelection.Add(L_hair[Random.Range(0, L_hair.Count)]);
            // pick ears
            L_constructorSelection.Add(L_ears[Random.Range(0, L_ears.Count)]);
            // pick shoes
            L_constructorSelection.Add(L_shoes[Random.Range(0, L_shoes.Count)]);
          
        }


        if (GenderValue == 1) // pick des élléments dans la liste des objets homme
        {
            print("Gender Value = Male");
            // pick torso
            L_constructorSelection.Add(L_male_torso[Random.Range(0, L_male_torso.Count)]);
            // pick hair
            L_constructorSelection.Add(L_male_hair[Random.Range(0, L_male_hair.Count)]);
            // pick ears
            L_constructorSelection.Add(L_male_ears[Random.Range(0, L_male_ears.Count)]);
            // pick shoes
            L_constructorSelection.Add(L_male_shoes[Random.Range(0, L_male_shoes.Count)]);
        }

        if (GenderValue == 0)
        {
            print("Gender Value = Female");
           
            // pick torso
            L_constructorSelection.Add(L_Female_torso[Random.Range(0, L_Female_torso.Count)]);            
            // pick hair
            L_constructorSelection.Add(L_Female_hair[Random.Range(0, L_Female_hair.Count)]);
            // pick ears
            L_constructorSelection.Add(L_Female_ears[Random.Range(0, L_Female_ears.Count)]);
            // pick shoes
            L_constructorSelection.Add(L_Female_shoes[Random.Range(0, L_Female_shoes.Count)]);
        }

        // PICK HAT
        if (Random.Range(0, 2) == 1 && L_constructorSelection.Contains(HatExeption) == false) // a-t-il un chapeau ? sa coupe est elle compatible ?
        {
            L_constructorSelection.Add(L_hat[Random.Range(0, L_hat.Count)]);
        }

        // pick legs
        L_constructorSelection.Add(L_legs[Random.Range(0, L_legs.Count)]);

        // pick nose
        L_constructorSelection.Add(L_nose[Random.Range(0, L_nose.Count)]);

        // pick accesoires
        if (Random.Range(0, 3) == 1) // a-t-il un acessoire ?
        {
            L_constructorSelection.Add(L_Accesoires[Random.Range(0, L_Accesoires.Count)]);
        }


        foreach (GameObject go in L_constructorSelection) // appliquer les choix du constructeur
        {
            go.SetActive(true);
        }

    }

    public void CleanBodyConstructor() // Clean la liste du constructeur 
    {
       foreach(GameObject go in L_constructorSelection)
        {
            go.SetActive(false);
            
        }

        L_constructorSelection.Clear();

    }

    public void RefreshBodyConstructor() // refresh
    {
        CleanBodyConstructor();
        BodyConstructor();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnerAI : MonoBehaviour
{
    public bool autoSpawn;
    public GameObject AI;
    GameLibrary refGL;
    PeonManager refPeonM;
    public float timerSpawn;
    // Start is called before the first frame update
    void Start()
    {
        refPeonM = PeonManager._instance;
        refGL = GameLibrary._instance;
        if(autoSpawn)
        InvokeRepeating("SpawnAI", 0, timerSpawn);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            SpawnAI();
    }

    // Update is called once per fra
    void SpawnAI()
    {
        var x = Random.Range(0, refGL.enter.Count);
        Instantiate(AI, refGL.enter[x].transform.position, refGL.enter[x].transform.rotation);
        
    }
}

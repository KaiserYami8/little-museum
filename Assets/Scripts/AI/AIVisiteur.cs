﻿using DG.Tweening.Plugins.Core.PathCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class AIVisiteur : MonoBehaviour
{

    // Reference Script
    SalleV2 refSalleV2;
    GameLibrary refGL;
    NavMeshAgent agent;
    private Animator anim;
    PeonManager refPeonM;

    // TableauACheck
    public int currentTabMvt;
    public float tauxSourire;

    // Class Visiteur
    private ClassVisiteur visiteur;

    // Nom Prenom Mouvement Art Pref
    [Header("Carte d'Identité")]
    public string firstname;
    public string lastname;
    public string sexe;
    public string courantArtPref;

    // Son Corps
    [Header("SonCorps")]
    public List<GameObject> l_SkinSelection;
    public List<GameObject> l_SkinGlobal;

    // Destination
    [Header("Destination")]
    public List<GameObject> destination;
    public int minTimeToSeeTableau;
    public int maxTimeToSeeTableau;
    public List<GameObject> xList;
    private int nbTableauView;
    private bool oneTap;

    public bool arriver;
    public bool collidTableau;
    public bool arriverToCheckPoint;
    public bool isBuying;
    public bool goToMusee;

    private bool _isInMusee;
    public bool isInMusee
    {

        get => _isInMusee;
        set
        {
            if (value == _isInMusee) return;

            _isInMusee = value;

            if (_isInMusee) refPeonM.MuseumVisiteursList.Add(gameObject);
            else refPeonM.MuseumVisiteursList.Remove(gameObject);
        }
    }
    

    NavMeshPath path;

    private bool checkIfMuseeHaveTab() 
    {
        var isTab = false;
        var refTabs = refSalleV2.ListTabSalles_GO;
        foreach (var item in refTabs)
        {
            //print(item.Count);
            if (item.Count > 0)
            {
                isTab = true;
                break;
            }
        }
        return isTab;
    }
    public bool wantVisite()
    {
        if (checkIfMuseeHaveTab())
        {
            var x = Random.Range(0, 2);
            if (x == 0)
            {
                goToMusee = true;
                return true;
            }
            else
            {
                goToMusee = false;
                return false;
            }
        }
        else
        {
            goToMusee = false;
            return false;
        }
        
    }


    // Start is called before the first frame update
    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        refSalleV2 = SalleV2._instance;
        refGL = GameLibrary._instance;
        refPeonM = PeonManager._instance;
        anim = GetComponent<Animator>();

        //Generation Carte Identité
        visiteur = new ClassVisiteur();

        courantArtPref = visiteur.GetCourantArtPref().ToString();
        sexe = visiteur.GetSexe().ToString();
        lastname = visiteur.GetNames().Lastname;
        firstname = visiteur.GetNames().Firstname;

        //Get son skin
        ConstructionSkin();

        //SI LE VISITEUR VEUT VISITER
        if (wantVisite())
        {
            destination.Add(refPeonM.CheckPoint.gameObject);
            
        }
        //SI LE VISITEUR NE VEUT PAS VISITER
        else
        {
            DirectionExit();
        }

        //Generation de sa liste de deplacement
        //GenerateList();


        refPeonM.GlobalVisiteursList.Add(gameObject);
        path = new NavMeshPath();
    }



    void CheckAttente()
    {
        //SI LA LISTE D'ATTENTE N'EST PAS PLEINE
        if (refPeonM.AttenteVisiteursList[refPeonM.AttenteVisiteursList.Length - 1] == null)
        {
            for (int i = 0; i < refPeonM.AttenteVisiteursList.Length; i++)
            {
                if (refPeonM.AttenteVisiteursList[i] == null)
                {
                    destination.Add(refPeonM.AttenteVisiteursPoint[i].gameObject);
                    refPeonM.AttenteVisiteursList[i] = gameObject;
                    break;
                }
            }

        }
        //SI LA LISTE D'ATTENTE EST PLEINE
        else
        {
            DirectionExit();
        }

        destination.RemoveAt(0);
        //agent.ResetPath();
        //print("delete checkattente");
        arriver = false;
        oneTap = false;
        agent.destination = destination[0].transform.position;
    }


    // Update is called once per frame
    private void Update()
    {
        if (isBuying)
        {
            if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            {
                //print("gogo peon");
                isBuying = false;
                isInMusee = true;
                goToMusee = false;
                GenerateList();
                
                refPeonM.MovePeonInFile();
            }
        }

        if (destination.Count == 1 && destination[0] != refPeonM.CheckPoint.gameObject)
        {
            isInMusee = false;
        }


        if (!arriver)
        {
            if (!agent.isStopped)
            {
                    MoveAi();
                    anim.SetBool("isMoving", true);
                    arriver = true;                  
            }
        }


        // Check if we've reached the destination
        if (!agent.pathPending)
        {
            if (agent.CalculatePath(destination[0].transform.position, path) == true)
            {
                if (agent.remainingDistance <= 0 || collidTableau)
                {
                    if (!oneTap)
                    {
                        if (destination.Count >= 1)
                        {
                            if (!arriverToCheckPoint)
                            {
                                //print("arrive to checkpoint");
                                arriverToCheckPoint = true;
                                CheckAttente();
                            }
                            else
                            {
                                if (isInMusee)
                                {
                                    //print("arriver");
                                    if (destination[0].transform.parent.transform.gameObject.tag == "tableau") CheckTableauSourire(destination[0].transform.parent.transform.gameObject);
                                    
                                    anim.SetBool("isMoving", false);
                                    oneTap = true;

                                }
                                else
                                {
                                    anim.SetBool("isMoving", false);

                                    if (refPeonM.AttenteVisiteursList[0] == gameObject)
                                    {
                                        if (refPeonM.MuseumVisiteursList.Count < refPeonM.MaxPeonMusee)
                                        {
                                            if (!isBuying)
                                            {
                                                anim.SetTrigger("isHappy");
                                                isBuying = true;
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }




    }
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == destination[0] && other.tag == "view_tableau")
        {
            collidTableau = true;
            //print("hitableau");
        }

        if (other.gameObject == destination[0] && other.tag == "exit")
        {
            ExitMusee();
            
            //print("hitexit");
        }
    }

    
    private void MoveAi()
    {
        if(destination[0] != null)
        {
            if (agent.CalculatePath(destination[0].transform.position, path))
            {
                agent.destination = destination[0].transform.position;
            }
            //else
            //{
            //    destination.RemoveAt(0);
            //    print("un vieux text");
            //}
        }
        //else
        //{
        //    destination.RemoveAt(0);
        //    print("un text jeune");
        //}
    }


    

    private void GenerateList()
    {
        //print($"destination count : {destination.Count}");
        //print("Start generateList");
        nbTableauView = Random.Range(1, GameLibrary._instance.tableau.Count);
        destination = new List<GameObject>();
        //print($"nbTabView : {nbTableauView}");
        xList = new List<GameObject>();
        for (int i = 0; i < GameLibrary._instance.tableau.Count; i++)
        {
            xList.Add(GameLibrary._instance.tableau[i]);
        }

        for (int i = 0; i < nbTableauView; i++)
        {
            var x = Random.Range(0,xList.Count);
            //print($"destinationTaille1 : {destination.Count}");

            destination.Add(xList[x].transform.GetChild(0).gameObject);
            //print($"destinationTaille2 : {destination.Count}");


            xList.RemoveAt(x);

        }
        //print($"destinationTailleFin1 : {destination.Count}");
        DirectionExit();
        //print($"destinationTailleFin2 : {destination.Count}");
        //print("end generateList");

        if (destination.Count == 0) DirectionExit();
        agent.destination = destination[0].transform.position;
        arriver = false;
        oneTap = false;

        //Invoke("UpdatePeon", 0.1f);
        
    }

    private void UpdatePeon()
    {
        print($"delete update peon");
        //destination.RemoveAt(0);
        //agent.ResetPath();

        //if (destination.Count == 0) DirectionExit();
        //agent.destination = destination[0].transform.position;
        //arriver = false;
        //oneTap = false;
        
    }

    void DirectionExit()
    {
        var y = Random.Range(0, refGL.exit.Count);
        destination.Add(refGL.exit[y]);
    }

    private void ExitMusee()
    {
        refPeonM.GlobalVisiteursList.Remove(gameObject);
        Destroy(gameObject);
    }

    void CheckTableauSourire(GameObject Tableau)
    {
        var refTableau = Tableau.GetComponent<DisplayTableau>().Tableau;
        var idSalle = refTableau.idSalle;
        var refCourrant = CheckValueTableau(refTableau);

        var sameCourant = refSalleV2.nbTableauParSalleParMouvement[idSalle][refCourrant];
        var nbTableauSalle = refSalleV2.ListTabSalles_GO[idSalle].Count;
        print(nbTableauSalle);
        tauxSourire = (sameCourant / nbTableauSalle) * 100;

        StartCoroutine(SeeTableau(tauxSourire));

        print("checktableau sourire");

        
    }


    int CheckValueTableau(Proto_Tableau_Scriptable refTableau)
    {
        switch (refTableau.courantArtTableau)
        {
            case GameLibrary.CourantArtistique.Manierisme:
                return 0;
            case GameLibrary.CourantArtistique.Impressionnisme:
                return 1;
            case GameLibrary.CourantArtistique.Cubisme:
                return 2;
            case GameLibrary.CourantArtistique.Surrealisme:
                return 3;
            case GameLibrary.CourantArtistique.Artnouveau:
                return 4;
        }
        return -1;
    }



    private IEnumerator SeeTableau(float tauxSourrire)
    {
        anim.SetBool("isMoving", false);

        this.gameObject.transform.LookAt(destination[0].transform.parent);
        var x = Random.Range(minTimeToSeeTableau, maxTimeToSeeTableau);

        if(tauxSourire <= 25)
        {
            anim.SetTrigger("isSad");
        }
        else if(tauxSourire <= 50)
        {
            anim.SetTrigger("isInteresting");
        }
        else if (tauxSourire <= 75)
        {
            anim.SetTrigger("isClapClap");
        }
        else
        {
            anim.SetTrigger("isHappy");
        }

        //print($"value attente : {x}");
        yield return new WaitForSeconds(x);
        //print($"stop attente");

        destination.RemoveAt(0);
        //agent.ResetPath();
        //print("delete see tableau");
        arriver = false;
        
        agent.destination = destination[0].transform.position;
        oneTap = false;
        collidTableau = false;

        yield break;
        
    }

    private void OnTriggerStay(Collider other)
    {
        if (!arriverToCheckPoint && goToMusee)
        {
            //print(other.name);
            if(other.name == "CheckPoint")
            {
                arriverToCheckPoint = true;
                CheckAttente();
            }
        }
    }



    //GESTION DU SKIN DU PEON

    //AFFICHAGE DU SKIN
    void AfficherSkin()
    {
        foreach (var item in l_SkinSelection)
        {
            for (int i = 0; i < l_SkinGlobal.Count; i++)
            {
                if (item.name == l_SkinGlobal[i].name)
                {
                    l_SkinGlobal[i].SetActive(true);
                }
            }
        }
    }


    //CONSTRUCTION DU SKIN
    public void ConstructionSkin()
    {
        ClearLeSkin();
        visiteur.BodyConstructor();
        l_SkinSelection = visiteur.GetSkin();
        AfficherSkin();
    }


    //CLEAR DU SKIN
    void ClearLeSkin()
    {
        foreach (var item in l_SkinGlobal)
        {
            item.SetActive(false);
        }
    }
}

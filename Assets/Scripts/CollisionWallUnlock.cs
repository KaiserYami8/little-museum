﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionWallUnlock : MonoBehaviour
{
    // Start is called before the first frame update
    public bool canCheck;
    public List<GameObject> listTabGO;
    public List<Proto_Tableau_Scriptable> listTab;
    void Start()
    {
        canCheck = false;
        listTabGO = new List<GameObject>();
        listTab = new List<Proto_Tableau_Scriptable>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerStay(Collider other)
    {
        if (canCheck)
        {
            if (other.tag == "tableau"/* et d'autre potentiellement*/)
            {
                var refTab = other.gameObject.GetComponent<DisplayTableau>().Tableau;
                if (!listTab.Contains(refTab))
                {
                    listTab.Add(refTab);
                    listTabGO.Add(other.gameObject);
                }

            }
        }
        
    }

    public void SetupBool(bool value)
    {
        canCheck = value;
        GetComponent<BoxCollider>().enabled = canCheck;
        if (!canCheck) ResetList();
    }

    public void ResetList()
    {
        listTabGO = new List<GameObject>();
        listTab = new List<Proto_Tableau_Scriptable>();
    }
}

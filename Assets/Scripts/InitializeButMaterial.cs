﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InitializeButMaterial : MonoBehaviour
{
    public enum TypeMat
    {
        WallMaterial = 0,
        FloorMaterial = 1
    }
    public TypeMat TypeMaterial;

    public GameObject PrefabBut;
    private UIController uiControlRef;


    private void Awake()
    {
        
    }
    void Start()
    {
        uiControlRef = UIController._instance;
        //print(TypeMaterial);
        switch (TypeMaterial)
        {
            case TypeMat.WallMaterial:
                for(int i=0; i<uiControlRef.ArrayMaterialWall.Length; i++)
                {
                    var newBut = Instantiate(PrefabBut, transform);
                    newBut.name = $"Mat_Wall_{i}";
                    newBut.GetComponent<Button>().onClick.AddListener(uiControlRef.ChangeMaterialWall);
                    newBut.transform.GetChild(0).gameObject.GetComponent<Text>().text = uiControlRef.ArrayMaterialWall[i].name;
                    if (i == 0) newBut.GetComponent<Image>().color = Color.red;

                }
                break;

            case TypeMat.FloorMaterial:
                for (int i = 0; i < uiControlRef.ArrayMaterialFloor.Length; i++)
                {
                    var newBut = Instantiate(PrefabBut, transform);
                    newBut.name = $"Mat_Floor_{i}";
                    newBut.GetComponent<Button>().onClick.AddListener(uiControlRef.ChangeMaterialFloor);
                    newBut.transform.GetChild(0).gameObject.GetComponent<Text>().text = uiControlRef.ArrayMaterialFloor[i].name;
                    if (i == 0) newBut.GetComponent<Image>().color = Color.red;

                    //print(i);
                }
                break;
        }
    }

    
    void Update()
    {
        
    }
}

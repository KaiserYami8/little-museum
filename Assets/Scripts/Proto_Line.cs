﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Proto_Line : MonoBehaviour
{
    public static Proto_Line _instance;
    GameObject line;
    public List<Material> matLine;



    public float coefForRed;
    public float coefForGreen;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    public GameObject CreateLine(Vector3 posTableau1, Vector3 posTableau2, GameObject parent, float coef)
    {
        //print(coef);
        line = new GameObject();
        line.transform.parent = parent.transform;
        line.tag = "line";
        line.AddComponent<LineRenderer>();
        line.GetComponent<LineRenderer>().widthMultiplier = 0.1f; 
        line.GetComponent<LineRenderer>().SetPosition(0, posTableau1);
        line.GetComponent<LineRenderer>().SetPosition(1, posTableau2);
        
        if(coef < coefForRed)
        {
            line.GetComponent<LineRenderer>().material = matLine[2];
        }
        else if(coef > coefForGreen)
        {
            line.GetComponent<LineRenderer>().material = matLine[0];
        }
        else
        {
            line.GetComponent<LineRenderer>().material = matLine[1];
        }
        line.SetActive(false);

        return line;
    }




}

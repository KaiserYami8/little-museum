﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings_Manager : MonoBehaviour
{

    MusicAdapt1 refMusicEvent;

    void Start()
    {
        refMusicEvent = MusicAdapt1._instance;
    }
    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
        refMusicEvent.musiqueAdaptive.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }
    public void QuitTheGame()
    {
        Application.Quit();
    }
}

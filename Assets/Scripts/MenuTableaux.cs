﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class MenuTableaux : MonoBehaviour
{
    UIController refUiControl;
    GenerationMur refGenMur;
    SoundManager refSm;
    PainterManager refpaint;

    public GameObject RefMenuCompare;
    public Proto_Tableau_Scriptable refTableau;
    public GameObject refTableauGO;
    Camera cam;
    Vector3 target;

    private bool mouseOnMenu;
    private bool isSpawn;

    //INFORMATION
    public Text titreTableau;
    public Text artisteTableau;
    public Text periodeTableau;
    public Text mouvementTableau;
    public Text elementsTableau;
    public Text styleTableau;

    void Start()
    {
        refSm = SoundManager._instance;
        refUiControl = UIController._instance;
        refGenMur = GenerationMur._instance;
        refpaint = PainterManager._instance;

        RefMenuCompare = refUiControl.refMenuCompareTableau;
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        target = transform.position;
        
        if (refUiControl.refMenuTableau == null)
        {
            refUiControl.refMenuTableau = gameObject;
        }
        else
        {
            Destroy(refUiControl.refMenuTableau);
            refUiControl.refMenuTableau = gameObject;
        }

        Invoke("Spawned", 0.1f);
        UpdateInfo();

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = cam.WorldToScreenPoint(target);
        checkIfMenuClosed();

    }


    public void AppelFonctionMove()
    {
        refGenMur.enableMoveTab(refTableauGO, gameObject);
        refSm.TableauEnMain();
    }

    public void AppelFonctionStore()
    {
        refGenMur.ReturnTabToInventory(refTableau, refTableauGO);
        //print($"REFTAB = {refTableau} + REFTABGO = {refTableauGO}");
        Destroy(gameObject);
    }

    public void AppelFonctionInfo()
    {
        print("info");
    }

    public void AppelFonctionCompare()
    {
        refUiControl.CompareTableaux();
        RefMenuCompare.GetComponent<CompareTabScript>().FirstTab = refTableau;

        Destroy(gameObject);

    }

    public void MouseEnter()
    {
        mouseOnMenu = true;
    }

    public void MouseExit()
    {
        mouseOnMenu = false;
    }

    public void checkIfMenuClosed()
    {
        if (isSpawn)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                if (!mouseOnMenu) DestroyMenu();
            }
            else if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                DestroyMenu();
            }
        }
        
    }

    private void DestroyMenu()
    {
        refUiControl.refMenuTableau = null;
        Destroy(gameObject);
    }

    private void Spawned()
    {
        isSpawn = true;
    }

    void UpdateInfo()
    {
        titreTableau.text = $"TITRE : {refTableau.titre}";
        artisteTableau.text = $"ARTISTE : {refTableau.auteur}";
        periodeTableau.text = $"ARTISTE : {refTableau.periode.ToString()}";
        mouvementTableau.text = refTableau.courantArtTableau.ToString();
        styleTableau.text = refTableau.style.ToString();

    }

}

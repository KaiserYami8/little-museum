﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager _instance;

    public float MusicVolume_SM;
    public float SFXVolume_SM;
    public float MasterVolume_SM;
    public int qualityIndex_SM;
    public bool FullScreen_SM;
    public int ResolutionIndex_SM;


    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else if (_instance != this)
            Destroy(gameObject);
    }
}

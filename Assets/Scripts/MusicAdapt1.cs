﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MusicAdapt1 : MonoBehaviour
{

    public static MusicAdapt1 _instance;
    UnlockSalle refUS;

    // Système V1
    public FMOD.Studio.EventInstance musiqueAdaptive;

    /*
    [FMODUnity.EventRef]
    public string Smalt;
    [FMODUnity.EventRef]
    public string Smalt;
    [FMODUnity.EventRef]
    public string Smalt;
    
    public float randomSong;
    */

    public int oldNbUnlockedRooms;

    /*
    bool IsPlaying(FMOD.Studio.EventInstance instance)
    {
        FMOD.Studio.PLAYBACK_STATE state;
        instance.getPlaybackState(out state);
        return state != FMOD.Studio.PLAYBACK_STATE.STOPPED;
    }
    */




    private void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        refUS = UnlockSalle._instance;

        // Système V1
        musiqueAdaptive = FMODUnity.RuntimeManager.CreateInstance("event:/New (Mai)/Musique/MusicPlaylistRandom3");
        musiqueAdaptive.start();

        //musiqueAdaptiveV1.setParameterByName("UnlockedRooms", Random.Range(1, 3), true);
        musiqueAdaptive.setParameterByName("UnlockedRooms", 0, true);

        oldNbUnlockedRooms = refUS.nbRoomUnlocked;

        /*
        randomSong = Mathf.FloorToInt(Random.RandomRange(1f,3f));
        
        if (randomSong == 1)
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/New (Mai)/Musique/Smalt");
        }

        if (randomSong == 2)
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/New (Mai)/Musique/Lilac");
        }

        if (randomSong == 3)
        {
            FMODUnity.RuntimeManager.CreateInstance("event:/New (Mai)/Musique/Lime");
        }
        */
    }
    


    void Update()
    {
        // Système V1
        
        if (refUS.nbRoomUnlocked > oldNbUnlockedRooms)
        {
            //musiqueAdaptiveV1.setParameterByName("UnlockedRooms", Random.Range(1,3));
            musiqueAdaptive.setParameterByName("UnlockedRooms", oldNbUnlockedRooms+1);
            oldNbUnlockedRooms = refUS.nbRoomUnlocked;
        }
        

        //Debug.Log(FMOD.Studio.PLAYBACK_STATE.);


    }
}

﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PeonManager : MonoBehaviour
{

    public static PeonManager _instance;

    public int MaxPeonMusee;
    public List<GameObject> GlobalVisiteursList;
    public List<GameObject> MuseumVisiteursList;
    public GameObject[] AttenteVisiteursList;
    public Transform CheckPoint;
    public Transform[] AttenteVisiteursPoint;

    void Awake()
    {
        if (_instance == null)
            _instance = this;
        else if (_instance != this)
            Destroy(gameObject);
    }

    void Start()
    {
        GlobalVisiteursList = new List<GameObject>();
        MuseumVisiteursList = new List<GameObject>();
        AttenteVisiteursList = new GameObject[AttenteVisiteursPoint.Length];

    }
    public void SpeedPeon(float speed)
    {
        foreach (var item in GlobalVisiteursList)
        {
            switch (speed)
            {
                case 0:
                    item.GetComponent<NavMeshAgent>().speed = 0;
                    item.GetComponent<NavMeshAgent>().isStopped = true;
                    item.GetComponent<Animator>().speed = 0;
                    item.GetComponent<Rigidbody>().velocity = Vector3.zero;
                    item.GetComponent<NavMeshAgent>().velocity = Vector3.zero;
                    break;
                case 1:
                    item.GetComponent<NavMeshAgent>().speed = 3.5f;
                    item.GetComponent<Animator>().speed = 1;
                    item.GetComponent<NavMeshAgent>().isStopped = false;
                    item.GetComponent<Rigidbody>().velocity = Vector3.forward;
                    item.GetComponent<NavMeshAgent>().velocity = Vector3.forward;
                    break;
                case 2:
                    item.GetComponent<NavMeshAgent>().speed = 7f;
                    item.GetComponent<Animator>().speed = 2;
                    item.GetComponent<NavMeshAgent>().isStopped = false;
                    item.GetComponent<Rigidbody>().velocity = Vector3.forward;
                    item.GetComponent<NavMeshAgent>().velocity = Vector3.forward;
                    break;
                case 3:
                    item.GetComponent<NavMeshAgent>().speed = 10.5f;
                    item.GetComponent<Animator>().speed = 3;
                    item.GetComponent<NavMeshAgent>().isStopped = false;
                    item.GetComponent<Rigidbody>().velocity = Vector3.forward;
                    item.GetComponent<NavMeshAgent>().velocity = Vector3.forward;
                    break;
            }
        }

        
    }


    public void MovePeonInFile()
    {
        AttenteVisiteursList[0] = null;
        for (int i = 0; i < AttenteVisiteursList.Length; i++)
        {
            if(i < AttenteVisiteursList.Length - 1)
            {
                if(AttenteVisiteursList[i + 1] != null)
                {
                    AttenteVisiteursList[i] = AttenteVisiteursList[i + 1];
                    var refVisi = AttenteVisiteursList[i].GetComponent<AIVisiteur>();
                    refVisi.destination.RemoveAt(0);
                   
                    refVisi.arriver = false;
                    refVisi.destination.Add(AttenteVisiteursPoint[i].gameObject);
                }
                else
                {
                    AttenteVisiteursList[i] = null;
                }
                
            }
            else
            {
                AttenteVisiteursList[i] = null;
            }
            
        }
    }
}

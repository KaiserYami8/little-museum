﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    //Transitions Jeu - Menu
    public void PlayGameFromMenu()
    {
        SceneManager.LoadScene(1);
    }
    public void PlayNewLDFromMenu()
    {
        SceneManager.LoadScene(2);
    }
    public void GameToMenu()
    {
        SceneManager.LoadScene(0);
    }

    //Transitions Crédits - Menu
    public void CreditsFromMenu()
    {
        SceneManager.LoadScene(3);
    }
    public void CreditsToMenu()
    {
        SceneManager.LoadScene(0);
    }


    public void QuitGame()
    {
        Debug.Log("A Quitté l'application");
        Application.Quit();
    }
}


   
